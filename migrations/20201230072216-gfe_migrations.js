'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const sql = [
      'ALTER TABLE gfe_records ALTER COLUMN complaint_id SET NOT NULL;',
      'ALTER TABLE gfe_records ADD UNIQUE (complaint_id);'
    ]
    return Promise.all([queryInterface.sequelize.query(sql.join(' '))])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
}
