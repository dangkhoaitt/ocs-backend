'use strict'

const table = 'product_return_records'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const sql = [
      'ALTER TABLE product_return_records ALTER COLUMN complaint_id SET NOT NULL;',
      'ALTER TABLE product_return_records ADD UNIQUE (complaint_id);'
    ]
    return Promise.all([queryInterface.sequelize.query(sql.join(' '))])
  },

  down: async (queryInterface, Sequelize) => {}
}
