'use strict'

const table = 'product_return_records'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.describeTable(table).then((tableDefinition) => {
        console.log('tableDefinition', tableDefinition)
        const query = []
        if (!tableDefinition['infection_name']) {
          query.push(
            queryInterface.addColumn(table, 'infection_name', {
              type: Sequelize.STRING(500),
              allowNull: true
            })
          )
        }
        if (!tableDefinition['infectious_information_required']) {
          query.push(
            queryInterface.addColumn(table, 'infectious_information_required', {
              type: Sequelize.BOOLEAN,
              allowNull: true
            })
          )
        }
        if (!tableDefinition['required_action']) {
          query.push(
            queryInterface.addColumn(table, 'required_action', {
              type: Sequelize.Sequelize.STRING(50),
              allowNull: true
            })
          )
        }
        if (tableDefinition['infectious_information']) {
          query.push(queryInterface.removeColumn(table, 'infectious_information'))
        }
        if (tableDefinition['product_received_date']) {
          query.push(queryInterface.removeColumn(table, 'product_received_date'))
        }
        return Promise.all(query)
      })
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
}
