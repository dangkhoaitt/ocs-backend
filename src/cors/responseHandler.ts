// 4XX status code related to client side error
// 5XX status code related to server side error

import getErrorStatus from '../constant/ErrorData'

export interface SuccessResponse {
  status: number
  succMessage: string
  data: unknown
  total?: number
  page?: number
  message_key?: string
  limit?: number
}

function findErrorMessage(status: number) {
  return getErrorStatus.ERROR_STATUS_ARRAY.find((v) => v.status === status) || { error: 'There must be an error' }
}

/**
 * Success Response.
 * @param {number} status - Success response status
 * @param {string} succMessage - Success response message
 * @param {any} data - Success response custom data
 * @param {any} data - Success response custom data
 */
const sucessResponse = (options: SuccessResponse): SuccessResponse => options

/**
 * Error Reposnse.
 * @param {Response} res - Send error response
 * @param {number} statusCode - Error Status Code
 */
const errorResponse = (statusCode: number) => {
  return findErrorMessage(statusCode)
}

export default { errorResponse, sucessResponse }
