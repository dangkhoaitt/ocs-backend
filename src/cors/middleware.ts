import { get } from 'config';
import { NextFunction, Response } from 'express';
import jwt from 'jsonwebtoken';
import mode from '../config';
import constant from '../constant/constant';
import UserProfileServices from '../services/UserProfileServices';
import reqResponse from './responseHandler';

const checkToken = async (req: any, res: Response, next: NextFunction) => {
    const token = req.headers.authorization.replace('Bearer ', '');
    console.log(token);

    if (token) {
        jwt.verify(token, get(`${mode}.secret`), {
            ignoreExpiration: true
        }, async (err, decoded: any) => {
            if (err) {
                return res.status(414).send(reqResponse.errorResponse(414));
            } else {
                const user = await UserProfileServices.getUserById(decoded.id)
                if (!user) {
                    return res.status(414).send(reqResponse.errorResponse(414));
                }
                req.userId = decoded.id;
                req.countryId = decoded.countryId;
                req.role = user.role
                next();
            }
        });
    } else {
        return res.status(415).send(reqResponse.errorResponse(415));
    }
}

const checkRole = async (req: any, res: Response, next: NextFunction,role:string) => {
    const user = await UserProfileServices.getUserById( req.userId)
    if(role === constant.ROLE_COMPLAINT.VIEW) {
        if (user.role === constant.ROLE.ROLE_OEM || user.role === constant.ROLE.ROLE_WIPRO || user.role === constant.ROLE.ROLE_QA) {
            next();
            return;
        }
    } else if(role === constant.ROLE_COMPLAINT.EDIT || role === constant.ROLE_COMPLAINT.ADD || role === constant.ROLE_COMPLAINT.CLONE) {
        if (user.role === constant.ROLE.ROLE_WIPRO || user.role === constant.ROLE.ROLE_QA) {
            next();
            return;
        }
    }
    return res.status(403).send({ status: 403, message: "permission denied", message_key: constant.RES_ERROR });
}

export default {
    checkToken,
    checkRole
}
