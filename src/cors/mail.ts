import config from 'config'
import nodemailer from 'nodemailer'
import Mail from 'nodemailer/lib/mailer'

const sendMail = (options: Mail.Options) => {
  return new Promise((resolve, reject) => {
    const mode = process.env.NODE_ENV || 'dev'
    const transporter = nodemailer.createTransport({
      host: config.get(`${mode}.email.smtp_address`),
      port: config.get(`${mode}.email.smtp_port`),
      secure: config.get(`${mode}.email.smtp_port`) === 465 ? true : false, // true for 465, false for other ports
      auth: {
        user: config.get(`${mode}.email.smtp_user_name`),
        pass: config.get(`${mode}.email.smtp_password`)
      }
    })

    const mailOptions: Mail.Options = { from: `${mode}.email.mail_from`, ...options }

    transporter.sendMail(mailOptions, (error) => {
      if (error) {
        reject('There is error in mail:- ' + error)
      } else {
        console.log('Mail has been sent to', options.to)
        resolve('Mail Sent!!!')
      }
    })
  })
}

export default {
  sendMail
}
