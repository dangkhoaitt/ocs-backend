import config from 'config'

const mode = process.env.NODE_ENV || config.get('local')

export default mode
