import bodyParser from 'body-parser'
import chalk from 'chalk'
import config from 'config'
import cors from 'cors'
import express, { NextFunction, Request, Response } from 'express'
import monitor from 'express-status-monitor'
import path from 'path'
import mode from './config'
import database, { alterContrainst } from './models/sequelize'
import route from './routes'
// import pack from '../package'

const app = express()

app.use(cors())
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: false
  })
)
app.use(monitor())

route(app)

const dir = path.join(__dirname, 'assets')
app.use('/upload', express.static(dir))
app.use(express.static(path.join(__dirname, 'public')));
app.use(haltOnTimedout)

function haltOnTimedout(req: Request, _: Response, next: NextFunction) {
  if (!Object.keys(req).includes('timedout')) next()
}

database.sync({ force: false }).then(() => {
  console.log('Drop and re-sync db.')
  alterContrainst()
})

const start = () =>
  app.listen(config.get(`${mode}.port`), () => {
    console.log(chalk.yellow('.......................................'))
    console.log(chalk.green(config.get(`${mode}.name`)))
    console.log(chalk.green(`Port:\t\t${config.get(`${mode}.port`)}`))
    console.log(chalk.green(`Mode:\t\t${config.get(`${mode}.mode`)}`))
    // console.log(chalk.green(`App version:\t${pack.version}`))
    console.log(chalk.green('database connection is established'))
    console.log(chalk.yellow('.......................................'))
  })

const dbConnection = () => {
  // When you try to connect database please comment bellow start method
  start()

  // MYSQL database connection start

  // const databaseConfig = config.get(`${mode}.database`);
  // con can be access anywhre in the project
  // con = mysql.createPool(databaseConfig);

  // con.getConnection((err) => {
  //   if (err) {
  //     //- The server close the connection.
  //     if (err.code === "PROTOCOL_CONNECTION_LOST") {
  //       console.error("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
  //     }

  //     //- Connection in closing
  //     else if (err.code === "PROTOCOL_ENQUEUE_AFTER_QUIT") {
  //       console.error("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
  //     }

  //     //- Fatal error : connection variable must be recreated
  //     else if (err.code === "PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR") {
  //       console.error("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
  //     }

  //     //- Error because a connection is already being established
  //     else if (err.code === "PROTOCOL_ENQUEUE_HANDSHAKE_TWICE") {
  //       console.error("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
  //     }

  //     //- Anything else
  //     else {
  //       console.error("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
  //     }

  //     setTimeout(dbConnection, 5000);
  //   } else {
  //     app.set('con', con);
  //     start();
  //   }
  // });
}

dbConnection()
