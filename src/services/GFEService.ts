import { col, FindAttributeOptions, FindOptions, fn, Op, WhereOptions } from 'sequelize'
import { Literal } from 'sequelize/types/lib/utils'
import constant from '../constant/constant'
import {
  DATE_FIELDS_REGEX,
  DEFAULT_LIMIT,
  DEFAULT_MAX_ITEM,
  DEFAULT_PAGE,
  DEFAULT_SORT,
  exportFields,
  findOneOptions
} from '../models/base'
import { GFEAttributes } from '../models/GFE'
import { GFERequestAttributes } from '../models/GfeRequest'
import { COMPLAINTS, GFE_RECORDS, GFE_REQUEST, PRODUCTS, SUPPLIERS, USERS } from '../models/ModelConstants'
import { getModel } from '../models/sequelize'
import CommonService from './CommonService'
import ExportService, { ExportModel } from './ExportService'
import gfeAttachmentService from './GfeAttachmentService'
import gfeWatcherService from './GfeWatcherService'
import utils from './utils'

const model = getModel<GFEAttributes>(GFE_RECORDS.TABLE)
const modelGFE = getModel<GFERequestAttributes>(GFE_REQUEST.TABLE)
const cplAlias = 'complaint'
const { literal } = model.sequelize.Sequelize
export interface GetGfeInput extends Record<string, unknown> {
  limit?: string
  page?: string
  sortField?: string
  sortOrder?: string
  keyword?: string
}

enum GFEStatus {
  GFE_CLOSED = 'GFE_CLOSED'
}

const dateFields = [
  GFE_RECORDS.FIELDS.REQUEST_DATE,
  GFE_RECORDS.FIELDS.REQUEST_DUE_DATE,
  GFE_RECORDS.FIELDS.REQUEST_DATE_FROM_OEM,
  GFE_RECORDS.FIELDS.PRODUCT_RECEIVED_DATE_BSC,
  GFE_RECORDS.FIELDS.RESPONSE_DATE
]

const GFE_EXPORT_FIELDS = exportFields(GFE_RECORDS.FIELDS)
const COMPLAINT_EXPORT_FIELDS = [
  COMPLAINTS.FIELDS.COMPLAINT_CODE,
  COMPLAINTS.FIELDS.DESCRIPTION,
  COMPLAINTS.FIELDS.EVENT_DATE,
  COMPLAINTS.FIELDS.BSC_AWARE_DATE,
  COMPLAINTS.FIELDS.JPOS_CIPI
]
const OTHER_EXPORT_FIELDS = [SUPPLIERS.FIELDS.SUPPLIER_ID, SUPPLIERS.FIELDS.SUPPLIER_NAME]

const countMyGFE = async (userId: number, countryId: number) => {
  const complaintModel = getModel(COMPLAINTS.TABLE)
  return {
    gfeCount: await model.count({
      where: {
        [GFE_RECORDS.FIELDS.STATUS]: { [Op.not]: GFEStatus.GFE_CLOSED },
        [GFE_RECORDS.FIELDS.DELETE_FLG]: { [Op.not]: true }
      },
      include: {
        model: complaintModel,
        where: {
          [COMPLAINTS.FIELDS.ASSIGNEE]: userId,
          [COMPLAINTS.FIELDS.COUNTRY_ID]: countryId,
          [COMPLAINTS.FIELDS.DELETE_FLG]: { [Op.not]: true }
        },
        include: [
          {
            model: getModel(USERS.TABLE),
            attributes: [],
            required: true,
            where: { [USERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          },
          {
            model: getModel(SUPPLIERS.TABLE),
            attributes: [],
            required: false,
            where: { [SUPPLIERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          },
          {
            model: getModel(PRODUCTS.TABLE),
            attributes: [],
            required: true,
            where: { [PRODUCTS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          }
        ]
      }
    })
  }
}

function getSearchOptions(keyword: string): WhereOptions {
  if (keyword)
    return {
      [Op.or]: [
        literal(`${cplAlias}."${COMPLAINTS.FIELDS.COMPLAINT_CODE}" ILIKE '%${keyword}%'`),
        literal(`${cplAlias}."${COMPLAINTS.FIELDS.DESCRIPTION}" ILIKE '%${keyword}%'`),
        literal(`"${SUPPLIERS.FIELDS.SUPPLIER_NAME}" ILIKE '%${keyword}%'`)
      ]
    }
  return undefined
}

function getFindOptions(filter: GetGfeInput): FindOptions {
  const { keyword = '', ...other } = filter
  const filters = []
  const search = getSearchOptions(keyword)
  for (const key in other) {
    if (other.hasOwnProperty(key)) {
      const dateFilter = other[key] as string
      if (COMPLAINTS.FIELDS.SUPPLIER_ID === key) {
        filters.push(literal(`${cplAlias}."${SUPPLIERS.FIELDS.SUPPLIER_ID}" IN (${dateFilter.split(',')})`))
      } else if (dateFields.includes(key)) {
        const values = dateFilter.split(',')
        filters.push({
          [Op.and]: [
            { [key]: { [Op.gte]: utils.convertDateStrToTimestamp(values[0]) } },
            { [key]: { [Op.lte]: utils.convertDateStrToTimestamp(values[1], true) } }
          ]
        })
      } else if (GFE_RECORDS.FIELDS.ASSIGNEE === key) {
        filters.push(literal(`"gfe_records"."${GFE_RECORDS.FIELDS.ASSIGNEE}" IN (${other[key]})`))
      } else {
        filters.push({ [key]: other[key] })
      }
    }
  }
  return {
    where: {
      [Op.and]: [{ [GFE_RECORDS.FIELDS.DELETE_FLG]: { [Op.not]: true } }, ...filters, search]
    },
    raw: true,
    include: [
      {
        model: getModel(COMPLAINTS.TABLE),
        attributes: [],
        as: cplAlias,
        required: true,
        include: [{ model: getModel(SUPPLIERS.TABLE), attributes: [], required: false }]
      },
      {
        model: getModel(USERS.TABLE),
        attributes: [],
        required: true
      }
    ]
  }
}

function getOrderBy(sortField: string, sortOrder: string): Literal {
  return literal(
    COMPLAINTS.FIELDS.JPOS_CIPI === sortField || COMPLAINTS.FIELDS.COMPLAINT_CODE === sortField
      ? `"${cplAlias}"."${sortField}" ${sortOrder}`
      : `"${GFE_RECORDS.TABLE}"."${sortField}" ${sortOrder}`
  )
}

const getGfeRecords = async (query: GetGfeInput) => {
  const {
    limit = DEFAULT_LIMIT,
    page = DEFAULT_PAGE,
    sortField = GFE_RECORDS.FIELDS.ID,
    sortOrder = DEFAULT_SORT,
    ...filter
  } = query

  const limitInt = utils.convertStrToInt(limit)
  const pageInt = utils.convertStrToInt(page)
  const [total, data] = await Promise.all([
    model.count({ ...getFindOptions(filter) }),
    model.findAll({
      ...getFindOptions(filter),
      attributes: [
        [fn('CONCAT', col(COMPLAINTS.FIELDS.COMPLAINT_CODE)), COMPLAINTS.FIELDS.COMPLAINT_CODE_ALIAS],
        [fn('CONCAT', col(COMPLAINTS.FIELDS.JPOS_CIPI)), COMPLAINTS.FIELDS.JPOS_CIPI_ALIAS],
        [
          fn('CONCAT', col(`user."${USERS.FIELDS.FIRST_NAME}"`), ' ', col(`user."${USERS.FIELDS.LAST_NAME}"`)),
          COMPLAINTS.FIELDS.ASSIGNEE
        ],
        GFE_RECORDS.FIELDS.ID,
        [GFE_RECORDS.FIELDS.COMPLAINT_ID, GFE_RECORDS.FIELDS.COMPLAINT_ID_ALIAS],
        [GFE_RECORDS.FIELDS.GFE_COUNT, GFE_RECORDS.FIELDS.GFE_COUNT_ALIAS],
        [GFE_RECORDS.FIELDS.REQUEST_DATE, GFE_RECORDS.FIELDS.REQUEST_DATE_ALIAS],
        [GFE_RECORDS.FIELDS.STATUS, GFE_RECORDS.FIELDS.STATUS_ALIAS],
        [GFE_RECORDS.FIELDS.REQUEST_DUE_DATE, GFE_RECORDS.FIELDS.REQUEST_DUE_DATE_ALIAS]
      ],
      raw: true,
      limit: limitInt,
      offset: (pageInt - 1) * limitInt,
      order: getOrderBy(sortField, sortOrder)
    })
  ])
  return { data, total, page: pageInt, limit: limitInt }
}

const exportGfeRecords = async (exportFields: string[], query: GetGfeInput) => {
  const {
    limit = DEFAULT_MAX_ITEM,
    sortField = GFE_RECORDS.FIELDS.CREATED_AT,
    sortOrder = DEFAULT_SORT,
    language = 'en',
    ...filter
  } = query

  const selectExportFields = selectExportFieldsQuery(exportFields)
  if (!selectExportFields) return null

  const limitInt = utils.convertStrToInt(limit)
  const result = await model.findAll({
    ...getFindOptions(filter),
    raw: true,
    attributes: selectExportFieldsQuery(exportFields),
    limit: limitInt,
    order: getOrderBy(sortField, sortOrder)
  })
  if (result.length === 0) return null

  return ExportService.generateExport({
    model: ExportModel.GFE_RECORDS,
    fields: Object.keys(result[0]),
    data: generateGFEExport(result),
    language: language as string
  })
}

function selectExportFieldsQuery(exportFields: string[]): FindAttributeOptions {
  const attributes: FindAttributeOptions = []
  for (const field of exportFields) {
    if (GFE_EXPORT_FIELDS.includes(field)) {
      attributes.push(field)
    } else if (COMPLAINT_EXPORT_FIELDS.includes(field)) {
      attributes.push(`${cplAlias}.${field}`)
    }
  }
  return attributes
}

function generateGFEExport(data: any[]) {
  for (const row of data) {
    Object.keys(row).forEach((key) => {
      if (DATE_FIELDS_REGEX.test(key)) row[key] = utils.convertTimestampToDateStr(Number(row[key]), 'YYYY-MM-DD HH:mm')
    })
  }
  return data
}

const getDetail = async (id: string) => {
  const dataGFE = await model.findOne({
    raw: true,
    where: {
      [GFE_RECORDS.FIELDS.ID]: id,
      [GFE_RECORDS.FIELDS.DELETE_FLG]: { [Op.not]: true }
    }
  })
  if (dataGFE) {
    const detail = await modelGFE.findAll({
      raw: true,
      where: {
        [GFE_REQUEST.FIELDS.GFE_ID]: utils.convertStrToInt(id),
        [GFE_REQUEST.FIELDS.DELETE_FLG]: { [Op.not]: true }
      }
    })
    const data = {
      gfeId: dataGFE.id,
      complaintId: dataGFE.complaintId,
      gfeCount: dataGFE.gfeCount,
      requests: detail
    }
    return data
  }
  return false
}

const getByComplaint = async (complaintId: number) => {
  return await model.findOne({
    raw: true,
    where: {
      [GFE_RECORDS.FIELDS.COMPLAINT_ID]: complaintId,
      [GFE_RECORDS.FIELDS.DELETE_FLG]: { [Op.not]: true }
    },
    attributes: [
      [GFE_RECORDS.FIELDS.ID, GFE_RECORDS.FIELDS.GFE_ID_ALIAS],
      [GFE_RECORDS.FIELDS.COMPLAINT_ID, GFE_RECORDS.FIELDS.COMPLAINT_ID_ALIAS],
      [GFE_RECORDS.FIELDS.GFE_COUNT, GFE_RECORDS.FIELDS.GFE_COUNT_ALIAS]
    ]
  })
}

const createGfe = async (createdBy: string | number, input: GFEAttributes) => {
  const { watchers, attachments, ...other } = input
  const complaintId = other.complaintId
  const gefRecord = await model.findOne({
    raw: true,
    where: {
      [GFE_RECORDS.FIELDS.COMPLAINT_ID]: complaintId,
      [GFE_RECORDS.FIELDS.DELETE_FLG]: { [Op.not]: true }
    }
  })

  let gfeRequest: GFERequestAttributes = null

  if (gefRecord) {
    if (gefRecord.gfeCount === 3) {
      return false
    }
    other.gfeCount = gefRecord.gfeCount + 1
    other.gfeId = gefRecord.id
    gfeRequest = await modelGFE.create(other)
    if (gfeRequest) {
      const update = await model.update(other, { where: { id: gefRecord.id } })
      CommonService.addHistory({
        action: 'CREATE',
        recordId: gefRecord.id,
        type: constant.TYPE_ENUM.GFE,
        userId: createdBy
      })
    }
  } else {
    other.gfeCount = 1
    const gfeRs = await model.create(other)
    other.gfeId = gfeRs.id
    gfeRequest = await modelGFE.create(other)
    CommonService.addHistory({
      action: 'CREATE',
      recordId: gfeRs.id,
      type: constant.TYPE_ENUM.GFE,
      userId: createdBy
    })
  }

  if (watchers && gfeRequest) {
    watchers.map(async (user: any) => {
      const userId = user.userId
      return await gfeWatcherService.addWatcher({ objId: gfeRequest.id, userId })
    })
    CommonService.doSendMail({ createdBy, screen: 'CREATE', type: constant.TYPE_ENUM.GFE, userIds: watchers })
  }

  if (attachments && gfeRequest) {
    attachments.map(async (input: any) => {
      input.objId = gfeRequest.id
      return await gfeAttachmentService.addAttachGfe(input)
    })
  }

  return gfeRequest
}

const editGfe = async (createdBy: string, id: string, body: GFERequestAttributes) => {
  const whereOptions = { where: findOneOptions({ [GFE_REQUEST.FIELDS.ID]: utils.convertStrToInt(id) }) }
  const attr = await getAtrribute(body)
  const { dataValues } = await modelGFE.findOne({ ...whereOptions, attributes: Object.keys(attr) })
  if (dataValues) {
    await modelGFE.update(attr, whereOptions)
    await model.update(attr, { where: { id: attr.gfeId } })
    CommonService.addHistory({
      action: 'EDIT',
      body: attr,
      detail: dataValues as Record<string, unknown>,
      recordId: attr.gfeId,
      type: constant.TYPE_ENUM.GFE,
      userId: createdBy
    })
    return body
  }
  return null
}

const getAtrribute = async (body: GFERequestAttributes) => {
  return {
    assignee: body.assignee,
    gfeStatus: body.gfeStatus,
    gfeId: body.gfeId,
    gfeCount: body.gfeCount,
    requestDate: body.requestDate,
    requestDueDate: body.requestDueDate,
    productReceivedDateBsc: body.productReceivedDateBsc,
    requestDateFromOem: body.requestDateFromOem,
    sendDate: body.sendDate,
    responseDate: body.responseDate,
    requestInformation: body.requestInformation,
    requestTemplate: body.requestTemplate,
    respondent: body.respondent,
    response: body.response,
    productAvailableForReturn: body.productAvailableForReturn
  }
}

const getGfeId = async (gfeRequestId: number) => {
  const { gfeId } = await modelGFE.findOne({ where: { id: gfeRequestId } })
  return gfeId
}

export default { countMyGFE, getGfeRecords, exportGfeRecords, getDetail, createGfe, getByComplaint, editGfe, getGfeId }
