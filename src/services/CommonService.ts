import path from 'path'
import { literal, Op } from 'sequelize'
import constant from '../constant/constant'
import Mail from '../cors/mail'
import { concatFields, DEFAULT_LIMIT, DEFAULT_SORT, findOneOptions } from '../models/base'
import { ComplaintAttachmentAttributes } from '../models/ComplaintAttachment'
import { ComplaintWatcherAttributes, GET_LIST_QUERY } from '../models/ComplaintWatcher'
import { CountryAttributes } from '../models/Country'
import {
  COMPLAINT_ATTACHMENTS,
  COMPLAINT_WATCHERS,
  COUNTRIES,
  HISTORIES,
  STATUS,
  SUPPLIERS,
  USERS
} from '../models/ModelConstants'
import { executeQuery, getModel } from '../models/sequelize'
import { StatusAttributes } from '../models/Status'
import { UserAttributes } from '../models/User'
import ComplaintService from './ComplaintService'
import ComplaintWatcherService from './ComplaintWatcherService'
import CRLService from './CRLService'
import EmailTemplateServices from './EmailTemplateServices'
import GfeAttachmentService from './GfeAttachmentService'
import GFEService from './GFEService'
import GfeWatcherService from './GfeWatcherService'
import MDVService from './MDVService'
import ProductReturnService from './ProductReturnService'
import PRWatcherService from './PRWatcherService'
import UserProfileServices from './UserProfileServices'
import utils from './utils'

const statusModel = getModel<StatusAttributes>(STATUS.TABLE)
const countryModel = getModel<CountryAttributes>(COUNTRIES.TABLE)
const complaintAttachmentModel = getModel<ComplaintAttachmentAttributes>(COMPLAINT_ATTACHMENTS.TABLE)
const complaintWatcherModel = getModel<ComplaintWatcherAttributes>(COMPLAINT_WATCHERS.TABLE)
const userModel = getModel<UserAttributes>(USERS.TABLE)

const getStatusList = async () => {
  return statusModel.findAll({
    attributes: [
      STATUS.FIELDS.ID,
      STATUS.FIELDS.CODE,
      [STATUS.FIELDS.COLOR_HEX, STATUS.FIELDS.COLOR_HEX_ALIAS],
      STATUS.FIELDS.DESCRIPTION,
      STATUS.FIELDS.TYPE,
      STATUS.FIELDS.POSITION
    ]
  })
}

const getCountryList = async () => {
  return countryModel.findAll({
    attributes: [
      [COUNTRIES.FIELDS.COUNTRY_CODE, COUNTRIES.FIELDS.COUNTRY_CODE_ALIAS],
      [COUNTRIES.FIELDS.COUNTRY_ID, COUNTRIES.FIELDS.COUNTRY_ID_ALIAS],
      [COUNTRIES.FIELDS.COUNTRY_KEY, COUNTRIES.FIELDS.COUNTRY_KEY_ALIAS],
      [COUNTRIES.FIELDS.COUNTRY_NAME, COUNTRIES.FIELDS.COUNTRY_NAME_ALIAS]
    ]
  })
}

const getAttachment = async (type: string, id: number) => {
  if (type === constant.TYPE_ENUM.COMPLAINT) {
    return complaintAttachmentModel.findAll({
      where: { [COMPLAINT_ATTACHMENTS.FIELDS.COMPLAINT_ID]: id }
    })
  } else if (type === constant.TYPE_ENUM.GFE) {
    return GfeAttachmentService.getAttachment(id)
  }
}

const getWatcher = async (type: string, id: number) => {
  switch (type) {
    case constant.TYPE_ENUM.COMPLAINT:
      const sql = GET_LIST_QUERY + ` \n AND cplwt.complaint_id = ${id} `
      return executeQuery({ modelString: COMPLAINT_WATCHERS.TABLE, query: sql })
    case constant.TYPE_ENUM.PRO_RETURN:
      return PRWatcherService.getWatcher(id)
    case constant.TYPE_ENUM.GFE:
      return GfeWatcherService.getWatcher(id)
    default:
      return null
  }
}

const upLoadFile = async (req: any) => {
  const data: unknown[] = []
  const fileUploads = req.files

  fileUploads.forEach(function (file: any) {
    if (file) {
      const ext = path.extname(file.originalname).slice(1)
      const fileName = file.originalname.substring(0, file.originalname.lastIndexOf('.'))
      const dataFIle = {
        fileName,
        fileType: ext,
        filePath: file.location
      }
      data.push(dataFIle)
    }
  })
  return data
}

type AttachmentInput = { objId: number; fileName: string; fileType: string; filePath: string; type: string }
const addAttachmentFile = async (createdBy: string | number, input: AttachmentInput[]) => {
  let data
  if (input.length > 0) {
    const { type, objId, ...other } = input[0]
    const attachOpts: AttachOptions[] = []
    function addAttachmentByType(item: AttachmentInput) {
      switch (type) {
        case constant.TYPE_ENUM.COMPLAINT:
          attachOpts.push({ ...other, type: 'ADD' })
          return handleAddAttachComplaint(item)
        case constant.TYPE_ENUM.GFE:
          attachOpts.push({ ...other, type: 'ADD' })
          return GfeAttachmentService.addAttachGfe(item)
        default:
          break
      }
    }
    const gfeId = await GFEService.getGfeId(objId)
    let recordId = objId
    if (gfeId && type === constant.TYPE_ENUM.GFE) {
      recordId = gfeId
    }
    data = await Promise.all(input.map(addAttachmentByType))
    addHistory({ action: 'ATTACHMENT', recordId, type, userId: createdBy, attachOpts })
  }
  return data
}

type WatcherRecord = { objId: number; userId: number; type: string }
const addWatcher = async (createdBy: string | number, input: WatcherRecord[]) => {
  let data: any[] = []
  if (input.length > 0) {
    const { type, objId } = input[0]
    const watchers: WatcherOptions[] = []
    function addWatcherByType(item: WatcherRecord) {
      switch (type) {
        case constant.TYPE_ENUM.COMPLAINT:
          watchers.push({ watcherType: 'ADD', watcherId: item.userId })
          return handleAddWatcherComplaint(item)
        case constant.TYPE_ENUM.PRO_RETURN:
          watchers.push({ watcherType: 'ADD', watcherId: item.userId })
          return PRWatcherService.addWatcher(item)
        case constant.TYPE_ENUM.GFE:
          watchers.push({ watcherType: 'ADD', watcherId: item.userId })
          return GfeWatcherService.addWatcher(item)
        default:
          break
      }
    }
    data = await Promise.all(input.map(addWatcherByType))
    const gfeId = await GFEService.getGfeId(objId)
    let recordId = objId
    if (gfeId && type === constant.TYPE_ENUM.GFE) {
      recordId = gfeId
    }
    addHistory({ action: 'WATCHER', recordId, type, userId: createdBy, watchers })
  }
  return data
}

type AttachOptions = { fileName: string; fileType: string; filePath: string; type: 'ADD' | 'REMOVE' }
type WatcherOptions = { watcherId?: string | number; watcherType: 'ADD' | 'REMOVE' }
type HistoryOptions = {
  action: 'CREATE' | 'EDIT' | 'MAIL' | 'ATTACHMENT' | 'WATCHER'
  type: string
  recordId: string | number
  userId: string | number
  detail?: Record<string, unknown>
  body?: Record<string, unknown>
  mailOpts?: unknown
  attachOpts?: AttachOptions[]
  watchers?: WatcherOptions[]
}
interface HistoryRecord extends Record<string, unknown> {
  userId: string | number
  action: string
  fieldName?: string
  fieldCode?: string
  oldValue?: string | unknown
  newValue?: string | unknown
  createdAt?: number
  fullName?: string
}
const HISTORY_MODEL = {
  [constant.TYPE_ENUM.COMPLAINT]: getModel(HISTORIES.COMPLAINT),
  [constant.TYPE_ENUM.PRO_RETURN]: getModel(HISTORIES.PRODUCT_RETURN),
  [constant.TYPE_ENUM.GFE]: getModel(HISTORIES.GFE)
}
const HISTORY_ID_FIELD = {
  [constant.TYPE_ENUM.COMPLAINT]: HISTORIES.FIELDS.COMPLAINT_ID_ALIAS,
  [constant.TYPE_ENUM.PRO_RETURN]: HISTORIES.FIELDS.PR_ID_ALIAS,
  [constant.TYPE_ENUM.GFE]: HISTORIES.FIELDS.GFE_ID_ALIAS
}
const HISTORY_ID_FIELD_GET = {
  [constant.TYPE_ENUM.COMPLAINT]: HISTORIES.FIELDS.COMPLAINT_ID,
  [constant.TYPE_ENUM.PRO_RETURN]: HISTORIES.FIELDS.PR_ID,
  [constant.TYPE_ENUM.GFE]: HISTORIES.FIELDS.GFE_ID
}
function historyChanges(options: HistoryOptions): HistoryRecord[] {
  const { action, type, body = {}, detail = {}, recordId, userId } = options
  const data: HistoryRecord[] = []
  for (const field in body) {
    if (body[field] !== detail[field]) {
      data.push({
        action,
        [`${HISTORY_ID_FIELD[type]}`]: recordId,
        fieldName: field,
        newValue: body[field],
        oldValue: detail[field],
        userId
      })
    }
  }
  return data
}
const addHistory = async (options: HistoryOptions) => {
  const { action, type, recordId, userId } = options
  const { attachOpts = [], mailOpts = {}, watchers = [] } = options
  const model = HISTORY_MODEL[type]
  let record: HistoryRecord
  let changes: HistoryRecord[] = []
  switch (action) {
    case 'ATTACHMENT':
      attachOpts.forEach((file) => {
        record = { userId, action, attachment: JSON.stringify(file) }
        record[`${HISTORY_ID_FIELD[type]}`] = recordId
        changes.push(record)
      })
      if (changes.length > 0) model.bulkCreate(changes)
      break
    case 'CREATE':
      record = { userId, action }
      record[`${HISTORY_ID_FIELD[type]}`] = recordId
      model.bulkCreate([record])
      break
    case 'EDIT':
      changes = historyChanges(options)
      if (changes.length > 0) model.bulkCreate(changes)
      break
    case 'MAIL':
      record = { userId, action, mail: JSON.stringify(mailOpts) }
      record[`${HISTORY_ID_FIELD[type]}`] = recordId
      model.bulkCreate([record])
      break
    case 'WATCHER':
      watchers.forEach((file) => {
        record = { userId, action, ...file }
        record[`${HISTORY_ID_FIELD[type]}`] = recordId
        changes.push(record)
      })
      if (changes.length > 0) model.bulkCreate(changes)
      break

    default:
      break
  }
}

const handleAddWatcherComplaint = async (input: any) => {
  const dataWatcher = await complaintWatcherModel.create({
    complaintId: input.objId,
    userId: input.userId
  })

  const userInfo = await userModel.findOne({ where: { id: dataWatcher.userId } })

  dataWatcher.setDataValue('avatar', userInfo?.avatar)
  dataWatcher.setDataValue('fullName', userInfo?.fullName)
  dataWatcher.setDataValue('email', userInfo?.email)
  return dataWatcher
}

const handleAddAttachComplaint = async (input: any) => {
  const dataAttach = await complaintAttachmentModel.create({
    complaintId: input.objId,
    fileName: input.fileName,
    fileType: input.fileType,
    filePath: input.filePath
  })
  return dataAttach
}

const deleteWatcher = async (createdBy: string | number, idWatcher: number, type: string) => {
  if (type === constant.TYPE_ENUM.COMPLAINT) {
    const { complaintId, userId } = await complaintWatcherModel.findOne({
      where: findOneOptions({ id: idWatcher })
    })
    const rowDeleted = await complaintWatcherModel.destroy({
      where: { id: idWatcher }
    })
    addHistory({
      action: 'WATCHER',
      recordId: complaintId,
      type,
      userId: createdBy,
      watchers: [{ watcherType: 'REMOVE', watcherId: userId }]
    })
    return rowDeleted
  } else if (type === constant.TYPE_ENUM.PRO_RETURN) {
    const { prId, userId } = await PRWatcherService.deleteWatcher(idWatcher)
    addHistory({
      action: 'WATCHER',
      recordId: prId,
      type,
      userId: createdBy,
      watchers: [{ watcherType: 'REMOVE', watcherId: userId }]
    })
    return 1
  } else if (type === constant.TYPE_ENUM.GFE) {
    const { gfeId, userId } = await GfeWatcherService.deleteWatcher(idWatcher)
    addHistory({
      action: 'WATCHER',
      recordId: gfeId,
      type,
      userId: createdBy,
      watchers: [{ watcherType: 'REMOVE', watcherId: userId }]
    })
    return 1
  }
}

const deleteAttachmentFile = async (createdBy: string | number, idFile: number, type: string) => {
  if (type === constant.TYPE_ENUM.COMPLAINT) {
    const fileAttachment = await complaintAttachmentModel.findOne({ where: { id: idFile } })
    if (!fileAttachment) {
      return false
    }
    const rowDeleted = await complaintAttachmentModel.destroy({ where: { id: idFile } })

    if (rowDeleted === 1) {
      const { complaintId, fileName, filePath, fileType } = fileAttachment
      addHistory({
        action: 'ATTACHMENT',
        recordId: complaintId,
        type,
        userId: createdBy,
        attachOpts: [{ fileName, filePath, fileType, type: 'REMOVE' }]
      })
      const params = { fileName: fileAttachment.fileName + '.' + fileAttachment.fileType }
      return params
    }
    return false
  } else if (type === constant.TYPE_ENUM.GFE) {
    const fileAttachment = await GfeAttachmentService.deleteAttachmentFile(idFile)
    if (fileAttachment) {
      const { gfeId, fileName, filePath, fileType } = fileAttachment
      addHistory({
        action: 'ATTACHMENT',
        recordId: gfeId,
        type,
        userId: createdBy,
        attachOpts: [{ fileName, filePath, fileType, type: 'REMOVE' }]
      })
      const params = { fileName: fileAttachment.fileName + '.' + fileAttachment.fileType }
      return params
    }
    return false
  }
}

const getSummary = async (userId: number, countryId: number) => {
  const result = await Promise.all([
    ComplaintService.countMyComplaint(userId, countryId),
    ProductReturnService.countMyPR(userId, countryId),
    GFEService.countMyGFE(userId, countryId),
    CRLService.countMyCRL(userId, countryId),
    MDVService.countMyMDV(userId, countryId)
  ])
  return Object.assign({}, ...result)
}

type HistoryOutput = {
  userId: string | number
  userName: string
  createdAt: number
  action: string
  changes: unknown[]
}
const processHistoryOutput = (result: unknown[]) => {
  const output: HistoryOutput[] = []
  result.forEach((value: { dataValues: HistoryRecord }, index) => {
    const { dataValues } = value
    const { userId, createdAt, fullName, action, ...other } = dataValues
    if (index === 0) {
      output.push({ userId, createdAt, userName: fullName, action, changes: [other] })
    } else {
      const idx = output.findIndex((o) => o.userId === userId && o.createdAt === createdAt && o.action === action)
      if (idx < 0) {
        output.push({ userId, createdAt, userName: fullName, action, changes: [other] })
      } else {
        output[idx].changes.push(other)
      }
    }
  })
  return output
}

type HistoryFilter = {
  type?: string
  page?: string
  limit?: string
  lastestDate?: string
  oldestDate?: string
}
const HISTORY_TABLE = {
  [constant.TYPE_ENUM.COMPLAINT]: HISTORIES.COMPLAINT,
  [constant.TYPE_ENUM.PRO_RETURN]: HISTORIES.PRODUCT_RETURN
}
const USER_FK_ALIAS = {
  [constant.TYPE_ENUM.COMPLAINT]: HISTORIES.COMPLAINT_USER_FK,
  [constant.TYPE_ENUM.PRO_RETURN]: HISTORIES.PR_USER_FK
}
const WATCHER_FK_ALIAS = {
  [constant.TYPE_ENUM.COMPLAINT]: HISTORIES.COMPLAINT_WATCHER_FK,
  [constant.TYPE_ENUM.PRO_RETURN]: HISTORIES.PR_WATCHER_FK
}
const getActivities = async (id: string, filter: HistoryFilter) => {
  const { type = constant.TYPE_ENUM.COMPLAINT, ...other } = filter
  const { limit = DEFAULT_LIMIT, lastestDate, oldestDate } = other
  const model = HISTORY_MODEL[type]
  const table = HISTORY_TABLE[type]
  const userFKAlias = USER_FK_ALIAS[type]
  const watcherFKAlias = WATCHER_FK_ALIAS[type]
  const andOprs: any[] = [{ [HISTORY_ID_FIELD_GET[type]]: utils.convertStrToInt(id) }]
  if (lastestDate) {
    andOprs.push({ [HISTORIES.FIELDS.CREATED_AT]: { [Op.lte]: utils.convertStrToInt(lastestDate) } })
  }
  if (oldestDate) {
    andOprs.push({ [HISTORIES.FIELDS.CREATED_AT]: { [Op.lte]: utils.convertStrToInt(oldestDate) } })
  }
  const limitInt = utils.convertStrToInt(limit)
  const result = await model.findAll({
    where: { [Op.and]: andOprs },
    attributes: [
      HISTORIES.FIELDS.ACTION,
      HISTORIES.FIELDS.ATTACHMENT,
      [HISTORIES.FIELDS.USER_ID, HISTORIES.FIELDS.USER_ID_ALIAS],
      [HISTORIES.FIELDS.CREATED_AT, HISTORIES.FIELDS.CREATED_AT_ALIAS],
      [HISTORIES.FIELDS.FIELD_NAME, HISTORIES.FIELDS.FIELD_NAME_ALIAS],
      [HISTORIES.FIELDS.OLD_VALUE, HISTORIES.FIELDS.OLD_VALUE_ALIAS],
      [HISTORIES.FIELDS.NEW_VALUE, HISTORIES.FIELDS.NEW_VALUE_ALIAS],
      [HISTORIES.FIELDS.WATCHER_ID, HISTORIES.FIELDS.WATCHER_ID_ALIAS],
      [HISTORIES.FIELDS.WATCHER_TYPE, HISTORIES.FIELDS.WATCHER_TYPE_ALIAS],
      [
        concatFields(`"${userFKAlias}"."${USERS.FIELDS.FIRST_NAME}"`, `"${userFKAlias}"."${USERS.FIELDS.LAST_NAME}"`),
        USERS.FIELDS.FULL_NAME
      ],
      [
        concatFields(
          `"${watcherFKAlias}"."${USERS.FIELDS.FIRST_NAME}"`,
          `"${watcherFKAlias}"."${USERS.FIELDS.LAST_NAME}"`
        ),
        HISTORIES.FIELDS.WATCHER_NAME_ALIAS
      ],
      [concatFields(`"${watcherFKAlias}"."${USERS.FIELDS.AVATAR}"`), HISTORIES.FIELDS.WATCHER_AVATAR_ALIAS]
    ],
    include: [
      {
        model: getModel(USERS.TABLE),
        attributes: [],
        as: userFKAlias,
        required: true,
        on: {
          [Op.and]: [
            literal(`"${table}"."${HISTORIES.FIELDS.USER_ID}" = "${userFKAlias}"."${USERS.FIELDS.ID}"`),
            { [USERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          ]
        }
      },
      {
        model: getModel(USERS.TABLE),
        attributes: [],
        as: watcherFKAlias,
        on: {
          [Op.and]: [
            literal(`"${table}"."${HISTORIES.FIELDS.WATCHER_ID}" = "${watcherFKAlias}"."${USERS.FIELDS.ID}"`),
            { [USERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          ]
        }
      }
    ],
    limit: limitInt,
    order: [[HISTORIES.FIELDS.CREATED_AT, DEFAULT_SORT]]
  })
  return processHistoryOutput(result)
}

type SendMailOptions = {
  type: string
  screen: 'CREATE' | 'EDIT' | 'DETAIL'
  createdBy: string | number
  userIds: string | number[]
  recordId?: string | number
  isHistory?: boolean
}
const doSendMail = async (options: SendMailOptions) => {
  const { type, userIds, createdBy, screen, isHistory, recordId } = options
  const users = await UserProfileServices.getUserEmailCountry(userIds)
  users.forEach(async (user) => {
    const to = user.getDataValue(SUPPLIERS.FIELDS.SUPPLIER_EMAIL_TO_ALIAS) || user.email
    if (to) {
      const emailFilter = { type, countryId: user.countryId, role: user.role, screen }
      const mailTemplate = await EmailTemplateServices.getEmailTemplate(emailFilter)
      if (mailTemplate) {
        const cc = user.getDataValue(SUPPLIERS.FIELDS.SUPPLIER_EMAIL_CC_ALIAS)
        const mailOpts = { to, cc, subject: mailTemplate.title, text: mailTemplate.htmlContent }
        try {
          await Mail.sendMail(mailOpts)
          if (isHistory) addHistory({ action: 'MAIL', recordId, type, userId: createdBy, mailOpts })
        } catch (error) {
          console.log('error', error)
        }
      }
    }
  })
}

type ManualMailFilter = { type: string; objId: number }
const getManualMailTemplate = async (countryId: string | number, filter: ManualMailFilter) => {
  const { type, objId } = filter
  switch (type) {
    case constant.TYPE_ENUM.COMPLAINT:
      return ComplaintService.getMailInfo(objId, countryId, type)
    default:
      return {}
  }
}

const sendMailAuto = async (createdBy: string | number, filter: ManualMailFilter) => {
  const { type, objId } = filter
  let userIds: number[] = []
  switch (type) {
    case constant.TYPE_ENUM.COMPLAINT:
      userIds = (await ComplaintWatcherService.getWatcherIds(objId)).map((u) => u.userId)
      break
    case constant.TYPE_ENUM.PRO_RETURN:
      userIds = (await PRWatcherService.getWatcherIds(objId)).map((u) => u.userId)
      break
    default:
      break
  }

  if (userIds.length > 0) doSendMail({ createdBy, screen: 'DETAIL', type, userIds, isHistory: true })
  return true
}

export default {
  getStatusList,
  getCountryList,
  getAttachment,
  getWatcher,
  upLoadFile,
  addAttachmentFile,
  addWatcher,
  addHistory,
  deleteAttachmentFile,
  deleteWatcher,
  handleAddWatcherComplaint,
  handleAddAttachComplaint,
  getSummary,
  getActivities,
  doSendMail,
  getManualMailTemplate,
  sendMailAuto
}
