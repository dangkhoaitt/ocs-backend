import { Op } from 'sequelize'
import { MDVAttributes } from '../models/MDV'
import { COMPLAINTS, MDV_RECORDS, PRODUCTS, SUPPLIERS, USERS } from '../models/ModelConstants'
import { getModel } from '../models/sequelize'

enum MDVStatus {
  MDV_CLOSED = 'MDV_CLOSED'
}
const countMyMDV = async (userId: number, countryId: number) => {
  const model = getModel<MDVAttributes>(MDV_RECORDS.TABLE)
  const complaintModel = getModel(COMPLAINTS.TABLE)
  return {
    mdvCount: await model.count({
      where: {
        [MDV_RECORDS.FIELDS.STATUS]: { [Op.not]: MDVStatus.MDV_CLOSED },
        [MDV_RECORDS.FIELDS.DELETE_FLG]: { [Op.not]: true }
      },
      include: {
        model: complaintModel,
        where: {
          [COMPLAINTS.FIELDS.ASSIGNEE]: userId,
          [COMPLAINTS.FIELDS.COUNTRY_ID]: countryId,
          [COMPLAINTS.FIELDS.DELETE_FLG]: { [Op.not]: true }
        },
        include: [
          {
            model: getModel(USERS.TABLE),
            attributes: [],
            required: true,
            where: { [USERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          },
          {
            model: getModel(SUPPLIERS.TABLE),
            attributes: [],
            required: false,
            where: { [SUPPLIERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          },
          {
            model: getModel(PRODUCTS.TABLE),
            attributes: [],
            required: true,
            where: { [PRODUCTS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          }
        ]
      }
    })
  }
}

export default { countMyMDV }
