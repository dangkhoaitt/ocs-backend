import { FindAttributeOptions, FindOptions, Op, WhereOptions } from 'sequelize'
import { Fn } from 'sequelize/types/lib/utils'
import { concatFields, DATE_FIELDS_REGEX, DEFAULT_MAX_ITEM, DEFAULT_SORT, exportFields } from '../models/base'
import { CRLAttributes } from '../models/CRL'
import { COMPLAINTS, CRL_RECORDS, PRODUCTS, SUPPLIERS, USERS } from '../models/ModelConstants'
import { getModel } from '../models/sequelize'
import ExportService, { ExportModel } from './ExportService'
import utils from './utils'

const model = getModel<CRLAttributes>(CRL_RECORDS.TABLE)
const cplAlias = 'complaint'
const ownerAlias = 'owner'
const recipientAlias = 'recipient'
const assistantAlias = 'assistant'
const userAlias = 'complaint->user'

const { literal } = model.sequelize.Sequelize

export interface GetCRLFilter extends Record<string, string> {
  limit?: string
  page?: string
  sortField?: string
  sortOrder?: string
  keyword?: string
}

enum CRLStatus {
  CRL_CLOSED = 'CRL_CLOSED'
}

const COMPLAINT_FIELDS = [COMPLAINTS.FIELDS.ASSIGNEE, COMPLAINTS.FIELDS.COUNTRY_ID]
const DATE_FIELDS = [CRL_RECORDS.FIELDS.LETTER_DUE_DATE, CRL_RECORDS.FIELDS.RECEIVE_DATE, CRL_RECORDS.FIELDS.SEND_DATE]

const CRL_EXPORT_FIELDS = exportFields(CRL_RECORDS.FIELDS)

const COMPLAINT_EXPORT_FIELDS = [
  COMPLAINTS.FIELDS.COMPLAINT_CODE,
  COMPLAINTS.FIELDS.DESCRIPTION,
  COMPLAINTS.FIELDS.EVENT_DATE,
  COMPLAINTS.FIELDS.BSC_AWARE_DATE,
  COMPLAINTS.FIELDS.JPOS_CIPI,
  COMPLAINTS.FIELDS.ASSIGNEE
]

const countMyCRL = async (userId: number, countryId: number) => {
  const model = getModel<CRLAttributes>(CRL_RECORDS.TABLE)
  const complaintModel = getModel(COMPLAINTS.TABLE)
  return {
    crlCount: await model.count({
      where: {
        [CRL_RECORDS.FIELDS.STATUS]: { [Op.not]: CRLStatus.CRL_CLOSED },
        [CRL_RECORDS.FIELDS.DELETE_FLG]: { [Op.not]: true }
      },
      include: {
        model: complaintModel,
        where: {
          [COMPLAINTS.FIELDS.ASSIGNEE]: userId,
          [COMPLAINTS.FIELDS.COUNTRY_ID]: countryId,
          [COMPLAINTS.FIELDS.DELETE_FLG]: { [Op.not]: true }
        },
        include: [
          {
            model: getModel(USERS.TABLE),
            attributes: [],
            required: true,
            where: { [USERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          },
          {
            model: getModel(SUPPLIERS.TABLE),
            attributes: [],
            required: false,
            where: { [SUPPLIERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          },
          {
            model: getModel(PRODUCTS.TABLE),
            attributes: [],
            required: true,
            where: { [PRODUCTS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          }
        ]
      }
    })
  }
}

function getSearchOptions(keyword: string): WhereOptions {
  if (keyword)
    return {
      [Op.or]: [
        literal(`${cplAlias}."${COMPLAINTS.FIELDS.COMPLAINT_CODE}" ILIKE '%${keyword}%'`),
        literal(`${cplAlias}."${COMPLAINTS.FIELDS.DESCRIPTION}" ILIKE '%${keyword}%'`),
        literal(`"${SUPPLIERS.FIELDS.SUPPLIER_NAME}" ILIKE '%${keyword}%'`)
      ]
    }
  return undefined
}

function getFindOptions(filter: GetCRLFilter): FindOptions {
  const { keyword = '', ...other } = filter
  const filters = []
  const search = getSearchOptions(keyword)
  for (const key in other) {
    if (DATE_FIELDS.includes(key)) {
      const values = other[key].split(',')
      filters.push({
        [Op.and]: [
          { [key]: { [Op.gte]: utils.convertDateStrToTimestamp(values[0]) } },
          { [key]: { [Op.lte]: utils.convertDateStrToTimestamp(values[1], true) } }
        ]
      })
    } else if (COMPLAINT_FIELDS.includes(key)) {
      filters.push(literal(`"${cplAlias}"."${key}" = ${other[key]}`))
    } else filters.push({ [key]: other[key] })
  }
  return {
    where: {
      [Op.and]: [{ [CRL_RECORDS.FIELDS.DELETE_FLG]: { [Op.not]: true } }, ...filters, search]
    },
    include: [
      {
        model: getModel(COMPLAINTS.TABLE),
        include: [
          {
            model: getModel(USERS.TABLE),
            attributes: [],
            as: userAlias,
            required: true
          }
        ],
        attributes: [],
        as: cplAlias,
        required: true
      },
      {
        model: getModel(USERS.TABLE),
        attributes: [],
        as: ownerAlias,
        required: true
      },
      {
        model: getModel(USERS.TABLE),
        attributes: [],
        as: recipientAlias,
        required: true
      },
      {
        model: getModel(USERS.TABLE),
        attributes: [],
        as: assistantAlias,
        required: true
      }
    ]
  }
}

function getOrderBy(sortField: string, sortOrder: string) {
  return literal(
    COMPLAINTS.FIELDS.JPOS_CIPI === sortField || COMPLAINTS.FIELDS.COMPLAINT_CODE === sortField
      ? `"${cplAlias}"."${sortField}" ${sortOrder}`
      : `"${CRL_RECORDS.TABLE}"."${sortField}" ${sortOrder}`
  )
}

function generateCRLExport(data: CRLAttributes[]) {
  for (const row of data) {
    Object.keys(row).forEach((key) => {
      if (DATE_FIELDS_REGEX.test(key)) row[key] = utils.convertTimestampToDateStr(Number(row[key]), 'YYYY-MM-DD HH:mm')
    })
  }
  return data
}

function fullName(alias: string): Fn {
  return concatFields(`"${alias}"."${USERS.FIELDS.FIRST_NAME}"`, `"${alias}"."${USERS.FIELDS.LAST_NAME}"`)
}

function selectExportFieldsQuery(exportFields: string[]): FindAttributeOptions {
  const attributes: FindAttributeOptions = []
  for (const field of exportFields) {
    if (CRL_EXPORT_FIELDS.includes(field)) {
      if (field === CRL_RECORDS.FIELDS.CRL_OWNER) {
        attributes.push([fullName(ownerAlias), CRL_RECORDS.FIELDS.CRL_OWNER])
      } else if (CRL_RECORDS.FIELDS.CRL_RECIPIENT === field) {
        attributes.push([fullName(recipientAlias), CRL_RECORDS.FIELDS.CRL_RECIPIENT])
      } else if (CRL_RECORDS.FIELDS.CRL_ASSISTANT === field) {
        attributes.push([fullName(assistantAlias), CRL_RECORDS.FIELDS.CRL_ASSISTANT])
      } else attributes.push(field)
    } else if (COMPLAINT_EXPORT_FIELDS.includes(field)) {
      if (field === COMPLAINTS.FIELDS.ASSIGNEE) {
        attributes.push([fullName(userAlias), COMPLAINTS.FIELDS.ASSIGNEE])
      } else attributes.push(`${cplAlias}.${field}`)
    }
  }
  return attributes
}

const exportCRL = async (exportFields: string[], query: GetCRLFilter) => {
  const {
    limit = DEFAULT_MAX_ITEM,
    sortField = CRL_RECORDS.FIELDS.CREATED_AT,
    sortOrder = DEFAULT_SORT,
    language = 'en',
    ...filter
  } = query
  const selectExportFields = selectExportFieldsQuery(exportFields)
  if (!selectExportFields) return null
  const limitInt = utils.convertStrToInt(limit)
  const result = await model.findAll({
    ...getFindOptions(filter),
    raw: true,
    attributes: selectExportFields,
    limit: limitInt,
    order: getOrderBy(sortField, sortOrder)
  })
  console.log('result :>> ', result)
  if (result.length === 0) return null

  return ExportService.generateExport({
    model: ExportModel.CRL_RECORDS,
    fields: Object.keys(result[0]),
    data: generateCRLExport(result),
    language
  })
}

export default { countMyCRL, exportCRL }
