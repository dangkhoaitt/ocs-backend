import { Column, Workbook } from 'exceljs'
import fs from 'fs'
import { basename, dirname } from 'path'

export enum ExportModel {
  COMPLAINT = 'complaint',
  PRODUCT_RETURNS = 'product_returns',
  GFE_RECORDS = 'gfe_records',
  CRL_RECORDS = 'crl_records'
}

// prevent required fields form library
type ColumnDefinition = Partial<Column>
type ExportOptions = {
  model: ExportModel
  data: unknown[]
  fields: string[]
  language?: string
}

function getColumns(options: ExportOptions): Column[] {
  const { fields, model, language } = options
  const locales = basename(dirname(__dirname)) + `/export/locales/${language.toUpperCase()}/${model}.json`
  const content: Record<string, string> = JSON.parse(fs.readFileSync(locales).toString())
  const columns: ColumnDefinition[] = []
  for (const field of fields) {
    columns.push({ header: content[field] || field, key: field })
  }
  return columns as Column[]
}

const generateExport = async (options: ExportOptions) => {
  const { model, language, data } = options
  const template = basename(dirname(__dirname)) + `/export/templates/${model}/${language.toUpperCase()}/${model}.xlsx`
  const statusFile = basename(dirname(__dirname)) + `/export/locales/${language.toUpperCase()}/status.json`
  const status: Record<string, string> = JSON.parse(fs.readFileSync(statusFile).toString())
  const workbook = await new Workbook().xlsx.readFile(template)

  const templateSheet = workbook.getWorksheet('Sheet1')
  const headerStyle = templateSheet.getRow(1).getCell(1).style
  const cellStyle = templateSheet.getRow(2).getCell(1).style
  const workSheet = workbook.addWorksheet(`${model}`)
  workSheet.columns = getColumns(options)
  workSheet.addRows(data)
  workSheet.eachRow((row, index) => {
    index === 1
      ? row.eachCell((cell) => (cell.style = headerStyle))
      : row.eachCell((cell) => {
          cell.value = status[`${cell.value}`] || cell.value
          cell.style = cellStyle
        })
  })
  workbook.removeWorksheet(1)
  return await workbook.xlsx.writeBuffer()
}

export default { generateExport }
