import aws from 'aws-sdk'
import config from 'config'
import moment from 'moment'
import multer from 'multer'
import multerS3 from 'multer-s3'
import mode from '../../config'
import constant from '../../constant/constant'
const s3 = new aws.S3({
  accessKeyId: config.get(`${mode}.s3.accessKeyId`),
  secretAccessKey: config.get(`${mode}.s3.secretAccessKey`)
})

function buildDateValue(date: string): string {
  return `('${date}'::date + '1 day'::interval)`
}

function buildOrderBy(sortType: string, alias: string): string {
  return `\n ORDER BY "${alias}"."${sortType}" `
}

function buildLimit(limit: number, offset: number): string {
  return `\n LIMIT ${limit} OFFSET ${offset}`
}

function convertStrToInt(str: string): number {
  return /^\d+$/.test(str) ? parseInt(str, 10) : 1
}

function upload() {
  const storage = multerS3({
    s3,
    bucket: config.get(`${mode}.s3.bucket`),
    acl: 'public-read',
    metadata(req, file, cb) {
      cb(null, { fieldName: file.fieldname })
    },
    key(req, file, cb) {
      cb(null, file.originalname)
    }
  })

  const uploadFile = multer({
    storage,
    limits: { fileSize: constant.MAXSIZE }
  }).array('fileUploads', 12)

  return uploadFile
}

function deleteFileFromS3(params: any) {
  s3.deleteObject(params, function (err, data) {
    if (err) console.log(err, err.stack)
    // an error occurred
    else console.log(data) // successful response
  })
}

function convertDateStrToTimestamp(date: string, end = false, formatter = 'YYYY-MM-DD'): number {
  const momentDate = moment(date, formatter, true)
  if (!momentDate.isValid()) return -1
  const time = end ? momentDate.endOf('day').valueOf() : momentDate.valueOf()
  return Math.floor(time / 1000)
}

function getCurrentTime(): number {
  return Math.floor(Date.now() / 1000)
}

function convertTimestampToDateStr(timestamp: number, formatter = 'YYYY-MM-DD'): string {
  return moment.unix(timestamp).format(formatter)
}

function generateCode(code?: string) {
  let strCode = String(1).padStart(constant.OCS_RUNNING_NUMBER, '0')
  if (code) {
    strCode = code.substr(code.lastIndexOf('-') + 1, code.length)
    strCode = String(+strCode + 1).padStart(constant.OCS_RUNNING_NUMBER, '0')
  }
  return constant.OCS_PREFIX + '-' + strCode
}

const utils = {
  buildDateValue,
  buildOrderBy,
  buildLimit,
  convertStrToInt,
  convertDateStrToTimestamp,
  upload,
  getCurrentTime,
  convertTimestampToDateStr,
  generateCode,
  deleteFileFromS3
}
export default utils
