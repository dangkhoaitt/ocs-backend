import { Op } from 'sequelize'
import { DEFAULT_LIMIT, DEFAULT_SORT } from '../models/base'
import { PRODUCT_RETURN_HISTORIES, USERS } from '../models/ModelConstants'
import { getModel } from '../models/sequelize'
import utils from './utils'

const model = getModel(PRODUCT_RETURN_HISTORIES.TABLE)

type HistoryFilter = { limit?: string; lastestDate?: string; oldestDate?: string }
const getHistories = async (id: string, filter: HistoryFilter) => {
  const { limit = DEFAULT_LIMIT, lastestDate, oldestDate } = filter
  const andOprs: any[] = [{ [PRODUCT_RETURN_HISTORIES.FIELDS.PR_ID]: utils.convertStrToInt(id) }]
  if (lastestDate) {
    andOprs.push({ [PRODUCT_RETURN_HISTORIES.FIELDS.CREATED_AT]: { [Op.lte]: utils.convertStrToInt(lastestDate) } })
  }
  if (oldestDate) {
    andOprs.push({ [PRODUCT_RETURN_HISTORIES.FIELDS.CREATED_AT]: { [Op.lte]: utils.convertStrToInt(oldestDate) } })
  }
  const limitInt = utils.convertStrToInt(limit)
  return model.findAll({
    where: { [Op.and]: andOprs },
    attributes: [PRODUCT_RETURN_HISTORIES.FIELDS.ACTION],
    include: {
      model: getModel(USERS.TABLE),
      attributes: []
    },
    limit: limitInt,
    order: [[PRODUCT_RETURN_HISTORIES.FIELDS.CREATED_AT, DEFAULT_SORT]]
    // group: [PRODUCT_RETURN_HISTORIES.FIELDS.USER_ID]
  })
}
export default { getHistories }
