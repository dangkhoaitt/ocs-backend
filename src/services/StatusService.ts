import { STATUS } from '../models/ModelConstants'
import { getModel } from '../models/sequelize'
import { StatusAttributes } from '../models/Status'

const model = getModel<StatusAttributes>(STATUS.TABLE)
const checkStatusExist = (code: string) => {
  return model.count({ where: { code } })
}

export default { checkStatusExist }
