import { col, fn, Op } from 'sequelize'
import { findOneOptions } from '../models/base'
import { GfeWatcherAttributes } from '../models/GfeWatcher'
import { GFE_WATCHERS, USERS } from '../models/ModelConstants'
import { getModel } from '../models/sequelize'
import { UserAttributes } from '../models/User'

const gfeWatcherModel = getModel<GfeWatcherAttributes>(GFE_WATCHERS.TABLE)
const addWatcher = async (input: { objId: number; userId: number }, isPRCreate?: boolean) => {
  if (isPRCreate) return gfeWatcherModel.create({ gfeId: input.objId, userId: input.userId })

  const userModel = getModel<UserAttributes>(USERS.TABLE)
  const dataWatcher = await gfeWatcherModel.create({ gfeId: input.objId, userId: input.userId })
  const userInfo = await userModel.findOne({ where: { id: input.userId } })
  dataWatcher.setDataValue(USERS.FIELDS.AVATAR, userInfo?.avatar)
  dataWatcher.setDataValue(USERS.FIELDS.FULL_NAME, userInfo?.fullName)
  dataWatcher.setDataValue(USERS.FIELDS.EMAIL, userInfo?.email)
  return dataWatcher
}

const getWatcher = async (gfeId: number) => {
  const userModel = getModel<UserAttributes>(USERS.TABLE)
  const alias = 'user'
  return gfeWatcherModel.findAll({
    raw: true,
    where: {
      [GFE_WATCHERS.FIELDS.GFE_ID]: gfeId,
      [GFE_WATCHERS.FIELDS.DELETE_FLG]: { [Op.not]: true }
    },
    attributes: [
      GFE_WATCHERS.FIELDS.ID,
      `${alias}.${USERS.FIELDS.AVATAR}`,
      `${alias}.${USERS.FIELDS.EMAIL}`,
      [fn('CONCAT', col(USERS.FIELDS.FIRST_NAME), ' ', col(USERS.FIELDS.LAST_NAME)), USERS.FIELDS.FULL_NAME]
    ],
    include: { as: alias, model: userModel, where: { [USERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }, attributes: [] }
  })
}

const deleteWatcher = async (id: number) => {
  const watcher = await gfeWatcherModel.findOne({
    where: findOneOptions({ id }),
    attributes: [
      [GFE_WATCHERS.FIELDS.GFE_ID, GFE_WATCHERS.FIELDS.GFE_ID_ALIAS],
      [GFE_WATCHERS.FIELDS.USER_ID, GFE_WATCHERS.FIELDS.USER_ID_ALIAS]
    ]
  })
  if (watcher) {
    await gfeWatcherModel.destroy({ where: { [GFE_WATCHERS.FIELDS.ID]: id } })
  }
  return watcher
}

export default { addWatcher, getWatcher, deleteWatcher }
