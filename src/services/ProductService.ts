import { Op } from 'sequelize'
import { DEFAULT_LIMIT } from '../models/base'
import { PRODUCTS } from '../models/ModelConstants'
import { ProductAttributes } from '../models/Product'
import { getModel } from '../models/sequelize'
import utils from './utils'

const model = getModel<ProductAttributes>(PRODUCTS.TABLE)

const getProductSuggestionList = async (keyword: string, isDefault?: boolean) => {
  const filter = isDefault
    ? { [PRODUCTS.FIELDS.PRODUCT_ID]: { [Op.in]: keyword.split(',') } }
    : {[Op.or]: [
        {[PRODUCTS.FIELDS.PRODUCT_NAME]: { [Op.iLike]: `%${keyword}%` }},
        {[PRODUCTS.FIELDS.UPN]: { [Op.iLike]: `%${keyword}%` }},
        {[PRODUCTS.FIELDS.MODEL_NUMBER]: { [Op.iLike]: `%${keyword}%` }},
        {[PRODUCTS.FIELDS.CATALOG_NUMBER]: { [Op.iLike]: `%${keyword}%` }}
      ]
  }
  return model.findAll({
    where: Object.assign(filter, { [PRODUCTS.FIELDS.DELETE_FLG]: { [Op.not]: true } }),
    limit: utils.convertStrToInt(DEFAULT_LIMIT)
  }).then(function(result){
    return result.map(function(item){
      return {
        'id':item.productId,
        'value':item
      }
    });
  });
}

export default {
  getProductSuggestionList
}
