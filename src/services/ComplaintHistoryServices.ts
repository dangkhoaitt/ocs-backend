import { Op } from 'sequelize'
import { concatFields, DEFAULT_LIMIT, DEFAULT_SORT } from '../models/base'
import { ComplainHistoryAttributes } from '../models/ComplaintHistory'
import { COMPLAINT_HISTORIES, USERS } from '../models/ModelConstants'
import { getModel } from '../models/sequelize'
import utils from './utils'

/**
 * @api {post} /api/complaint-histories Create complaint history
 * @apiName CreateComplaintHistory
 * @apiGroup Complaint
 *
 * @apiParam {String} reporter Exp: admin *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "version": "1.0",
 *        "statusCode": 200,
 *        "messageCode": "SUCCESS",
 *        "message": "Request successful."
 *      }
 */

/**
 * @api {get} /api/complaints Get list complaint
 * @apiName GetList
 * @apiGroup Complaint
 *
 * @apiParam {String} reporter Exp: admin
 * @apiParam {String} assigned Exp: alex
 * @apiParam {String} created Exp: 2020-12-07
 * @apiParam {String} type Exp: {TYPE_ENUM}
 * @apiParam {Number} status StatusID
 * @apiParam {Number} country country code
 * @apiParam {Number} limit number
 * @apiParam {Number} page number
 * @apiParam {String} sortField field to sort
 * @apiParam {String} sortOrder ASC or DESC
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "version": "1.0",
 *        "statusCode": 200,
 *        "messageCode": "SUCCESS",
 *        "message": "Request successful.",
 *        "data":[
 *                  {
 *                    "complaint_id":1,
 *                    "status_name":"NEW",
 *                    "status_id":1,
 *                    "reporter":"Hai Tran",
 *                    "assigned":"Alex",
 *                    "created_at":"2020-12-07 16:10",
 *                    "country":"China",
 *                    "product_name":"Machine",
 *                    "mdv_id": 1,
 *                    "product_return_id":1,
 *                    "crl_id":1,
 *                    "gpe_id":1
 *                  }
 *              ],
 *        "limit": 10,
 *        "page": 1,
 *        "total": 6
 *      }
 */

const model = getModel<ComplainHistoryAttributes>(COMPLAINT_HISTORIES.TABLE)
const { literal } = model.sequelize.Sequelize
const USER_ALIAS = 'createdByUser'
const WATCHER_ALIAS = 'watcherUser'

type HistoryFilter = { limit?: string; lastestDate?: string; oldestDate?: string }

const getList = async (id: string, filter: HistoryFilter) => {
  const { limit = DEFAULT_LIMIT, lastestDate, oldestDate } = filter
  const andOprs: any[] = [{ [COMPLAINT_HISTORIES.FIELDS.COMPLAINT_ID]: utils.convertStrToInt(id) }]
  if (lastestDate) {
    andOprs.push({ [COMPLAINT_HISTORIES.FIELDS.CREATED_AT]: { [Op.lte]: utils.convertStrToInt(lastestDate) } })
  }
  if (oldestDate) {
    andOprs.push({ [COMPLAINT_HISTORIES.FIELDS.CREATED_AT]: { [Op.lte]: utils.convertStrToInt(oldestDate) } })
  }
  const limitInt = utils.convertStrToInt(limit)
  return model.findAll({
    where: { [Op.and]: andOprs },
    attributes: [
      COMPLAINT_HISTORIES.FIELDS.ACTION,
      [COMPLAINT_HISTORIES.FIELDS.USER_ID, COMPLAINT_HISTORIES.FIELDS.USER_ID_ALIAS],
      [COMPLAINT_HISTORIES.FIELDS.CREATED_AT, COMPLAINT_HISTORIES.FIELDS.CREATED_AT_ALIAS],
      [COMPLAINT_HISTORIES.FIELDS.FIELD_NAME, COMPLAINT_HISTORIES.FIELDS.FIELD_NAME_ALIAS],
      [COMPLAINT_HISTORIES.FIELDS.OLD_VALUE, COMPLAINT_HISTORIES.FIELDS.OLD_VALUE_ALIAS],
      [COMPLAINT_HISTORIES.FIELDS.NEW_VALUE, COMPLAINT_HISTORIES.FIELDS.NEW_VALUE_ALIAS],
      [COMPLAINT_HISTORIES.FIELDS.WATCHER_ID, COMPLAINT_HISTORIES.FIELDS.WATCHER_ID_ALIAS],
      [COMPLAINT_HISTORIES.FIELDS.WATCHER_TYPE, COMPLAINT_HISTORIES.FIELDS.WATCHER_TYPE_ALIAS],
      [
        concatFields(`"${USER_ALIAS}"."${USERS.FIELDS.FIRST_NAME}"`, `"${USER_ALIAS}"."${USERS.FIELDS.LAST_NAME}"`),
        USERS.FIELDS.FULL_NAME
      ],
      [
        concatFields(
          `"${WATCHER_ALIAS}"."${USERS.FIELDS.FIRST_NAME}"`,
          `"${WATCHER_ALIAS}"."${USERS.FIELDS.LAST_NAME}"`
        ),
        COMPLAINT_HISTORIES.FIELDS.WATCHER_NAME_ALIAS
      ],
      [concatFields(`"${WATCHER_ALIAS}"."${USERS.FIELDS.AVATAR}"`), COMPLAINT_HISTORIES.FIELDS.WATCHER_AVATAR_ALIAS]
    ],
    include: [
      {
        model: getModel(USERS.TABLE),
        attributes: [],
        as: USER_ALIAS,
        required: true,
        on: {
          [Op.and]: [
            literal(
              `"${COMPLAINT_HISTORIES.TABLE}"."${COMPLAINT_HISTORIES.FIELDS.USER_ID}" = "${USER_ALIAS}"."${USERS.FIELDS.ID}"`
            ),
            { [USERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          ]
        }
      },
      {
        model: getModel(USERS.TABLE),
        attributes: [],
        as: WATCHER_ALIAS,
        on: {
          [Op.and]: [
            literal(
              `"${COMPLAINT_HISTORIES.TABLE}"."${COMPLAINT_HISTORIES.FIELDS.WATCHER_ID}" = "${WATCHER_ALIAS}"."${USERS.FIELDS.ID}"`
            ),
            { [USERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          ]
        }
      }
    ],
    limit: limitInt,
    order: [[COMPLAINT_HISTORIES.FIELDS.CREATED_AT, DEFAULT_SORT]]
  })
}

export default { getList }
