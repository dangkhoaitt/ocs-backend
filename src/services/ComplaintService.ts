import { FindAttributeOptions, FindOptions, Op, WhereOptions } from 'sequelize'
import { Literal } from 'sequelize/types/lib/utils'
import constant from '../constant/constant'
import { concatFields, DEFAULT_LIMIT, DEFAULT_MAX_ITEM, DEFAULT_PAGE, DEFAULT_SORT } from '../models/base'
import { ComplaintAttributes, GET_LIST_QUERY_STATUS } from '../models/Complaint'
import { EmailTemplateAttributes } from '../models/EmailTemplate'
import {
  COMPLAINTS,
  COUNTRIES,
  CRL_RECORDS,
  EMAIL_TEMPLATE,
  GFE_RECORDS,
  MDV_RECORDS,
  PRODUCTS,
  PRODUCT_RETURNS,
  STATUS,
  SUGGESTION,
  SUPPLIERS,
  USERS
} from '../models/ModelConstants'
import { ProductAttributes } from '../models/Product'
import { executeQuery, getModel } from '../models/sequelize'
import { StatusAttributes } from '../models/Status'
import CommonService from './CommonService'
import ComplaintWatcherService from './ComplaintWatcherService'
import EmailTemplateServices from './EmailTemplateServices'
import ExportService, { ExportModel } from './ExportService'
import { default as UserProfileServices, default as userService } from './UserProfileServices'
import utils from './utils'

/**
 * @api {post} /api/complaints Create complaint
 * @apiName CreateComplaint
 * @apiGroup Complaint
 *
 * @apiParam {String} reporter Exp: admin *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "version": "1.0",
 *        "statusCode": 200,
 *        "messageCode": "SUCCESS",
 *        "message": "Request successful."
 *      }
 */

/**
 * @api {get} /api/complaints Get list complaint
 * @apiName GetList
 * @apiGroup Complaint
 *
 * @apiParam {String} reporter Exp: admin
 * @apiParam {String} assigned Exp: alex
 * @apiParam {String} created Exp: 2020-12-07
 * @apiParam {String} type Exp: {TYPE_ENUM}
 * @apiParam {Number} status StatusID
 * @apiParam {Number} country country code
 * @apiParam {Number} limit number
 * @apiParam {Number} page number
 * @apiParam {String} sortField field to sort
 * @apiParam {String} sortOrder ASC or DESC
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "version": "1.0",
 *        "statusCode": 200,
 *        "messageCode": "SUCCESS",
 *        "message": "Request successful.",
 *        "data":[
 *                  {
 *                    "complaint_id":1,
 *                    "status_name":"NEW",
 *                    "status_id":1,
 *                    "reporter":"Hai Tran",
 *                    "assigned":"Alex",
 *                    "created_at":"2020-12-07 16:10",
 *                    "country":"China",
 *                    "product_name":"Machine",
 *                    "mdv_id": 1,
 *                    "product_return_id":1,
 *                    "crl_id":1,
 *                    "gpe_id":1
 *                  }
 *              ],
 *        "limit": 10,
 *        "page": 1,
 *        "total": 6
 *      }
 */

/**
 * @api {get} /api/product-return Get list product return
 * @apiName GetList
 * @apiGroup ProductReturn
 *
 * @apiParam {String} reporter Exp: admin
 * @apiParam {String} assigned Exp: alex
 * @apiParam {String} created Exp: 2020-12-07
 * @apiParam {String} type Exp: {TYPE_ENUM}
 * @apiParam {Number} status StatusID
 * @apiParam {Number} country country code
 * @apiParam {Number} limit number
 * @apiParam {Number} page number
 * @apiParam {String} sortField field to sort
 * @apiParam {String} sortOrder ASC or DESC
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "version": "1.0",
 *        "statusCode": 200,
 *        "messageCode": "SUCCESS",
 *        "message": "Request successful.",
 *        "data":[
 *                  {
 *                    "complaint_id":1,
 *                    "status_name":"NEW",
 *                    "status_id":1,
 *                    "reporter":"Hai Tran",
 *                    "assigned":"Alex",
 *                    "created_at":"2020-12-07 16:10",
 *                    "country":"China",
 *                    "product_name":"Machine",
 *                    "mdv_id": 1,
 *                    "product_return_id":1,
 *                    "crl_id":1,
 *                    "gpe_id":1
 *                  }
 *              ],
 *        "limit": 10,
 *        "page": 1,
 *        "total": 6
 *      }
 */

export interface GetListInput extends Record<string, string> {
  createdAt?: string
  limit?: string
  page?: string
  sortField?: string
  sortOrder?: string
  getAll?: string
  keyword?: string
}
type GetListOutput = { total: number; page: number; data: unknown[]; limit?: number }

const COUNTRY_ALIAS = 'country'
const PR_ALIAS = 'product_return_record'
const CRL_ALIAS = 'crl_record'
const GFE_ALIAS = 'gfe_record'
const MDV_ALIAS = 'mdv_record'
const PRODUCT_ALIAS = 'product'
const USER_ALIAS = 'user'
const SUPPLIER_ALIAS = 'supplier'
const COMPLAINT_ALIAS = COMPLAINTS.TABLE

const COMPLAINT_ATTRIBUTES: FindAttributeOptions = [
  COMPLAINTS.FIELDS.STATUS,
  [COMPLAINTS.FIELDS.COMPLAINT_ID, COMPLAINTS.FIELDS.COMPLAINT_ID_ALIAS],
  [COMPLAINTS.FIELDS.COMPLAINT_CODE, COMPLAINTS.FIELDS.COMPLAINT_CODE_ALIAS],
  [COMPLAINTS.FIELDS.JPOS_CIPI, COMPLAINTS.FIELDS.JPOS_CIPI_ALIAS],
  [COMPLAINTS.FIELDS.CREATED_AT, COMPLAINTS.FIELDS.CREATED_AT_ALIAS],
  [concatFields(`${PRODUCT_ALIAS}.${PRODUCTS.FIELDS.DIVISION}`), PRODUCTS.FIELDS.DIVISION],
  [concatFields(`${PRODUCT_ALIAS}.${PRODUCTS.FIELDS.PRODUCT_NAME}`), PRODUCTS.FIELDS.PRODUCT_NAME_ALIAS],
  [
    concatFields(`${USER_ALIAS}.${USERS.FIELDS.FIRST_NAME}`, `${USER_ALIAS}.${USERS.FIELDS.LAST_NAME}`),
    COMPLAINTS.FIELDS.ASSIGNEE
  ],
  [concatFields(`${COUNTRY_ALIAS}.${COUNTRIES.FIELDS.COUNTRY_NAME}`), COUNTRIES.FIELDS.COUNTRY_NAME_ALIAS],
  [concatFields(`${SUPPLIER_ALIAS}.${SUPPLIERS.FIELDS.SUPPLIER_NAME}`), SUPPLIERS.FIELDS.SUPPLIER_NAME_ALIAS],
  [concatFields(`${MDV_ALIAS}.${MDV_RECORDS.FIELDS.STATUS}`), MDV_RECORDS.FIELDS.STATUS_ALIAS],
  [concatFields(`${PR_ALIAS}.${PRODUCT_RETURNS.FIELDS.PR_STATUS}`), PRODUCT_RETURNS.FIELDS.PR_STATUS_ALIAS],
  [concatFields(`${CRL_ALIAS}.${CRL_RECORDS.FIELDS.STATUS}`), CRL_RECORDS.FIELDS.STATUS_ALIAS],
  [concatFields(`${GFE_ALIAS}.${GFE_RECORDS.FIELDS.STATUS}`), GFE_RECORDS.FIELDS.STATUS_ALIAS]
]
const WHERE_IN_FIELDS = [COMPLAINTS.FIELDS.SUPPLIER_ID, COMPLAINTS.FIELDS.ASSIGNEE]
const DATE_FIELDS = [
  COMPLAINTS.FIELDS.CREATED_AT,
  COMPLAINTS.FIELDS.OEM_NOTIFICATION_DATE,
  COMPLAINTS.FIELDS.COMPLANT_REVIEW_DATE
]

const COMPLAINT_EXPORT_FIELDS = exportFields(COMPLAINTS.FIELDS)
const PRODUCT_EXPORT_FIELDS = exportFields(PRODUCTS.FIELDS)
const OTHER_EXPORT_FIELDS = [SUPPLIERS.FIELDS.SUPPLIER_ID, SUPPLIERS.FIELDS.SUPPLIER_NAME]
const DATE_FIELDS_REGEX = /(?:_at|date)/

const model = getModel<ComplaintAttributes>(COMPLAINTS.TABLE)
const { literal } = model.sequelize.Sequelize

function getAlias(key: string): string {
  if (PRODUCT_RETURNS.FIELDS.PR_STATUS === key) return PR_ALIAS
  if (CRL_RECORDS.FIELDS.STATUS === key) return CRL_ALIAS
  if (GFE_RECORDS.FIELDS.STATUS === key) return GFE_ALIAS
  if (MDV_RECORDS.FIELDS.STATUS === key) return MDV_ALIAS
  if (PRODUCTS.FIELDS.DIVISION === key) return PRODUCT_ALIAS
  return COMPLAINT_ALIAS
}

function getSearchOptions(keyword: string): WhereOptions {
  if (keyword)
    return {
      [Op.or]: [
        literal(`"${COMPLAINTS.FIELDS.COMPLAINT_CODE}" ILIKE '%${keyword}%'`),
        literal(`"${COMPLAINTS.FIELDS.DESCRIPTION}" ILIKE '%${keyword}%'`),
        literal(`"${COMPLAINTS.FIELDS.FACILITY_NAME}" ILIKE '%${keyword}%'`),
        literal(`"${COMPLAINTS.FIELDS.JPOS_CIPI}" ILIKE '%${keyword}%'`),
        literal(`${SUPPLIER_ALIAS}."${SUPPLIERS.FIELDS.SUPPLIER_NAME}" ILIKE '%${keyword}%'`),
        literal(`${PRODUCT_ALIAS}."${PRODUCTS.FIELDS.PRODUCT_NAME}" ILIKE '%${keyword}%'`),
        literal(`${PRODUCT_ALIAS}."${PRODUCTS.FIELDS.UPN}" ILIKE '%${keyword}%'`),
        literal(`${PRODUCT_ALIAS}."${PRODUCTS.FIELDS.BATCH}" ILIKE '%${keyword}%'`),
        literal(`${PRODUCT_ALIAS}."${PRODUCTS.FIELDS.LOT}" ILIKE '%${keyword}%'`)
      ]
    }
  return undefined
}

function getFindOptions(filter: GetListInput): FindOptions {
  const { keyword = '', ...other } = filter
  const filters = []
  const search = getSearchOptions(keyword)
  for (const key in other) {
    if (WHERE_IN_FIELDS.includes(key)) filters.push(literal(`"${COMPLAINT_ALIAS}"."${key}" IN (${other[key]})`))
    else if (DATE_FIELDS.includes(key)) {
      const values = other[key].split(',')
      filters.push({
        [Op.and]: [
          { [key]: { [Op.gte]: utils.convertDateStrToTimestamp(values[0]) } },
          { [key]: { [Op.lte]: utils.convertDateStrToTimestamp(values[1], true) } }
        ]
      })
    } else {
      filters.push(literal(`"${getAlias(key)}"."${key}" = '${other[key]}'`))
    }
  }
  return {
    where: {
      [Op.and]: [{ [PRODUCT_RETURNS.FIELDS.DELETE_FLG]: { [Op.not]: true } }, ...filters, search]
    },
    include: [
      {
        model: getModel(COUNTRIES.TABLE),
        attributes: [],
        required: true,
        where: { [COUNTRIES.FIELDS.DELETE_FLG]: { [Op.not]: true } }
      },
      {
        model: getModel(USERS.TABLE),
        attributes: [],
        required: true,
        where: { [USERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
      },
      {
        model: getModel(SUPPLIERS.TABLE),
        attributes: [],
        required: false,
        where: { [SUPPLIERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
      },
      {
        model: getModel(PRODUCTS.TABLE),
        attributes: [],
        required: true,
        where: { [PRODUCTS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
      },
      {
        model: getModel(PRODUCT_RETURNS.TABLE),
        attributes: [],
        required: false,
        where: { [PRODUCT_RETURNS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
      },
      {
        model: getModel(CRL_RECORDS.TABLE),
        attributes: [],
        required: false,
        where: { [CRL_RECORDS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
      },
      {
        model: getModel(MDV_RECORDS.TABLE),
        attributes: [],
        required: false,
        where: { [MDV_RECORDS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
      },
      {
        model: getModel(GFE_RECORDS.TABLE),
        attributes: [],
        required: false,
        where: { [GFE_RECORDS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
      }
    ]
  }
}

function getOrderBy(sortField: string, sortOrder: string): Literal {
  return literal(
    PRODUCTS.FIELDS.DIVISION === sortField
      ? `"${PRODUCT_ALIAS}"."${sortField}" ${sortOrder}`
      : `"${COMPLAINTS.TABLE}"."${sortField}" ${sortOrder}`
  )
}

const getList = async (query: GetListInput): Promise<GetListOutput> => {
  const {
    limit = DEFAULT_LIMIT,
    page = DEFAULT_PAGE,
    sortField = COMPLAINTS.FIELDS.CREATED_AT,
    sortOrder = DEFAULT_SORT,
    ...filter
  } = query
  const limitInt = utils.convertStrToInt(limit)
  const pageInt = utils.convertStrToInt(page)

  const [total, data] = await Promise.all([
    model.count({ ...getFindOptions(filter) }),
    model.findAll({
      ...getFindOptions(filter),
      attributes: COMPLAINT_ATTRIBUTES,
      raw: true,
      limit: limitInt,
      offset: (pageInt - 1) * limitInt,
      order: getOrderBy(sortField, sortOrder)
    })
  ])
  return { data, total, page: pageInt, limit: limitInt }
}

function exportFields(modelFields: Record<string, string>) {
  const fields: string[] = []
  for (const key in modelFields) {
    if (!key.includes('ALIAS')) fields.push(modelFields[key])
  }
  return fields
}

function selectExportFieldsQuery(exportFields: string[]): FindAttributeOptions {
  const attributes: FindAttributeOptions = []
  for (const field of exportFields) {
    if (COMPLAINT_EXPORT_FIELDS.includes(field) && COMPLAINTS.FIELDS.ASSIGNEE !== field)
      attributes.push(`${COMPLAINT_ALIAS}.${field}`)
    else if (PRODUCT_EXPORT_FIELDS.includes(field)) attributes.push(`${PRODUCT_ALIAS}.${field}`)
    else if (OTHER_EXPORT_FIELDS.includes(field)) attributes.push(`${SUPPLIER_ALIAS}.${field}`)
    else if (COUNTRIES.FIELDS.COUNTRY_NAME === field) attributes.push(`${COUNTRY_ALIAS}.${field}`)
    else if (COMPLAINTS.FIELDS.ASSIGNEE === field)
      attributes.push([
        concatFields(`${USER_ALIAS}.${USERS.FIELDS.FIRST_NAME}`, `${USER_ALIAS}.${USERS.FIELDS.LAST_NAME}`),
        field
      ])
  }
  return attributes
}

const generateComplaintExport = (data: ComplaintAttributes[]) => {
  for (const row of data) {
    Object.keys(row).forEach((key) => {
      if (DATE_FIELDS_REGEX.test(key) && COMPLAINTS.FIELDS.USED_BEFORE_EXPIRY_DATE !== key)
        row[key] = utils.convertTimestampToDateStr(Number(row[key]))
    })
  }
  return data
}

const getExportList = async (exportFields: string[], query?: GetListInput) => {
  const {
    limit = DEFAULT_MAX_ITEM,
    sortField = COMPLAINTS.FIELDS.CREATED_AT,
    sortOrder = DEFAULT_SORT,
    ...filter
  } = query
  const selectExportFields = selectExportFieldsQuery(exportFields)
  if (!selectExportFields) return null
  const limitInt = utils.convertStrToInt(limit)
  const result = await model.findAll({
    ...getFindOptions(filter),
    raw: true,
    attributes: selectExportFieldsQuery(exportFields),
    limit: limitInt,
    order: getOrderBy(sortField, sortOrder)
  })
  if (result.length === 0) return null
  return ExportService.generateExport({
    model: ExportModel.COMPLAINT,
    fields: Object.keys(result[0]),
    data: generateComplaintExport(result as ComplaintAttributes[]),
    language: 'en'
  })
}

const getDetail = async (complaintId: number) => {
  const statusModel = getModel<StatusAttributes>(STATUS.TABLE)
  const productModel = getModel<ProductAttributes>(PRODUCTS.TABLE)
  const result = await model.findOne({ where: { complaint_id: complaintId } })

  if (result) {
    const status = await statusModel.findOne({
      where: { code: result.statusCode }
    })
    const productDetail = await productModel.findOne({ where: { product_id: result.productId } })
    const assigneeInfo = await userService.getUserById(result.assignee)
    if (assigneeInfo) {
      result.setDataValue('assigneeObj', {
        fullName: assigneeInfo.fullName,
        email: assigneeInfo.email,
        role: assigneeInfo.role,
        avatar: assigneeInfo.avatar,
        type: null
      })
    }
    // set data
    result.setDataValue('statusColor', status.colorHex)
    result.setDataValue('statusType', status.type)
    result.setDataValue('productDetail', productDetail)
  }
  return result
}

const sendMailToWatchers = async (createdBy: string | number, id: number) => {
  const watchersIds = await ComplaintWatcherService.getWatcherIds(id)
  const userIds = watchersIds.map((w) => w.userId)

  // send email to watchers
  const type = constant.TYPE_ENUM.COMPLAINT
  CommonService.doSendMail({ createdBy, screen: 'EDIT', type, userIds, recordId: id, isHistory: true })
}

const updateOrCreate = async (userId: string, input: Record<string, any>, complaintId: number = null) => {
  const foundItem = await model.findOne({ where: { complaint_id: complaintId } })
  const dataComplaint = input.detail
  if (!foundItem) {
    const complaintCode = await checkExistCode()
    dataComplaint.complaintCode = complaintCode

    // Item not found, create a new one
    const item = await model.create(dataComplaint)
    const complaintId = item.getDataValue('complaintId')
    if (item.complaintId) {
      CommonService.addHistory({
        action: 'CREATE',
        recordId: item.complaintId,
        type: constant.TYPE_ENUM.COMPLAINT,
        userId
      })
    }

    if (item && input.watchers) {
      const idUsers = []
      for (const item of input.watchers) {
        await CommonService.handleAddWatcherComplaint({ objId: complaintId, userId: item.userId })
        idUsers.push(item.userId)
      }
      const type = constant.TYPE_ENUM.COMPLAINT
      CommonService.doSendMail({ createdBy: userId, screen: 'CREATE', type, userIds: idUsers })
    }

    if (item && input.attachments) {
      for (const item of input.attachments) {
        await CommonService.handleAddAttachComplaint({
          objId: complaintId,
          fileName: item.fileName,
          fileType: item.fileType,
          filePath: item.filePath
        })
      }
    }
    return item
  }

  // Found an item, update it
  const item = await model.update(dataComplaint, { where: { complaint_id: complaintId } })
  if (item) {
    CommonService.addHistory({
      action: 'EDIT',
      recordId: complaintId,
      type: constant.TYPE_ENUM.COMPLAINT,
      userId,
      body: dataComplaint,
      detail: foundItem
    })
    sendMailToWatchers(userId, complaintId)
    return complaintId
  }
}

const cloneComplaint = async (complaintId: number) => {
  const result = await model.findOne({ where: { complaint_id: complaintId } })
  if (!result) {
    return false
  }
  const complaintCode = await checkExistCode()
  result.complaintCode = complaintCode
  const dataComplaint = result.dataValues as any
  delete dataComplaint.complaintId
  const item = await model.create(result.dataValues)
  return item.complaintId
}

const checkExistCode = async () => {
  let complaintCode = utils.generateCode()
  const complaintInfo = await model.findOne({
    order: [['complaint_id', 'DESC']],
    attributes: [COMPLAINTS.FIELDS.COMPLAINT_CODE]
  })
  console.log('complaintInfo', complaintInfo)
  if (complaintInfo) {
    const cod = complaintInfo.dataValues as any
    complaintCode = utils.generateCode(cod.complaint_code)
  }
  return complaintCode
}

const checkComplaintExist = async (id: number) => {
  return model.count({ where: { [COMPLAINTS.FIELDS.COMPLAINT_ID]: id } })
}

const getHtmlTemplate = async () => {
  const emailTmpModel = getModel<EmailTemplateAttributes>(EMAIL_TEMPLATE.TABLE)
  const html = await emailTmpModel.findOne()
}

const getInfoByComplaintId = async (complaintId: number) => {
  const result = await model.findOne({ where: { complaint_id: complaintId } })
  const sql = GET_LIST_QUERY_STATUS + ` \n AND cpl.complaint_id = ${complaintId} `
  const data = await executeQuery({ modelString: COMPLAINTS.TABLE, query: sql })
  return data
}

enum ComplaintStatus {
  STATUS_CLOSE = 'STATUS_CLOSE'
}
function getComplaintFilterNotClosed(userId: number, countryId: number) {
  return {
    [COMPLAINTS.FIELDS.ASSIGNEE]: userId,
    [COMPLAINTS.FIELDS.COUNTRY_ID]: countryId,
    [COMPLAINTS.FIELDS.DELETE_FLG]: { [Op.not]: true },
    [COMPLAINTS.FIELDS.STATUS]: { [Op.not]: ComplaintStatus.STATUS_CLOSE }
  }
}

const countMyComplaint = async (userId: number, countryId: number) => {
  return {
    complaintCount: await model.count({
      where: getComplaintFilterNotClosed(userId, countryId),
      include: [
        {
          model: getModel(USERS.TABLE),
          attributes: [],
          required: true,
          where: { [USERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
        },
        {
          model: getModel(PRODUCTS.TABLE),
          attributes: [],
          required: true,
          where: { [PRODUCTS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
        },
        {
          model: getModel(SUPPLIERS.TABLE),
          attributes: [],
          required: false,
          where: { [SUPPLIERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
        }
      ]
    })
  }
}

const getSuggestionList = async (keyword: string, countryId: number, isDefault?: boolean) => {
  const filter = isDefault
    ? { [COMPLAINTS.FIELDS.COMPLAINT_ID]: { [Op.in]: keyword.split(',') } }
    : {
        [COMPLAINTS.FIELDS.COMPLAINT_CODE]: { [Op.iLike]: `%${keyword}%` },
        [COMPLAINTS.FIELDS.COUNTRY_ID]: countryId,
        [COMPLAINTS.FIELDS.STATUS]: { [Op.not]: ComplaintStatus.STATUS_CLOSE }
      }
  return model.findAll({
    where: Object.assign(filter, { [COMPLAINTS.FIELDS.DELETE_FLG]: { [Op.not]: true } }),
    attributes: [
      [COMPLAINTS.FIELDS.COMPLAINT_ID, SUGGESTION.ID],
      [COMPLAINTS.FIELDS.COMPLAINT_CODE, SUGGESTION.VALUE]
    ],
    limit: utils.convertStrToInt(DEFAULT_LIMIT)
  })
}

const getBasicComplaint = async (id: string) => {
  return model.findOne({
    where: {
      [COMPLAINTS.FIELDS.COMPLAINT_ID]: utils.convertStrToInt(id),
      [COMPLAINTS.FIELDS.DELETE_FLG]: { [Op.not]: true }
    },
    include: {
      model: getModel(SUPPLIERS.TABLE),
      required: false,
      attributes: [[SUPPLIERS.FIELDS.SUPPLIER_NAME, SUPPLIERS.FIELDS.SUPPLIER_NAME_ALIAS]]
    },
    attributes: [
      [COMPLAINTS.FIELDS.COMPLAINT_ID, COMPLAINTS.FIELDS.COMPLAINT_ID_ALIAS],
      [COMPLAINTS.FIELDS.COMPLAINT_CODE, COMPLAINTS.FIELDS.COMPLAINT_CODE_ALIAS],
      [COMPLAINTS.FIELDS.EVENT_DATE, COMPLAINTS.FIELDS.EVENT_DATE_ALIAS],
      [COMPLAINTS.FIELDS.BSC_AWARE_DATE, COMPLAINTS.FIELDS.BSC_AWARE_DATE_ALIAS],
      [COMPLAINTS.FIELDS.JPOS_CIPI, COMPLAINTS.FIELDS.JPOS_CIPI_ALIAS]
    ]
  })
}

const getMailInfo = async (id: number, countryId: string | number, type: string) => {
  const [detail, users, template] = await Promise.all([
    getDetail(id),
    UserProfileServices.getUserEmailCountry((await ComplaintWatcherService.getWatcherIds(id)).map((w) => w.userId)),
    EmailTemplateServices.getEmailManualTemplate({ type, countryId })
  ])
  const to: string[] = []
  const cc: string[] = []
  users.forEach((user) => {
    to.push(user.getDataValue(SUPPLIERS.FIELDS.SUPPLIER_EMAIL_TO_ALIAS) || user.email)
    if (user.getDataValue(SUPPLIERS.FIELDS.SUPPLIER_EMAIL_CC_ALIAS))
      cc.push(user.getDataValue(SUPPLIERS.FIELDS.SUPPLIER_EMAIL_CC_ALIAS))
  })

  // TODO replace template content
  return { title: template.title, htmlContent: template.htmlContent, to: to.join(', '), cc: cc.join(', ') }
}

export default {
  getList,
  getExportList,
  getDetail,
  updateOrCreate,
  cloneComplaint,
  checkComplaintExist,
  getHtmlTemplate,
  getInfoByComplaintId,
  countMyComplaint,
  getSuggestionList,
  getBasicComplaint,
  getMailInfo
}
