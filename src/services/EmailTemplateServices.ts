import { Op } from 'sequelize'
import { EmailTemplateAttributes } from '../models/EmailTemplate'
import { EMAIL_TEMPLATE } from '../models/ModelConstants'
import { getModel } from '../models/sequelize'

type EmailFilter = { type: string; countryId: number | string; role: string; screen: 'CREATE' | 'EDIT' | 'DETAIL' }
const getEmail = async (filter: EmailFilter) => {
  const { type, countryId, role, screen = 'DETAIL' } = filter
  const code = `${role}_${type}_${screen}`.toUpperCase()
  const isAuto = true
  const model = getModel<EmailTemplateAttributes>(EMAIL_TEMPLATE.TABLE)
  // TODO replace content with info
  return model.findOne({
    where: {
      [Op.or]: [
        { type, countryId, code, isAuto },
        { type, countryId, code: 'DEFAULT', isAuto }
      ]
    },
    attributes: [
      EMAIL_TEMPLATE.FIELDS.TITLE,
      [EMAIL_TEMPLATE.FIELDS.HTML_CONTENT, EMAIL_TEMPLATE.FIELDS.HTML_CONTENT_ALIAS]
    ]
  })
}

type EmailManualFilter = { type: string; countryId: string | number }
const getEmailManual = async (filter: EmailManualFilter) => {
  const { type, countryId } = filter
  const model = getModel<EmailTemplateAttributes>(EMAIL_TEMPLATE.TABLE)
  return model.findOne({
    where: { type, countryId, code: 'DEFAULT', isAuto: { [Op.not]: true } },
    attributes: [
      EMAIL_TEMPLATE.FIELDS.TITLE,
      [EMAIL_TEMPLATE.FIELDS.HTML_CONTENT, EMAIL_TEMPLATE.FIELDS.HTML_CONTENT_ALIAS]
    ]
  })
}

export default {
  getEmailTemplate: async (filter: EmailFilter) => getEmail(filter),
  getEmailManualTemplate: async (filter: EmailManualFilter) => getEmailManual(filter)
}
