import { Op } from 'sequelize'
import { ComplaintWatcherAttributes } from '../models/ComplaintWatcher'
import { COMPLAINT_WATCHERS } from '../models/ModelConstants'
import { getModel } from '../models/sequelize'

const getWatcherIds = async (prId: string | number) => {
  const model = getModel<ComplaintWatcherAttributes>(COMPLAINT_WATCHERS.TABLE)
  return model.findAll({
    raw: true,
    where: {
      [COMPLAINT_WATCHERS.FIELDS.COMPLAINT_ID]: prId,
      [COMPLAINT_WATCHERS.FIELDS.DELETE_FLG]: { [Op.not]: true }
    },
    attributes: [[COMPLAINT_WATCHERS.FIELDS.USER_ID, COMPLAINT_WATCHERS.FIELDS.USER_ID_ALIAS]]
  })
}

export default { getWatcherIds }
