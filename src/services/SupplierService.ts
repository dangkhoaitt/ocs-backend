import { Op } from 'sequelize'
import { DEFAULT_LIMIT } from '../models/base'
import { SUGGESTION, SUPPLIERS } from '../models/ModelConstants'
import { getModel } from '../models/sequelize'
import { SupplierAttributes } from '../models/Supplier'
import utils from './utils'

const model = getModel<SupplierAttributes>(SUPPLIERS.TABLE)

const getSupplierSuggestionList = async (keyword: string, isDefault?: boolean) => {
  const filter = isDefault
    ? { [SUPPLIERS.FIELDS.SUPPLIER_ID]: { [Op.in]: keyword.split(',') } }
    : { [SUPPLIERS.FIELDS.SUPPLIER_NAME]: { [Op.iLike]: `%${keyword}%` } }
  return model.findAll({
    where: Object.assign(filter, { [SUPPLIERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }),
    attributes: [
      [SUPPLIERS.FIELDS.SUPPLIER_ID, SUGGESTION.ID],
      [SUPPLIERS.FIELDS.SUPPLIER_NAME, SUGGESTION.VALUE]
    ],
    limit: utils.convertStrToInt(DEFAULT_LIMIT)
  })
}

export default {
  getSupplierSuggestionList
}
