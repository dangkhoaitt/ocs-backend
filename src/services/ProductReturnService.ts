import { col, FindAttributeOptions, FindOptions, fn, Op, WhereOptions } from 'sequelize'
import { Literal } from 'sequelize/types/lib/utils'
import constant from '../constant/constant'
import {
  DATE_FIELDS_REGEX,
  DEFAULT_LIMIT,
  DEFAULT_MAX_ITEM,
  DEFAULT_PAGE,
  DEFAULT_SORT,
  exportFields,
  findOneOptions
} from '../models/base'
import { ComplaintAttributes } from '../models/Complaint'
import { COMPLAINTS, PRODUCTS, PRODUCT_RETURNS, SUPPLIERS, USERS } from '../models/ModelConstants'
import { ProductReturnAttributes } from '../models/ProductReturn'
import { getModel } from '../models/sequelize'
import CommonService from './CommonService'
import EmailTemplateServices from './EmailTemplateServices'
import ExportService, { ExportModel } from './ExportService'
import PRWatcherService from './PRWatcherService'
import UserProfileServices from './UserProfileServices'
import utils from './utils'

const cplAlias = 'complaint'

const COMPLAINT_FIELDS = [COMPLAINTS.FIELDS.ASSIGNEE, COMPLAINTS.FIELDS.COUNTRY_ID]
const DATE_FIELDS = [
  PRODUCT_RETURNS.FIELDS.PACKING_DATE,
  PRODUCT_RETURNS.FIELDS.SHIPPED_DATE_TO_OEM,
  PRODUCT_RETURNS.FIELDS.PRODUCT_RECEIVED_DATE_OEM,
  PRODUCT_RETURNS.FIELDS.PRODUCT_RECEIVED_DATE_BSC
]

const PR_EXPORT_FIELDS = exportFields(PRODUCT_RETURNS.FIELDS)
const PRODUCT_EXPORT_FIELDS = [PRODUCTS.FIELDS.PRODUCT_ID, PRODUCTS.FIELDS.PRODUCT_NAME]
const COMPLAINT_EXPORT_FIELDS = [
  COMPLAINTS.FIELDS.COMPLAINT_CODE,
  COMPLAINTS.FIELDS.DESCRIPTION,
  COMPLAINTS.FIELDS.EVENT_DATE,
  COMPLAINTS.FIELDS.BSC_AWARE_DATE,
  COMPLAINTS.FIELDS.JPOS_CIPI
]
const OTHER_EXPORT_FIELDS = [SUPPLIERS.FIELDS.SUPPLIER_ID, SUPPLIERS.FIELDS.SUPPLIER_NAME]

const PR_LIST_ATTRIBUTES: FindAttributeOptions = [
  [fn('CONCAT', col(`${cplAlias}.${COMPLAINTS.FIELDS.COMPLAINT_ID}`)), COMPLAINTS.FIELDS.COMPLAINT_ID_ALIAS],
  [fn('CONCAT', col(COMPLAINTS.FIELDS.COMPLAINT_CODE)), COMPLAINTS.FIELDS.COMPLAINT_CODE_ALIAS],
  [fn('CONCAT', col(COMPLAINTS.FIELDS.JPOS_CIPI)), COMPLAINTS.FIELDS.JPOS_CIPI_ALIAS],
  [fn('CONCAT', col(PRODUCTS.FIELDS.PRODUCT_NAME)), PRODUCTS.FIELDS.PRODUCT_NAME_ALIAS],
  [fn('CONCAT', col(SUPPLIERS.FIELDS.SUPPLIER_NAME)), SUPPLIERS.FIELDS.SUPPLIER_NAME_ALIAS],
  [PRODUCT_RETURNS.FIELDS.PR_STATUS, PRODUCT_RETURNS.FIELDS.PR_STATUS_ALIAS],
  [PRODUCT_RETURNS.FIELDS.PACKING_DATE, PRODUCT_RETURNS.FIELDS.PACKING_DATE_ALIAS],
  [PRODUCT_RETURNS.FIELDS.SHIPPED_DATE_TO_OEM, PRODUCT_RETURNS.FIELDS.SHIPPED_DATE_TO_OEM_ALIAS],
  [PRODUCT_RETURNS.FIELDS.PRODUCT_RECEIVED_DATE_BSC, PRODUCT_RETURNS.FIELDS.PRODUCT_RECEIVED_DATE_BSC_ALIAS],
  PRODUCT_RETURNS.FIELDS.ID
]

const model = getModel<ProductReturnAttributes>(PRODUCT_RETURNS.TABLE)
const { literal } = model.sequelize.Sequelize

export interface GetProductReturnInput extends Record<string, string> {
  limit?: string
  page?: string
  sortField?: string
  sortOrder?: string
  keyword?: string
}

function generatePRExport(data: ProductReturnAttributes[]) {
  for (const row of data) {
    Object.keys(row).forEach((key) => {
      if (DATE_FIELDS_REGEX.test(key)) row[key] = utils.convertTimestampToDateStr(Number(row[key]), 'YYYY-MM-DD HH:mm')
    })
  }
  return data
}

function getSearchOptions(keyword: string): WhereOptions {
  if (keyword)
    return {
      [Op.or]: [
        literal(`${cplAlias}."${COMPLAINTS.FIELDS.COMPLAINT_CODE}" ILIKE '%${keyword}%'`),
        literal(`${cplAlias}."${COMPLAINTS.FIELDS.DESCRIPTION}" ILIKE '%${keyword}%'`),
        literal(`"${PRODUCTS.FIELDS.PRODUCT_NAME}" ILIKE '%${keyword}%'`),
        literal(`"${SUPPLIERS.FIELDS.SUPPLIER_NAME}" ILIKE '%${keyword}%'`)
      ]
    }
  return undefined
}

function getFindOptions(filter: GetProductReturnInput): FindOptions {
  const { keyword = '', ...other } = filter
  const filters = []
  const search = getSearchOptions(keyword)
  for (const key in other) {
    if (COMPLAINTS.FIELDS.SUPPLIER_ID === key)
      filters.push(literal(`"complaint->supplier"."${SUPPLIERS.FIELDS.SUPPLIER_ID}" IN (${other[key].split(',')})`))
    else if (DATE_FIELDS.includes(key)) {
      const values = other[key].split(',')
      filters.push({
        [Op.and]: [
          { [key]: { [Op.gte]: utils.convertDateStrToTimestamp(values[0]) } },
          { [key]: { [Op.lte]: utils.convertDateStrToTimestamp(values[1], true) } }
        ]
      })
    } else if (COMPLAINT_FIELDS.includes(key)) {
      filters.push(literal(`"${cplAlias}"."${key}" = ${other[key]}`))
    } else {
      filters.push({ [key]: other[key] })
    }
  }
  return {
    where: {
      [Op.and]: [{ [PRODUCT_RETURNS.FIELDS.DELETE_FLG]: { [Op.not]: true } }, ...filters, search]
    },
    include: {
      model: getModel<ComplaintAttributes>(COMPLAINTS.TABLE),
      attributes: [],
      as: cplAlias,
      required: true,
      include: [
        { model: getModel(PRODUCTS.TABLE), attributes: [], required: true },
        { model: getModel(SUPPLIERS.TABLE), attributes: [], required: false }
      ]
    }
  }
}

function getOrderBy(sortField: string, sortOrder: string): Literal {
  return literal(
    COMPLAINTS.FIELDS.JPOS_CIPI === sortField || COMPLAINTS.FIELDS.COMPLAINT_CODE === sortField
      ? `"${cplAlias}"."${sortField}" ${sortOrder}`
      : `"${PRODUCT_RETURNS.TABLE}"."${sortField}" ${sortOrder}`
  )
}

const getProductReturns = async (query: GetProductReturnInput) => {
  const {
    limit = DEFAULT_LIMIT,
    page = DEFAULT_PAGE,
    sortField = PRODUCT_RETURNS.FIELDS.CREATED_AT,
    sortOrder = DEFAULT_SORT,
    ...filter
  } = query
  const limitInt = utils.convertStrToInt(limit)
  const pageInt = utils.convertStrToInt(page)
  const [total, data] = await Promise.all([
    model.count({ ...getFindOptions(filter) }),
    model.findAll({
      ...getFindOptions(filter),
      attributes: PR_LIST_ATTRIBUTES,
      raw: true,
      limit: limitInt,
      offset: (pageInt - 1) * limitInt,
      order: getOrderBy(sortField, sortOrder)
    })
  ])
  return { data, total, page: pageInt, limit: limitInt }
}

function selectExportFieldsQuery(exportFields: string[]): FindAttributeOptions {
  const attributes: FindAttributeOptions = []
  for (const field of exportFields) {
    if (PR_EXPORT_FIELDS.includes(field)) {
      attributes.push(field)
    } else if (COMPLAINT_EXPORT_FIELDS.includes(field)) {
      attributes.push(`${cplAlias}.${field}`)
    } else if (PRODUCT_EXPORT_FIELDS.includes(field) || OTHER_EXPORT_FIELDS.includes(field)) {
      attributes.push([fn('CONCAT', col(field)), field])
    }
  }
  return attributes
}

const exportProductReturns = async (exportFields: string[], query: GetProductReturnInput) => {
  const {
    limit = DEFAULT_MAX_ITEM,
    sortField = PRODUCT_RETURNS.FIELDS.CREATED_AT,
    sortOrder = DEFAULT_SORT,
    language = 'en',
    ...filter
  } = query

  const selectExportFields = selectExportFieldsQuery(exportFields)
  if (!selectExportFields) return null

  const limitInt = utils.convertStrToInt(limit)
  const result = await model.findAll({
    ...getFindOptions(filter),
    raw: true,
    attributes: selectExportFieldsQuery(exportFields),
    limit: limitInt,
    order: getOrderBy(sortField, sortOrder)
  })
  if (result.length === 0) return null

  return ExportService.generateExport({
    model: ExportModel.PRODUCT_RETURNS,
    fields: Object.keys(result[0]),
    data: generatePRExport(result),
    language
  })
}

const createWatchers = async (createdBy: string | number, id: number, watchers: number[]) => {
  await Promise.all(watchers.map((userId) => PRWatcherService.addWatcher({ objId: id, userId })))

  // send email to watchers
  CommonService.doSendMail({ createdBy, screen: 'CREATE', type: constant.TYPE_ENUM.PRO_RETURN, userIds: watchers })
}

const createProductReturn = async (createdBy: string | number, input: ProductReturnAttributes) => {
  const { watchers, ...other } = input
  const model = getModel<ProductReturnAttributes>(PRODUCT_RETURNS.TABLE)
  const prJusCreated = await model.create(other)
  if (prJusCreated) {
    const type = constant.TYPE_ENUM.PRO_RETURN
    CommonService.addHistory({ action: 'CREATE', recordId: prJusCreated.id, type, userId: createdBy })
    if (watchers) {
      createWatchers(createdBy, prJusCreated.id, watchers)
    }
  }
  return prJusCreated
}

enum PRStatus {
  PR_CLOSED = 'PR_CLOSED'
}
const countMyPR = async (userId: number, countryId: number) => {
  const model = getModel<ProductReturnAttributes>(PRODUCT_RETURNS.TABLE)
  const complaintModel = getModel(COMPLAINTS.TABLE)
  return {
    prCount: await model.count({
      where: {
        [PRODUCT_RETURNS.FIELDS.PR_STATUS]: { [Op.not]: PRStatus.PR_CLOSED },
        [PRODUCT_RETURNS.FIELDS.DELETE_FLG]: { [Op.not]: true }
      },
      include: {
        model: complaintModel,
        where: {
          [COMPLAINTS.FIELDS.ASSIGNEE]: userId,
          [COMPLAINTS.FIELDS.COUNTRY_ID]: countryId,
          [COMPLAINTS.FIELDS.DELETE_FLG]: { [Op.not]: true }
        },
        include: [
          {
            model: getModel(USERS.TABLE),
            attributes: [],
            required: true,
            where: { [USERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          },
          {
            model: getModel(SUPPLIERS.TABLE),
            attributes: [],
            required: false,
            where: { [SUPPLIERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          },
          {
            model: getModel(PRODUCTS.TABLE),
            attributes: [],
            required: true,
            where: { [PRODUCTS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
          }
        ]
      }
    })
  }
}

const getDetail = async (id: string) => {
  const complaintModel = getModel(COMPLAINTS.TABLE)
  return await model.findOne({
    raw: true,
    where: findOneOptions({ id }),
    include: {
      model: complaintModel,
      where: { [COMPLAINTS.FIELDS.DELETE_FLG]: { [Op.not]: true } },
      include: [{ model: getModel(SUPPLIERS.TABLE), attributes: [], required: false }],
      attributes: []
    },
    attributes: [
      PRODUCT_RETURNS.FIELDS.COMMENT,
      PRODUCT_RETURNS.FIELDS.CONTACT,
      [PRODUCT_RETURNS.FIELDS.PR_STATUS, PRODUCT_RETURNS.FIELDS.PR_STATUS_ALIAS],
      [PRODUCT_RETURNS.FIELDS.TRACKING_NUMBER, PRODUCT_RETURNS.FIELDS.TRACKING_NUMBER_ALIAS],
      [PRODUCT_RETURNS.FIELDS.OEM_SPECIAL_REQ_DATE, PRODUCT_RETURNS.FIELDS.OEM_SPECIAL_REQ_DATE_ALIAS],
      [PRODUCT_RETURNS.FIELDS.COURIER_NAME, PRODUCT_RETURNS.FIELDS.COURIER_NAME_ALIAS],
      [PRODUCT_RETURNS.FIELDS.SHIPPED_DATE_TO_OEM, PRODUCT_RETURNS.FIELDS.SHIPPED_DATE_TO_OEM_ALIAS],
      [PRODUCT_RETURNS.FIELDS.INFECTION_NAME, PRODUCT_RETURNS.FIELDS.INFECTION_NAME_ALIAS],
      [
        PRODUCT_RETURNS.FIELDS.INFECTIOUS_INFORMATION_REQUIRED,
        PRODUCT_RETURNS.FIELDS.INFECTIOUS_INFORMATION_REQUIRED_ALIAS
      ],
      [PRODUCT_RETURNS.FIELDS.REQUIRED_ACTION, PRODUCT_RETURNS.FIELDS.REQUIRED_ACTION_ALIAS],
      [PRODUCT_RETURNS.FIELDS.PRODUCT_RECEIVED_DATE_OEM, PRODUCT_RETURNS.FIELDS.PRODUCT_RECEIVED_DATE_OEM_ALIAS],
      [PRODUCT_RETURNS.FIELDS.PRODUCT_RECEIVED_DATE_BSC, PRODUCT_RETURNS.FIELDS.PRODUCT_RECEIVED_DATE_BSC_ALIAS],
      [PRODUCT_RETURNS.FIELDS.INFECTIOUS_PRODUCT, PRODUCT_RETURNS.FIELDS.INFECTIOUS_PRODUCT_ALIAS],
      [PRODUCT_RETURNS.FIELDS.PACKING_DATE, PRODUCT_RETURNS.FIELDS.PACKING_DATE_ALIAS],
      [PRODUCT_RETURNS.FIELDS.REQUEST_COUNT, PRODUCT_RETURNS.FIELDS.REQUEST_COUNT_ALIAS],
      [PRODUCT_RETURNS.FIELDS.REQUEST_DATE, PRODUCT_RETURNS.FIELDS.REQUEST_DATE_ALIAS],
      [PRODUCT_RETURNS.FIELDS.REQUEST_COMMENT, PRODUCT_RETURNS.FIELDS.REQUEST_COMMENT_ALIAS],
      [fn('CONCAT', col(SUPPLIERS.FIELDS.SUPPLIER_NAME)), SUPPLIERS.FIELDS.SUPPLIER_NAME_ALIAS],
      [fn('CONCAT', col(COMPLAINTS.FIELDS.EVENT_DESCRIPTION_EN)), COMPLAINTS.FIELDS.EVENT_DESCRIPTION_EN_ALIAS],
      [fn('CONCAT', col(COMPLAINTS.FIELDS.EVENT_DATE)), COMPLAINTS.FIELDS.EVENT_DATE_ALIAS],
      [fn('CONCAT', col(COMPLAINTS.FIELDS.BSC_AWARE_DATE)), COMPLAINTS.FIELDS.BSC_AWARE_DATE_ALIAS],
      [fn('CONCAT', col(COMPLAINTS.FIELDS.JPOS_CIPI)), COMPLAINTS.FIELDS.JPOS_CIPI_ALIAS]
    ]
  })
}

const sendMailToWatchers = async (createdBy: string | number, id: string | number) => {
  const watchersIds = await PRWatcherService.getWatcherIds(id)
  const userIds = watchersIds.map((w) => w.userId)

  // send email to watchers
  const type = constant.TYPE_ENUM.PRO_RETURN
  CommonService.doSendMail({ createdBy, screen: 'EDIT', type, userIds, recordId: id, isHistory: true })
}

const editProductReturn = async (createdBy: string, id: string, body: ProductReturnAttributes) => {
  const whereOptions = { where: findOneOptions({ id }) }
  const { dataValues } = await model.findOne({ ...whereOptions, attributes: Object.keys(body) })

  if (dataValues) {
    await model.update(body, whereOptions)
    CommonService.addHistory({
      action: 'EDIT',
      body,
      detail: dataValues as Record<string, unknown>,
      recordId: utils.convertStrToInt(id),
      type: constant.TYPE_ENUM.PRO_RETURN,
      userId: createdBy
    })
    sendMailToWatchers(createdBy, id)
    return body
  }
  return null
}

const getMailInfo = async (id: string, countryId: string | number, type: string) => {
  const [detail, users, template] = await Promise.all([
    getDetail(id),
    UserProfileServices.getUserEmailCountry((await PRWatcherService.getWatcherIds(id)).map((w) => w.userId)),
    EmailTemplateServices.getEmailManualTemplate({ type, countryId })
  ])
  const to: string[] = []
  const cc: string[] = []
  users.forEach((user) => {
    to.push(user.getDataValue(SUPPLIERS.FIELDS.SUPPLIER_EMAIL_TO_ALIAS) || user.email)
    if (user.getDataValue(SUPPLIERS.FIELDS.SUPPLIER_EMAIL_CC_ALIAS))
      cc.push(user.getDataValue(SUPPLIERS.FIELDS.SUPPLIER_EMAIL_CC_ALIAS))
  })

  // TODO replace template content
  return { title: template.title, htmlContent: template.htmlContent, to: to.join(', '), cc: cc.join(', ') }
}

export default {
  getProductReturns,
  exportProductReturns,
  createProductReturn,
  getDetail,
  countMyPR,
  editProductReturn,
  getMailInfo
}
