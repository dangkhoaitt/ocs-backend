import { col, fn, Op } from 'sequelize'
import { findOneOptions } from '../models/base'
import { PRODUCT_RETURN_WATCHERS, USERS } from '../models/ModelConstants'
import { ProductReturnWatcherAttributes } from '../models/ProductReturnWatcher'
import { getModel } from '../models/sequelize'
import { UserAttributes } from '../models/User'

const prWatcherModel = getModel<ProductReturnWatcherAttributes>(PRODUCT_RETURN_WATCHERS.TABLE)

const addWatcher = async (input: { objId: number; userId: number }, isPRCreate?: boolean) => {
  if (isPRCreate) return prWatcherModel.create({ prId: input.objId, userId: input.userId })

  const userModel = getModel<UserAttributes>(USERS.TABLE)
  const dataWatcher = await prWatcherModel.create({ prId: input.objId, userId: input.userId })
  const userInfo = await userModel.findOne({ where: { id: input.userId } })
  dataWatcher.setDataValue(USERS.FIELDS.AVATAR, userInfo?.avatar)
  dataWatcher.setDataValue(USERS.FIELDS.FULL_NAME, userInfo?.fullName)
  dataWatcher.setDataValue(USERS.FIELDS.EMAIL, userInfo?.email)
  return dataWatcher
}

const getWatcher = async (prId: number) => {
  const userModel = getModel<UserAttributes>(USERS.TABLE)
  const alias = 'user'
  return prWatcherModel.findAll({
    raw: true,
    where: {
      [PRODUCT_RETURN_WATCHERS.FIELDS.PR_ID]: prId,
      [PRODUCT_RETURN_WATCHERS.FIELDS.DELETE_FLG]: { [Op.not]: true }
    },
    attributes: [
      PRODUCT_RETURN_WATCHERS.FIELDS.ID,
      `${alias}.${USERS.FIELDS.AVATAR}`,
      `${alias}.${USERS.FIELDS.EMAIL}`,
      [fn('CONCAT', col(USERS.FIELDS.FIRST_NAME), ' ', col(USERS.FIELDS.LAST_NAME)), USERS.FIELDS.FULL_NAME]
    ],
    include: { as: alias, model: userModel, where: { [USERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }, attributes: [] }
  })
}

const deleteWatcher = async (id: number) => {
  const watcher = await prWatcherModel.findOne({
    where: findOneOptions({ id }),
    attributes: [
      [PRODUCT_RETURN_WATCHERS.FIELDS.PR_ID, PRODUCT_RETURN_WATCHERS.FIELDS.PR_ID_ALIAS],
      [PRODUCT_RETURN_WATCHERS.FIELDS.USER_ID, PRODUCT_RETURN_WATCHERS.FIELDS.USER_ID_ALIAS]
    ]
  })
  await prWatcherModel.destroy({ where: { [PRODUCT_RETURN_WATCHERS.FIELDS.ID]: id } })
  return watcher
}

const getWatcherIds = async (prId: string | number) => {
  return prWatcherModel.findAll({
    raw: true,
    where: {
      [PRODUCT_RETURN_WATCHERS.FIELDS.PR_ID]: prId,
      [PRODUCT_RETURN_WATCHERS.FIELDS.DELETE_FLG]: { [Op.not]: true }
    },
    attributes: [[PRODUCT_RETURN_WATCHERS.FIELDS.USER_ID, PRODUCT_RETURN_WATCHERS.FIELDS.USER_ID_ALIAS]]
  })
}

export default { addWatcher, getWatcher, deleteWatcher, getWatcherIds }
