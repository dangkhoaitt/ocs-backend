import { GfeAttachmentAttributes } from '../models/GfeAttachment'
import { GFE_ATTACHMENTS } from '../models/ModelConstants'
import { getModel } from '../models/sequelize'

const gfeAttachModel = getModel<GfeAttachmentAttributes>(GFE_ATTACHMENTS.TABLE)
type AttachmentInput = { objId: number; fileName: string; fileType: string; filePath: string; type: string }

const getAttachment = async (id: number) => {
  return gfeAttachModel.findAll({
    where: { [GFE_ATTACHMENTS.FIELDS.GFE_ID]: id }
  })
}

const addAttachGfe = async (input: AttachmentInput) => {
  const dataAttach = await gfeAttachModel.create({
    gfeId: input.objId,
    fileName: input.fileName,
    fileType: input.fileType,
    filePath: input.filePath
  })
  return dataAttach
}

const deleteAttachmentFile = async (idFile: number) => {
  const fileAttachment = await gfeAttachModel.findOne({ where: { id: idFile } })
  if (fileAttachment) {
    const rowDeleted = await gfeAttachModel.destroy({ where: { id: idFile } })
  }
  return fileAttachment
}

export default { getAttachment, addAttachGfe, deleteAttachmentFile }
