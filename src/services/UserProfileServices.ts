import bcrypt from 'bcryptjs'
import config from 'config'
import fs from 'fs'
import path from 'path'
import saml2 from 'saml2-js'
import { Op } from 'sequelize'
import mode from '../config'
import { concatFields, DEFAULT_LIMIT } from '../models/base'
import { SUPPLIERS, USERS } from '../models/ModelConstants'
import { executeQuery, getModel } from '../models/sequelize'
import { GET_SUGGESTION_QUERY, UserAttributes } from '../models/User'
import utils from './utils'

const UserModel = getModel<UserAttributes>(USERS.TABLE)
const { literal } = UserModel.sequelize.Sequelize

/**
 * @api {post} /api/complaints Create complaint
 * @apiName CreateComplaint
 * @apiGroup Complaint
 *
 * @apiParam {String} reporter Exp: admin *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "version": "1.0",
 *        "statusCode": 200,
 *        "messageCode": "SUCCESS",
 *        "message": "Request successful."
 *      }
 */

/**
 * @api {get} /api/complaints Get list complaint
 * @apiName GetList
 * @apiGroup Complaint
 *
 * @apiParam {String} reporter Exp: admin
 * @apiParam {String} assigned Exp: alex
 * @apiParam {String} created Exp: 2020-12-07
 * @apiParam {String} type Exp: {TYPE_ENUM}
 * @apiParam {Number} status StatusID
 * @apiParam {Number} country country code
 * @apiParam {Number} limit number
 * @apiParam {Number} page number
 * @apiParam {String} sortField field to sort
 * @apiParam {String} sortOrder ASC or DESC
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "version": "1.0",
 *        "statusCode": 200,
 *        "messageCode": "SUCCESS",
 *        "message": "Request successful.",
 *        "data":[
 *                  {
 *                    "complaint_id":1,
 *                    "status_name":"NEW",
 *                    "status_id":1,
 *                    "reporter":"Hai Tran",
 *                    "assigned":"Alex",
 *                    "created_at":"2020-12-07 16:10",
 *                    "country":"China",
 *                    "product_name":"Machine",
 *                    "mdv_id": 1,
 *                    "product_return_id":1,
 *                    "crl_id":1,
 *                    "gpe_id":1
 *                  }
 *              ],
 *        "limit": 10,
 *        "page": 1,
 *        "total": 6
 *      }
 */

/**
 * @api {get} /api/product-return Get list product return
 * @apiName GetList
 * @apiGroup ProductReturn
 *
 * @apiParam {String} reporter Exp: admin
 * @apiParam {String} assigned Exp: alex
 * @apiParam {String} created Exp: 2020-12-07
 * @apiParam {String} type Exp: {TYPE_ENUM}
 * @apiParam {Number} status StatusID
 * @apiParam {Number} country country code
 * @apiParam {Number} limit number
 * @apiParam {Number} page number
 * @apiParam {String} sortField field to sort
 * @apiParam {String} sortOrder ASC or DESC
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "version": "1.0",
 *        "statusCode": 200,
 *        "messageCode": "SUCCESS",
 *        "message": "Request successful.",
 *        "data":[
 *                  {
 *                    "complaint_id":1,
 *                    "status_name":"NEW",
 *                    "status_id":1,
 *                    "reporter":"Hai Tran",
 *                    "assigned":"Alex",
 *                    "created_at":"2020-12-07 16:10",
 *                    "country":"China",
 *                    "product_name":"Machine",
 *                    "mdv_id": 1,
 *                    "product_return_id":1,
 *                    "crl_id":1,
 *                    "gpe_id":1
 *                  }
 *              ],
 *        "limit": 10,
 *        "page": 1,
 *        "total": 6
 *      }
 */

const createUser = async (data: any) => {
  const result = await UserModel.create({
    userName: data.username,
    firstName: data.firstName,
    lastName: data.lastName,
    password: bcrypt.hashSync(data.password, 8),
    phone: data.phone,
    title: data.title,
    mobile: data.mobile,
    role: data.role,
    email: data.email,
    supplierId: data.supplierId
  })
  return result
}

const getUserByUserName = async (username: string) => {
  const userInfo = await UserModel.findOne({ where: { username } })
  return userInfo
}

const getUserById = async (userId: number) => {
  const userInfo = await UserModel.findOne({ where: { id: userId } })
  return userInfo
}

//#region SAML2
const createSamlSP = async (redirect: string) => {
  const sp_options: any = {
    entity_id: config.get(`${mode}.saml.companyName`),
    assert_endpoint: config.get(`${mode}.saml.assert_endpoint`) + (redirect.length ? redirect : ''),
    force_authn: true,
    auth_context: { comparison: 'exact', class_refs: ['urn:oasis:names:tc:SAML:2.0:protocol'] },
    nameid_format: 'urn:oasis:names:tc:SAML:2.0:metadata',
    sign_get_request: false,
    allow_unencrypted_assertion: true
  }
  return new saml2.ServiceProvider(sp_options)
}
const createSamlIP = async () => {
  const idp_options: any = {
    sso_login_url: config.get(`${mode}.saml.sso_login_url`),
    certificates: [fs.readFileSync(path.join(__dirname, '../config', 'cert.crt')).toString()]
  }
  return new saml2.IdentityProvider(idp_options)
}
//#endregion

const getUserSuggestionList = async (keyword: string, isDefault?: boolean) => {
  const filter = isDefault
    ? `"usr"."${USERS.FIELDS.ID}" IN(?)`
    : `("usr"."${USERS.FIELDS.FIRST_NAME}" ILIKE ? OR "usr"."${USERS.FIELDS.LAST_NAME}" LIKE ?) LIMIT ?`
  const values = isDefault
    ? [keyword.split(',')]
    : [`%${keyword}%`, `%${keyword}%`, utils.convertStrToInt(DEFAULT_LIMIT)]
  return executeQuery({
    query: GET_SUGGESTION_QUERY.replace('$filter', filter),
    values,
    modelString: USERS.TABLE
  })
}

const updateFilter = async (id: number, filter: string) => {
  const [result] = await UserModel.update({ filter }, { where: { id } })
  return result
}

const getUserEmailCountry = async (userIds: string | number[]) => {
  return UserModel.findAll({
    where: {
      [USERS.FIELDS.ID]: { [Op.in]: userIds },
      [USERS.FIELDS.DELETE_FLG]: { [Op.not]: true }
    },
    attributes: [
      USERS.FIELDS.EMAIL,
      USERS.FIELDS.ROLE,
      USERS.FIELDS.SUPPLIER_ID,
      [USERS.FIELDS.COUNTRY_ID, USERS.FIELDS.COUNTRY_ID_ALIAS],
      [concatFields(`supplier.${SUPPLIERS.FIELDS.SUPPLIER_EMAIL_CC}`), SUPPLIERS.FIELDS.SUPPLIER_EMAIL_CC_ALIAS],
      [concatFields(`supplier.${SUPPLIERS.FIELDS.SUPPLIER_EMAIL_TO}`), SUPPLIERS.FIELDS.SUPPLIER_EMAIL_TO_ALIAS]
    ],
    include: {
      model: getModel(SUPPLIERS.TABLE),
      required: false,
      attributes: [],
      on: {
        [Op.and]: [
          literal(`"${USERS.TABLE}"."${USERS.FIELDS.SUPPLIER_ID}" = supplier."${SUPPLIERS.FIELDS.SUPPLIER_ID}"`),
          { [SUPPLIERS.FIELDS.DELETE_FLG]: { [Op.not]: true } }
        ]
      }
    }
  })
}

export default {
  createUser,
  getUserByUserName,
  createSamlSP,
  createSamlIP,
  getUserById,
  updateUser: async (data: UserAttributes, params: unknown, query: unknown) => {
    return new Promise((resolve, reject) => {
      resolve(true)
    })
  },
  getUserSuggestionList,
  updateFilter,
  getUserEmailCountry
}
