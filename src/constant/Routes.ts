export default {
  USER: '/api/user',
  GET_ERROR: '/v1/errors',
  COMPLAINT: '/api/complaints',
  COMMON: '/api/common',
  PRODUCT_RETURN: '/api/product-returns',
  COMPLANT_HISTORIES: '/api/complaint-histories',
  GFE_RECORD: '/api/gfe',
  CRL_RECORD: '/api/crl'
}
