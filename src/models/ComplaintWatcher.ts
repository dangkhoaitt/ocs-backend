import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { COMPLAINT_WATCHERS, USERS } from './ModelConstants'

export interface ComplaintWatcherAttributes extends BaseAttributes {
  id?: number
  complaintId?: number
  userId?: number
}

export function ComplaintWatcherFactory(sequelize: Sequelize) {
  return sequelize.define<ComplaintWatcherAttributes>(
    COMPLAINT_WATCHERS.TABLE,
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
        field: COMPLAINT_WATCHERS.FIELDS.ID
      },
      complaintId: {
        type: DataTypes.INTEGER.UNSIGNED,
        field: COMPLAINT_WATCHERS.FIELDS.COMPLAINT_ID
      },
      userId: {
        type: DataTypes.INTEGER.UNSIGNED,
        field: COMPLAINT_WATCHERS.FIELDS.USER_ID
      },
      createdAt: {
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: true,
        defaultValue: utils.getCurrentTime(),
        field: COMPLAINT_WATCHERS.FIELDS.CREATED_AT
      },
      updatedAt: {
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: true,
        defaultValue: utils.getCurrentTime(),
        field: COMPLAINT_WATCHERS.FIELDS.UPDATED_AT
      },
      deletedFlg: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        field: COMPLAINT_WATCHERS.FIELDS.DELETE_FLG
      }
    },
    {
      freezeTableName: true
    }
  )
}

export const GET_LIST_QUERY = `SELECT cplwt."${COMPLAINT_WATCHERS.FIELDS.ID}",
        cplwt."${COMPLAINT_WATCHERS.FIELDS.COMPLAINT_ID}" AS "complaintId",
        cplwt."${COMPLAINT_WATCHERS.FIELDS.USER_ID}" AS "userId",
        CONCAT(usr."${USERS.FIELDS.FIRST_NAME}", ' ', usr."${USERS.FIELDS.LAST_NAME}") AS "fullName",
        usr."${USERS.FIELDS.AVATAR}" AS "avatar",
        usr."${USERS.FIELDS.EMAIL}" AS "email"
        FROM "${COMPLAINT_WATCHERS.TABLE}" cplwt
        LEFT JOIN "${USERS.TABLE}" usr
        ON usr."${USERS.FIELDS.ID}" = cplwt."${COMPLAINT_WATCHERS.FIELDS.USER_ID}"
        WHERE 1=1 `
