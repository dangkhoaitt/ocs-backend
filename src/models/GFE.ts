import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { GFE_RECORDS } from './ModelConstants'

export interface GFEAttributes extends BaseAttributes {
  id?: number
  gfeId?: number
  complaintId?: number
  assignee?: number
  gfeStatus?: string
  gfeCount?: number
  requestDueDate?: number
  productReceivedDateBsc?: number
  requestDate?: number
  requestDateFromOem?: number
  requestInformation?: Text
  requestTemplate?: string
  respondent?: string
  response?: string
  sendDate?: number
  productAvailableForReturn?: string
  watchers?: number[]
  attachments?: number[]
}

export function gfeFactory(sequelize: Sequelize) {
  return sequelize.define<GFEAttributes>(GFE_RECORDS.TABLE, {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: GFE_RECORDS.FIELDS.ID
    },
    assignee: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: GFE_RECORDS.FIELDS.ASSIGNEE
    },
    complaintId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: GFE_RECORDS.FIELDS.COMPLAINT_ID
    },
    gfeStatus: {
      type: DataTypes.STRING(50),
      allowNull: false,
      field: GFE_RECORDS.FIELDS.STATUS
    },
    gfeCount: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: GFE_RECORDS.FIELDS.GFE_COUNT
    },
    requestDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: GFE_RECORDS.FIELDS.REQUEST_DATE
    },
    requestDueDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: GFE_RECORDS.FIELDS.REQUEST_DUE_DATE
    },
    productReceivedDateBsc: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: GFE_RECORDS.FIELDS.PRODUCT_RECEIVED_DATE_BSC
    },
    requestDateFromOem: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: GFE_RECORDS.FIELDS.REQUEST_DATE_FROM_OEM
    },
    sendDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: GFE_RECORDS.FIELDS.SEND_DATE
    },
    responseDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: GFE_RECORDS.FIELDS.RESPONSE_DATE
    },
    requestInformation: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: GFE_RECORDS.FIELDS.REQUEST_INFORMATION
    },
    requestTemplate: {
      type: DataTypes.STRING,
      allowNull: true,
      field: GFE_RECORDS.FIELDS.REQUEST_TEMPLATE
    },
    respondent: {
      type: DataTypes.STRING,
      allowNull: true,
      field: GFE_RECORDS.FIELDS.RESPONDENT
    },
    response: {
      type: DataTypes.STRING,
      allowNull: true,
      field: GFE_RECORDS.FIELDS.RESPONSE
    },
    productAvailableForReturn: {
      type: DataTypes.STRING,
      allowNull: true,
      field: GFE_RECORDS.FIELDS.PRODUCT_AVAILABLE_FOR_RETURN
    },
    createdAt: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: utils.getCurrentTime(),
      field: GFE_RECORDS.FIELDS.CREATED_AT
    },
    updatedAt: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: utils.getCurrentTime(),
      field: GFE_RECORDS.FIELDS.UPDATED_AT
    },
    deletedFlg: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: GFE_RECORDS.FIELDS.DELETE_FLG
    }
  })
}
