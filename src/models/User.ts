import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { SUGGESTION, USERS } from './ModelConstants'

export interface UserAttributes extends BaseAttributes {
  id?: number
  userName: string
  password: string
  lastName: string
  firstName: string
  avatar: string
  type: string
  email: string
  role: string
  deleteFlg: number
  fullName: string
  countryId: number
  filter?: string
}

export function userFactory(sequelize: Sequelize) {
  return sequelize.define<UserAttributes>(USERS.TABLE, {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: USERS.FIELDS.ID
    },
    userName: {
      type: DataTypes.STRING,
      unique: true,
      field: USERS.FIELDS.USERNAME
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      field: USERS.FIELDS.EMAIL
    },
    password: {
      type: DataTypes.STRING,
      field: USERS.FIELDS.PASSWORD
    },
    firstName: {
      type: DataTypes.STRING,
      field: USERS.FIELDS.FIRST_NAME
    },
    lastName: {
      type: DataTypes.STRING,
      field: USERS.FIELDS.LAST_NAME
    },
    phone: {
      type: DataTypes.STRING,
      field: USERS.FIELDS.PHONE
    },
    mobile: {
      type: DataTypes.STRING,
      field: USERS.FIELDS.MOBILE
    },
    title: {
      type: DataTypes.STRING,
      field: USERS.FIELDS.TITLE
    },
    role: {
      type: DataTypes.STRING(10),
      field: USERS.FIELDS.ROLE
    },
    avatar: {
      type: DataTypes.STRING,
      field: USERS.FIELDS.AVATAR
    },
    type: {
      type: DataTypes.STRING(2),
      field: USERS.FIELDS.TYPE
    },
    supplierId: {
      type: DataTypes.INTEGER,
      field: USERS.FIELDS.SUPPLIER_ID
    },
    bostonId: {
      type: DataTypes.INTEGER,
      field: USERS.FIELDS.BOSTON_ID
    },
    countryId: {
      type: DataTypes.INTEGER,
      field: USERS.FIELDS.COUNTRY_ID
    },
    createdAt: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: utils.getCurrentTime(),
      field: USERS.FIELDS.CREATED_AT
    },
    updatedAt: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: utils.getCurrentTime(),
      field: USERS.FIELDS.UPDATED_AT
    },
    deleteFlg: {
      type: DataTypes.BOOLEAN,
      field: USERS.FIELDS.DELETE_FLG
    },
    fullName: {
      type: DataTypes.VIRTUAL,
      get() {
        return `${this.firstName} ${this.lastName}`
      }
    },
    filter: {
      type: DataTypes.TEXT,
      field: USERS.FIELDS.FILTER
    }
  })
}

export const GET_SUGGESTION_QUERY = `SELECT "${USERS.FIELDS.ID}",
  CONCAT(usr."${USERS.FIELDS.FIRST_NAME}", ' ', usr."${USERS.FIELDS.LAST_NAME}") AS "${SUGGESTION.VALUE}",
  "${USERS.FIELDS.AVATAR}" AS "${SUGGESTION.ICON}"
FROM "${USERS.TABLE}" AS "usr"
WHERE COALESCE("usr"."${USERS.FIELDS.DELETE_FLG}", FALSE) = FALSE
AND $filter;`
