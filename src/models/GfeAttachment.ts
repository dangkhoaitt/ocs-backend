import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { GFE_ATTACHMENTS } from './ModelConstants'

export interface GfeAttachmentAttributes extends BaseAttributes {
  id?: number
  gfeId?: string
  fileName?: string
  fileType?: string
  filePath?: string
}

export function GfeAttachmentFactory(sequelize: Sequelize) {
  return sequelize.define<GfeAttachmentAttributes>(
    GFE_ATTACHMENTS.TABLE,
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
        field: GFE_ATTACHMENTS.FIELDS.ID
      },
      gfeId: {
        type: DataTypes.NUMBER,
        allowNull: false,
        field: GFE_ATTACHMENTS.FIELDS.GFE_ID
      },
      fileName: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: GFE_ATTACHMENTS.FIELDS.FILE_NAME
      },
      fileType: {
        type: DataTypes.STRING(20),
        allowNull: false,
        field: GFE_ATTACHMENTS.FIELDS.FILE_TYPE
      },
      filePath: {
        type: DataTypes.STRING,
        allowNull: false,
        field: GFE_ATTACHMENTS.FIELDS.FILE_PATH
      },
      createdAt: {
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: true,
        defaultValue: utils.getCurrentTime(),
        field: GFE_ATTACHMENTS.FIELDS.CREATED_AT
      },
      updatedAt: {
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: true,
        defaultValue: utils.getCurrentTime(),
        field: GFE_ATTACHMENTS.FIELDS.UPDATED_AT
      },
      deletedFlg: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        field: GFE_ATTACHMENTS.FIELDS.DELETE_FLG
      }
    },
    {
      freezeTableName: true
    }
  )
}
