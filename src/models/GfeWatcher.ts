import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { GFE_WATCHERS } from './ModelConstants'

export interface GfeWatcherAttributes extends BaseAttributes {
  id?: number
  gfeId?: number
  userId?: number
}

export function GfeWatcherFactory(sequelize: Sequelize) {
  return sequelize.define<GfeWatcherAttributes>(
    GFE_WATCHERS.TABLE,
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: GFE_WATCHERS.FIELDS.ID
      },
      gfeId: {
        type: DataTypes.INTEGER,
        field: GFE_WATCHERS.FIELDS.GFE_ID
      },
      userId: {
        type: DataTypes.INTEGER,
        field: GFE_WATCHERS.FIELDS.USER_ID
      },
      createdAt: {
        type: DataTypes.BIGINT,
        allowNull: true,
        defaultValue: utils.getCurrentTime(),
        field: GFE_WATCHERS.FIELDS.CREATED_AT
      },
      updatedAt: {
        type: DataTypes.BIGINT,
        allowNull: true,
        defaultValue: utils.getCurrentTime(),
        field: GFE_WATCHERS.FIELDS.UPDATED_AT
      },
      deletedFlg: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        field: GFE_WATCHERS.FIELDS.DELETE_FLG
      }
    },
    { freezeTableName: true }
  )
}
