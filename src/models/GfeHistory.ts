import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { HISTORIES } from './ModelConstants'

export interface GfeHistoryAttributes extends BaseAttributes {
  fieldName?: string
  gfeId?: number
}

export function GfeHistoryFactory(sequelize: Sequelize) {
  return sequelize.define<GfeHistoryAttributes>(HISTORIES.GFE, {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: HISTORIES.FIELDS.ID
    },
    gfeId: {
      type: DataTypes.INTEGER,
      field: HISTORIES.FIELDS.GFE_ID
    },
    userId: {
      type: DataTypes.INTEGER,
      field: HISTORIES.FIELDS.USER_ID
    },
    fieldName: {
      type: DataTypes.STRING(100),
      allowNull: true,
      field: HISTORIES.FIELDS.FIELD_NAME
    },
    oldValue: {
      type: DataTypes.STRING(500),
      allowNull: true,
      field: HISTORIES.FIELDS.OLD_VALUE
    },
    newValue: {
      type: DataTypes.STRING(500),
      allowNull: true,
      field: HISTORIES.FIELDS.NEW_VALUE
    },
    watcherId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: HISTORIES.FIELDS.WATCHER_ID
    },
    watcherType: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: HISTORIES.FIELDS.WATCHER_TYPE
    },
    attachment: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: HISTORIES.FIELDS.ATTACHMENT
    },
    action: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: HISTORIES.FIELDS.ACTION
    },
    createdAt: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: utils.getCurrentTime(),
      field: HISTORIES.FIELDS.CREATED_AT
    },
    updatedAt: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: utils.getCurrentTime(),
      field: HISTORIES.FIELDS.UPDATED_AT
    },
    deletedFlg: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: HISTORIES.FIELDS.DELETE_FLG
    }
  })
}
