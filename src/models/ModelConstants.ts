export const COMPLAINTS = {
  TABLE: 'complaints',
  FIELDS: {
    COMPLAINT_ID: 'complaint_id',
    COMPLAINT_ID_ALIAS: 'complaintId',
    COMPLAINT_CODE: 'complaint_code',
    COMPLAINT_CODE_ALIAS: 'complaintCode',
    SUPPLIER_ID: 'supplier_id',
    ASSIGNEE: 'assignee',
    PRODUCT_ID: 'product_id',
    COUNTRY_ID: 'country_id',
    JPOS_CIPI: 'jpos_cipi',
    JPOS_CIPI_ALIAS: 'jposCipi',
    SOURCE: 'source',
    STATUS: 'status',
    DESCRIPTION: 'description',
    AR_CODE: 'ar_code',
    EVENT_DATE: 'event_date',
    EVENT_DATE_ALIAS: 'eventDate',
    EVENT_NOTIFICATION_DATE: 'event_notification_date',
    BSC_AWARE_DATE: 'bsc_aware_date',
    BSC_AWARE_DATE_ALIAS: 'bscAwareDate',
    PRODUCT_RETURNABLE: 'product_returnable',
    FACILITY_NAME: 'facility_name',
    FACILITY_SAP_CODE: 'facility_sap_code',
    PHYSICIAN_NAME: 'physician_name',
    OEM_NOTIFICATION_DATE: 'oem_notification_date',
    COMPLANT_REVIEW_DATE: 'complaint_review_date',
    CREATED_AT: 'created_at',
    CREATED_AT_ALIAS: 'createdAt',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg',
    PATIENT_OUTCOME: 'patient_outcome',
    EVENT_DESCRIPTION: 'event_description',
    EVENT_DESCRIPTION_EN: 'event_description_EN',
    EVENT_DESCRIPTION_EN_ALIAS: 'eventDescriptionEN',
    REPORTER_NAME: 'reporter_name',
    REPORTER_PHONE: 'reporter_phone',
    REPORTER_ADDRESS: 'reporter_address',
    REPORTER_SALE_REP: 'reporter_sale_rep',
    REPORTER_TITLE: 'reporter_title',
    OEM_REFERENCE_NUMBER: 'oem_reference_number',
    BUSINESS_DIVISION: 'business_division',
    RELATED_COMPLAINT_ID: 'related_complaint_id',
    PHYSICIAN_COMMENT: 'physician_comment',
    PHYSICIAN_DEPARTMENT: 'physician_department',
    PROCEDURE_DATE: 'procedure_date',
    INTENDED_USE: 'intended_use',
    TYPE_OF_PROCEDURE: 'type_of_procedure',
    ANATOMY_LOCATION: 'anatomy_location',
    DIAGNOSIS_OR_TREATMENT: 'diagnosis_or_treatment',
    RESOLUTION: 'resolution',
    TIME_OF_EVENT: 'time_of_event',
    WHERE_DID_EVENT_OCCUR: 'where_did_event_occur',
    FIRST_TIME_THE_UNIT: 'first_time_the_unit',
    REPROCESSED: 'reprocessed',
    USED_BEFORE_EXPIRY_DATE: 'used_before_expiry_date',
    FOLLOW_INSTRUCTION: 'follow_instruction',
    LAST_NAME: 'last_name',
    FIRST_NAME: 'first_name',
    IDENTIFIER: 'identifier',
    GENDER: 'gender',
    DATE_OF_BIRTH: 'date_of_birth',
    AGE: 'age',
    PATIENT_IS_UNDER_18: 'patient_is_under_18',
    WEIGHT: 'weight',
    BUSINESS_OFFICE: 'business_office',
    MANAGER_NAME: 'manager_name',
    CRL_REQUESTED: 'crl_requested',
    CRL_SEND_TO: 'crl_send_to',
    SCOPE_USED: 'scope_used',
    GUIDEWIRE_USED: 'guidewire_used',
    GENERATOR: 'generator',
    OTHER_USED: 'other_used',
    QUANTITY_OF_EVENT: 'quantity_of_event',
    QUANTITY_OF_PRODUCT_RETURN: 'quantity_of_product_return',
    REASON_PRODUCT_RETURN: 'reason_product_return',
    LOCAL_PRODUCT_LICENSE_NUMBER: 'product_license_number',
    OEM_SPECIAL_REQUIREMENT: 'oem_special_requirement',
    CMP: 'product_cmp'
  }
}

export const COUNTRIES = {
  TABLE: 'countries',
  FIELDS: {
    COUNTRY_ID: 'country_id',
    COUNTRY_ID_ALIAS: 'countryId',
    AREA_ID: 'area_id',
    AREA_ID_ALIAS: 'areaId',
    COUNTRY_KEY: 'country_key',
    COUNTRY_KEY_ALIAS: 'countryKey',
    COUNTRY_CODE: 'country_code',
    COUNTRY_CODE_ALIAS: 'countryCode',
    COUNTRY_NAME: 'country_name',
    COUNTRY_NAME_ALIAS: 'countryName',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg'
  }
}

export const CRL_RECORDS = {
  TABLE: 'crl_records',
  FIELDS: {
    ID: 'id',
    CRL_ID_ALIAS: 'crl_id',
    COMPLAINT_ID: 'complaint_id',
    COMPLAINT_ID_ALIAS: 'complaintId',
    CRL_OWNER: 'crl_owner',
    CRL_RECIPIENT: 'crl_recipient',
    CRL_REVIEW_STATUS: 'crl_review_status',
    STATUS: 'crl_status',
    STATUS_ALIAS: 'crlStatus',
    LETTER_REQUEST_DATE: 'letter_request_date',
    LETTER_DUE_DATE: 'letter_due_date',
    LETTER_DUE_DATE_ALIAS: 'dueDate',
    LETTER_TYPE: 'letter_type',
    LETTER_COMMENT: 'letter_comment',
    LETTER_COMPLETE_DATE: 'letter_complete_date',
    COVER_LETTER_REQUIRED: 'cover_letter_required',
    RECEIVE_DATE: 'receive_date',
    RECEIVE_DATE_ALIAS: 'receivedDate',
    SEND_DATE: 'send_date',
    SEND_DATE_ALIAS: "sentDate",
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg',
    CRL_ASSISTANT:"crl_assistant"
  }
}

export const PRODUCTS = {
  TABLE: 'products',
  FIELDS: {
    PRODUCT_ID: 'product_id',
    PRODUCT_ID_ALIAS: 'productId',
    PRODUCT_NAME: 'product_name',
    PRODUCT_NAME_ALIAS: 'productName',
    MODEL_NUMBER: 'model_number',
    CATALOG_NUMBER: 'catalog_number',
    UPN_BATCH_LOT: 'upn_batch_lot',
    UPN: 'upn',
    BATCH: 'batch',
    LOT: 'lot',
    BRANCH_NAME: 'branch_name',
    PRODUCT_FAMILY: 'product_family',
    DIVISION: 'division',
    SERIAL_NUMBER: 'serial_number',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg'
  }
}

export const MDV_RECORDS = {
  TABLE: 'mdv_records',
  FIELDS: {
    ID: 'id',
    MVD_ID_ALIAS: 'mdv_id',
    COMPLAINT_ID: 'complaint_id',
    PRODUCT_ID: 'product_id',
    STATUS: 'mdv_status',
    STATUS_ALIAS: 'mdvStatus',
    REPORTABLE: 'reportable',
    UNREPORTABLE_COMMENT: 'unreportable_comment',
    TIMELINE: 'timeline',
    SUBMISSION_DATE: 'submission_date',
    REPORTING_REFERENCE_NUMBER: 'reporting_reference_number',
    REQUEST_DATE: 'request_date',
    RESPONSE_DATE: 'response_date',
    LICENSE_NUMBER: 'license_number',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg'
  }
}

export const SUPPLIERS = {
  TABLE: 'suppliers',
  FIELDS: {
    SUPPLIER_ID: 'supplier_id',
    SUPPLIER_ID_ALIAS: 'supplierId',
    SUPPLIER_NAME: 'supplier_name',
    SUPPLIER_CODE: 'supplier_code',
    SUPPLIER_NAME_ALIAS: 'supplierName',
    SUPPLIER_EMAIL_TO: 'supplier_email_to',
    SUPPLIER_EMAIL_TO_ALIAS: 'supplierEmailTo',
    SUPPLIER_EMAIL_CC: 'supplier_email_cc',
    SUPPLIER_EMAIL_CC_ALIAS: 'supplierEmailCc',
    COMPANY_NAME: 'company_name',
    REMARKS: 'remarks',
    ADDRESS: 'address',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg'
  }
}

export const USERS = {
  TABLE: 'users',
  FIELDS: {
    ID: 'id',
    USERNAME: 'username',
    PASSWORD: 'password',
    LAST_NAME: 'last_name',
    LAST_NAME_ALIAS: 'lastName',
    FIRST_NAME: 'first_name',
    FIRST_NAME_ALIAS: 'firstName',
    AVATAR: 'avatar',
    TYPE: 'type',
    EMAIL: 'email',
    ROLE: 'role',
    PHONE: 'phone',
    MOBILE: 'mobile',
    TITLE: 'title',
    SUPPLIER_ID: 'supplier_id',
    BOSTON_ID: 'boston_id',
    COUNTRY_ID: 'country_id',
    COUNTRY_ID_ALIAS: 'countryId',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg',
    FULL_NAME: 'fullName',
    FILTER: 'filter'
  }
}

export const GFE_RECORDS = {
  TABLE: 'gfe_records',
  FIELDS: {
    ID: 'id',
    GFE_ID_ALIAS: 'gfeId',
    USER_ID: 'user_id',
    COMPLAINT_ID: 'complaint_id',
    COMPLAINT_ID_ALIAS: 'complaintId',
    ASSIGNEE: 'assignee',
    STATUS: 'gfe_status',
    STATUS_ALIAS: 'gfeStatus',
    GFE_COUNT: 'gfe_count',
    GFE_COUNT_ALIAS: 'gfeCount',
    REQUEST_DATE: 'request_date',
    REQUEST_DATE_ALIAS: 'requestDate',
    REQUEST_DUE_DATE: 'request_due_date',
    REQUEST_DUE_DATE_ALIAS: 'requestDueDate',
    REQUEST_INFORMATION: 'request_information',
    REQUEST_TEMPLATE: 'request_template',
    PRODUCT_AVAILABLE_FOR_RETURN: 'product_available_for_return',
    PRODUCT_RECEIVED_DATE_BSC: 'product_received_date_bsc',
    REQUEST_DATE_FROM_OEM: 'request_date_from_oem',
    RESPONSE: 'response',
    RESPONDENT: 'respondent',
    RESPONSE_DATE: 'response_date',
    SEND_DATE: 'send_date',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg'
  }
}

export const GFE_REQUEST = {
  TABLE: 'gfe_requests',
  FIELDS: {
    ID: 'id',
    GFE_ID: 'gfe_id',
    ASSIGNEE: 'assignee',
    USER_ID: 'user_id',
    STATUS: 'gfe_status',
    STATUS_ALIAS: 'gfeStatus',
    GFE_COUNT: 'gfe_count',
    GFE_COUNT_ALIAS: 'gfeCount',
    REQUEST_DATE: 'request_date',
    REQUEST_DATE_ALIAS: 'requestDate',
    REQUEST_DUE_DATE: 'request_due_date',
    REQUEST_DUE_DATE_ALIAS: 'requestDueDate',
    REQUEST_INFORMATION: 'request_information',
    REQUEST_TEMPLATE: 'request_template',
    PRODUCT_AVAILABLE_FOR_RETURN: 'product_available_for_return',
    PRODUCT_RECEIVED_DATE_BSC: 'product_received_date_bsc',
    REQUEST_DATE_FROM_OEM: 'request_date_from_oem',
    RESPONSE: 'response',
    RESPONDENT: 'respondent',
    RESPONSE_DATE: 'response_date',
    SEND_DATE: 'send_date',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg'
  }
}

export const PRODUCT_RETURNS = {
  TABLE: 'product_return_records',
  FIELDS: {
    ID: 'id',
    PR_ID_ALIAS: 'pr_id',
    COMPLAINT_ID: 'complaint_id',
    COMPLAINT_ID_ALIAS: 'complaintId',
    PR_STATUS: 'pr_status',
    PR_STATUS_ALIAS: 'prStatus',
    PRODUCT_RECEIVED_DATE_BSC: 'product_received_date_bsc',
    PRODUCT_RECEIVED_DATE_BSC_ALIAS: 'productReceivedDateBsc',
    PACKING_DATE: 'packing_date',
    PACKING_DATE_ALIAS: 'packingDate',
    COURIER_NAME: 'courier_name',
    COURIER_NAME_ALIAS: 'courierName',
    TRACKING_NUMBER: 'tracking_number',
    TRACKING_NUMBER_ALIAS: 'trackingNumber',
    OEM_SPECIAL_REQ_DATE: 'oem_special_req_date',
    OEM_SPECIAL_REQ_DATE_ALIAS: 'oemSpecialReqDate',
    SUPPLIER_ADDRESSES: 'supplier_addresses',
    SHIPPED_DATE_TO_OEM: 'shipped_date_to_oem',
    SHIPPED_DATE_TO_OEM_ALIAS: 'shippedDateToOem',
    PRODUCT_RECEIVED_DATE_OEM: 'product_received_date_oem',
    PRODUCT_RECEIVED_DATE_OEM_ALIAS: 'productReceivedDateOem',
    REQUEST_COUNT: 'request_count',
    REQUEST_COUNT_ALIAS: 'requestCount',
    REQUEST_COMMENT: 'request_comment',
    REQUEST_COMMENT_ALIAS: 'requestComment',
    REQUEST_DATE: 'request_date',
    REQUEST_DATE_ALIAS: 'requestDate',
    COMMENT: 'comment',
    CONTACT: 'contact',
    INFECTIOUS_PRODUCT: 'infectious_product',
    INFECTIOUS_PRODUCT_ALIAS: 'infectiousProduct',
    INFECTION_NAME: 'infection_name',
    INFECTION_NAME_ALIAS: 'infectionName',
    INFECTIOUS_INFORMATION_REQUIRED: 'infectious_information_required',
    INFECTIOUS_INFORMATION_REQUIRED_ALIAS: 'infectiousInformationRequired',
    REQUIRED_ACTION: 'required_action',
    REQUIRED_ACTION_ALIAS: 'requiredAction',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg',
    WATCHERS: 'watchers'
  }
}

export const STATUS = {
  TABLE: 'status',
  FIELDS: {
    ID: 'id',
    CODE: 'code',
    DESCRIPTION: 'description',
    TYPE: 'type',
    POSITION: 'position',
    COLOR_HEX: 'color_hex',
    COLOR_HEX_ALIAS: 'colorHex',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg'
  }
}

export const COMPLAINT_ATTACHMENTS = {
  TABLE: 'complaint_attachments',
  FIELDS: {
    ID: 'id',
    COMPLAINT_ID: 'complaint_id',
    FILE_NAME: 'file_name',
    FILE_TYPE: 'file_type',
    FILE_PATH: 'file_path',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg'
  }
}

export const COMPLAINT_WATCHERS = {
  TABLE: 'complaint_watchers',
  FIELDS: {
    ID: 'id',
    COMPLAINT_ID: 'complaint_id',
    USER_ID: 'user_id',
    USER_ID_ALIAS: 'userId',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg'
  }
}

export const HISTORIES = {
  COMPLAINT: 'complaint_histories',
  COMPLAINT_USER_FK: 'cpl_user',
  COMPLAINT_WATCHER_FK: 'cpl_watcher',
  PRODUCT_RETURN: 'product_return_histories',
  PR_USER_FK: 'pr_user',
  PR_WATCHER_FK: 'pr_watcher',
  GFE: 'gfe_histories',
  GFE_USER_FK: 'gfe_user',
  GFE_WATCHER_FK: 'gfe_watcher',
  FIELDS: {
    ID: 'id',
    COMPLAINT_ID: 'complaint_id',
    COMPLAINT_ID_ALIAS: 'complaintId',
    PR_ID: 'pr_id',
    PR_ID_ALIAS: 'prId',
    GFE_ID: 'gfe_id',
    GFE_ID_ALIAS: 'gfeId',
    USER_ID: 'user_id',
    USER_ID_ALIAS: 'userId',
    FIELD_NAME: 'field_name',
    FIELD_NAME_ALIAS: 'fieldName',
    FIELD_CODE: 'field_code',
    OLD_VALUE: 'old_value',
    OLD_VALUE_ALIAS: 'oldValue',
    NEW_VALUE: 'new_value',
    NEW_VALUE_ALIAS: 'newValue',
    OLD_VALUE_DISPLAY: 'old_value_display',
    NEW_VALUE_DISPLAY: 'new_value_display',
    WATCHER_ID: 'watcher_id',
    WATCHER_ID_ALIAS: 'watcherId',
    WATCHER_TYPE: 'watcher_type',
    WATCHER_TYPE_ALIAS: 'watcherType',
    WATCHER_NAME_ALIAS: 'watcherName',
    WATCHER_AVATAR_ALIAS: 'watcherAvatar',
    ATTACHMENT: 'attachment',
    MAIL: 'mail',
    ACTION: 'action',
    CREATED_AT: 'created_at',
    CREATED_AT_ALIAS: 'createdAt',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg'
  }
}

export const SETTING = {
  TABLE: 'setting',
  FIELDS: {
    ID: 'id',
    CODE: 'code',
    VALUE: 'value',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg'
  }
}

export const EMAIL_TEMPLATE = {
  TABLE: 'email_template',
  FIELDS: {
    ID: 'id',
    COUNTRY_ID: 'country_id',
    TITLE: 'title',
    HTML_CONTENT: 'html_content',
    HTML_CONTENT_ALIAS: 'htmlContent',
    TYPE: 'type',
    CODE: 'code',
    IS_AUTO: 'is_auto',
    DELETE_FLG: 'deleted_flg'
  }
}

export const PRODUCT_RETURN_WATCHERS = {
  TABLE: 'product_return_watchers',
  FIELDS: {
    ID: 'id',
    PR_ID: 'pr_id',
    PR_ID_ALIAS: 'prId',
    USER_ID: 'user_id',
    USER_ID_ALIAS: 'userId',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg'
  }
}

export const GFE_WATCHERS = {
  TABLE: 'gfe_watchers',
  FIELDS: {
    ID: 'id',
    GFE_ID: 'gfe_id',
    GFE_ID_ALIAS: 'gfeId',
    USER_ID: 'user_id',
    USER_ID_ALIAS: 'userId',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg'
  }
}

export const GFE_ATTACHMENTS = {
  TABLE: 'gfe_attachments',
  FIELDS: {
    ID: 'id',
    GFE_ID: 'gfe_id',
    GFE_ID_ALIAS: 'gfeId',
    FILE_NAME: 'file_name',
    FILE_TYPE: 'file_type',
    FILE_PATH: 'file_path',
    CREATED_AT: 'created_at',
    UPDATED_AT: 'updated_at',
    DELETE_FLG: 'deleted_flg'
  }
}

export const SUGGESTION = { ID: 'id', VALUE: 'value', ICON: 'icon' }
