import { get } from 'config'
import { Model, ModelCtor, QueryTypes, Sequelize } from 'sequelize'
import mode from '../config'
import { complaintFactory } from './Complaint'
import { ComplaintAttachmentFactory } from './ComplaintAttachment'
import { ComplaintHistoryFactory } from './ComplaintHistory'
import { ComplaintWatcherFactory } from './ComplaintWatcher'
import { countryFactory } from './Country'
import { crlFactory } from './CRL'
import { emailTemplateFactory } from './EmailTemplate'
import { gfeFactory } from './GFE'
import { GfeAttachmentFactory } from './GfeAttachment'
import { GfeHistoryFactory } from './GfeHistory'
import { gfeRequestFactory } from './GfeRequest'
import { GfeWatcherFactory } from './GfeWatcher'
import { mdvFactory } from './MDV'
import {
  COMPLAINTS,
  COMPLAINT_ATTACHMENTS,
  COMPLAINT_WATCHERS,
  CRL_RECORDS,
  EMAIL_TEMPLATE,
  GFE_ATTACHMENTS,
  GFE_RECORDS,
  GFE_REQUEST,
  GFE_WATCHERS,
  HISTORIES,
  PRODUCTS,
  PRODUCT_RETURN_WATCHERS,
  SUPPLIERS,
  USERS
} from './ModelConstants'
import { productFactory } from './Product'
import { productReturnFactory } from './ProductReturn'
import { productReturnHistoryFactory } from './ProductReturnHistory'
import { productReturnWatcherFactory } from './ProductReturnWatcher'
import { settingFactory } from './Setting'
import { statusFactory } from './Status'
import { supplierFactory } from './Supplier'
import { userFactory } from './User'

const dbConfig = new Sequelize(
  get(`${mode}.database.database`),
  get(`${mode}.database.user`),
  get(`${mode}.database.password`),
  {
    host: get(`${mode}.database.host`),
    dialectOptions: {
      multipleStatements: true,
      useUTC: true // -->Add this line. for reading from database
    },
    timezone: '+00:00', // -->Add this line. for writing to database

    dialect: 'postgres'
  }
)

// register user table
const user = userFactory(dbConfig)
const complaint = complaintFactory(dbConfig)
const productReturn = productReturnFactory(dbConfig)
const gfe = gfeFactory(dbConfig)
const gfeRequest = gfeRequestFactory(dbConfig)
const mdv = mdvFactory(dbConfig)
const crl = crlFactory(dbConfig)
const country = countryFactory(dbConfig)
const emailTemplate = emailTemplateFactory(dbConfig)
const product = productFactory(dbConfig)
const supplier = supplierFactory(dbConfig)
const complaintAttachment = ComplaintAttachmentFactory(dbConfig)
const complaintWatcher = ComplaintWatcherFactory(dbConfig)
const complaintHistory = ComplaintHistoryFactory(dbConfig)
const prWatcher = productReturnWatcherFactory(dbConfig)
const prHistory = productReturnHistoryFactory(dbConfig)
const gfeHistory = GfeHistoryFactory(dbConfig)
const gfeWatcher = GfeWatcherFactory(dbConfig)
const gfeAttachment = GfeAttachmentFactory(dbConfig)
statusFactory(dbConfig)
settingFactory(dbConfig)

complaint.hasOne(productReturn, {
  foreignKey: { field: COMPLAINTS.FIELDS.COMPLAINT_ID, allowNull: true }
})
complaint.hasOne(gfe, { foreignKey: { field: COMPLAINTS.FIELDS.COMPLAINT_ID, allowNull: true } })
complaint.hasOne(mdv, { foreignKey: { field: COMPLAINTS.FIELDS.COMPLAINT_ID, allowNull: true } })
complaint.hasOne(crl, { foreignKey: { field: COMPLAINTS.FIELDS.COMPLAINT_ID, allowNull: true } })
complaint.hasOne(complaintHistory, { foreignKey: { field: COMPLAINTS.FIELDS.COMPLAINT_ID, allowNull: true } })
complaint.hasMany(complaintAttachment, {
  foreignKey: COMPLAINT_ATTACHMENTS.FIELDS.COMPLAINT_ID
})
complaint.hasMany(complaintWatcher, { foreignKey: COMPLAINT_WATCHERS.FIELDS.COMPLAINT_ID })

productReturn.belongsTo(complaint)
productReturn.hasMany(prWatcher, { foreignKey: { field: PRODUCT_RETURN_WATCHERS.FIELDS.PR_ID, allowNull: true } })
productReturn.hasMany(prHistory, { foreignKey: { field: HISTORIES.FIELDS.PR_ID, allowNull: true } })
prWatcher.belongsTo(productReturn)
prHistory.belongsTo(productReturn)

gfeRequest.hasMany(gfeHistory, { foreignKey: { field: HISTORIES.FIELDS.GFE_ID, allowNull: true } })
gfeHistory.belongsTo(gfeRequest)

gfeRequest.hasMany(gfeAttachment, { foreignKey: { field: GFE_REQUEST.FIELDS.GFE_ID, allowNull: true } })
gfeAttachment.belongsTo(gfeRequest)

gfe.belongsTo(complaint)
mdv.belongsTo(complaint)
crl.belongsTo(complaint)

user.hasMany(gfeRequest, { foreignKey: { field: GFE_REQUEST.FIELDS.ASSIGNEE } })
gfeRequest.belongsTo(user)

gfe.hasMany(gfeRequest, { foreignKey: { field: GFE_REQUEST.FIELDS.GFE_ID } })
gfeRequest.belongsTo(gfe)

gfeRequest.hasMany(gfeWatcher, { foreignKey: { field: GFE_ATTACHMENTS.FIELDS.GFE_ID, allowNull: true } })
gfeWatcher.belongsTo(gfeRequest)

user.hasMany(gfeWatcher, { foreignKey: { field: GFE_WATCHERS.FIELDS.GFE_ID, allowNull: true } })
gfeWatcher.belongsTo(user)

complaintHistory.belongsTo(complaint)

user.hasMany(gfe, { foreignKey: { field: GFE_RECORDS.FIELDS.ASSIGNEE } })
gfe.belongsTo(user)

// PRO
user.hasMany(crl, { foreignKey: { field: CRL_RECORDS.FIELDS.CRL_OWNER, allowNull: false } })
user.hasMany(crl, { foreignKey: { field: CRL_RECORDS.FIELDS.CRL_RECIPIENT, allowNull: false } })
user.hasMany(crl, { foreignKey: { field: CRL_RECORDS.FIELDS.CRL_ASSISTANT, allowNull: false } })
crl.belongsTo(user, { as: 'owner',foreignKey: CRL_RECORDS.FIELDS.CRL_OWNER })
crl.belongsTo(user, { as: 'recipient',foreignKey: CRL_RECORDS.FIELDS.CRL_RECIPIENT })
crl.belongsTo(user, { as: 'assistant',foreignKey: CRL_RECORDS.FIELDS.CRL_ASSISTANT })

product.hasMany(complaint, { foreignKey: { field: PRODUCTS.FIELDS.PRODUCT_ID, allowNull: true } })
complaint.belongsTo(product)

country.hasMany(complaint, { foreignKey: { field: COMPLAINTS.FIELDS.COUNTRY_ID, allowNull: true } })
complaint.belongsTo(country)

country.hasMany(emailTemplate, { foreignKey: { field: EMAIL_TEMPLATE.FIELDS.COUNTRY_ID, allowNull: true } })
emailTemplate.belongsTo(country)

supplier.hasMany(complaint, { foreignKey: { field: SUPPLIERS.FIELDS.SUPPLIER_ID, allowNull: true } })
complaint.belongsTo(supplier)

user.hasMany(complaint, { foreignKey: { field: COMPLAINTS.FIELDS.ASSIGNEE, allowNull: true } })
complaint.belongsTo(user)

user.hasMany(complaintWatcher, { foreignKey: COMPLAINT_WATCHERS.FIELDS.USER_ID })
user.hasMany(prWatcher, { foreignKey: { field: PRODUCT_RETURN_WATCHERS.FIELDS.USER_ID, allowNull: true } })
prWatcher.belongsTo(user)

user.hasMany(prHistory, { foreignKey: { field: HISTORIES.FIELDS.USER_ID, allowNull: true } })
prHistory.belongsTo(user)

user.hasMany(gfeHistory, { foreignKey: { field: HISTORIES.FIELDS.USER_ID, allowNull: true } })
gfeHistory.belongsTo(user)

user.hasMany(complaintHistory, {
  as: HISTORIES.COMPLAINT_USER_FK,
  foreignKey: { field: HISTORIES.FIELDS.USER_ID, allowNull: true }
})
user.hasMany(complaintHistory, {
  as: HISTORIES.COMPLAINT_WATCHER_FK,
  foreignKey: { field: HISTORIES.FIELDS.WATCHER_ID, allowNull: true }
})
complaintHistory.belongsTo(user, { as: HISTORIES.COMPLAINT_USER_FK })
complaintHistory.belongsTo(user, { as: HISTORIES.COMPLAINT_WATCHER_FK })

user.hasMany(prHistory, {
  as: HISTORIES.PR_USER_FK,
  foreignKey: { field: HISTORIES.FIELDS.USER_ID, allowNull: true }
})
user.hasMany(prHistory, {
  as: HISTORIES.PR_WATCHER_FK,
  foreignKey: { field: HISTORIES.FIELDS.WATCHER_ID, allowNull: true }
})
prHistory.belongsTo(user, { as: HISTORIES.PR_USER_FK })
prHistory.belongsTo(user, { as: HISTORIES.PR_WATCHER_FK })

user.hasOne(supplier, { foreignKey: { field: USERS.FIELDS.SUPPLIER_ID, allowNull: true } })
supplier.belongsTo(user)

export function getModel<T extends Model>(modelString: string) {
  return dbConfig.models[modelString] as ModelCtor<T>
}

type RawQueryOptions = { modelString: string; query: string; values?: unknown[] }
export function executeQuery(options: RawQueryOptions) {
  return dbConfig.query(options.query, { replacements: options.values, type: QueryTypes.SELECT })
}

export async function alterContrainst() {
  const { exec } = await import('child_process')
  await new Promise((resolve, reject) => {
    const migrate = exec('sequelize db:migrate', { env: process.env }, (err) => (err ? reject(err) : resolve(true)))
    // Forward stdout+stderr to this process
    migrate.stdout.pipe(process.stdout)
    migrate.stderr.pipe(process.stderr)
  })
}

export default dbConfig
