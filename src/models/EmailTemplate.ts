import { DataTypes, Sequelize } from 'sequelize'
import { BaseAttributes } from './base'
import { EMAIL_TEMPLATE } from './ModelConstants'

export interface EmailTemplateAttributes extends BaseAttributes {
  id?: number
  countryId?: number
  title?: string
  htmlContent?: string
  type?: string
  code?: string
}

export function emailTemplateFactory(sequelize: Sequelize) {
  return sequelize.define<EmailTemplateAttributes>(
    EMAIL_TEMPLATE.TABLE,
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
        field: EMAIL_TEMPLATE.FIELDS.ID
      },
      code: {
        type: DataTypes.STRING(50),
        allowNull: true,
        field: EMAIL_TEMPLATE.FIELDS.CODE
      },
      countryId: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        field: EMAIL_TEMPLATE.FIELDS.COUNTRY_ID
      },
      title: {
        type: DataTypes.STRING(100),
        allowNull: true,
        field: EMAIL_TEMPLATE.FIELDS.TITLE
      },
      htmlContent: {
        type: DataTypes.TEXT,
        allowNull: false,
        field: EMAIL_TEMPLATE.FIELDS.HTML_CONTENT
      },
      type: {
        type: DataTypes.STRING(50),
        allowNull: true,
        field: EMAIL_TEMPLATE.FIELDS.TYPE
      },
      isAuto: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        field: EMAIL_TEMPLATE.FIELDS.IS_AUTO
      }
    },
    { freezeTableName: true, timestamps: false }
  )
}
