import { DataTypes, Sequelize } from 'sequelize'
import { BaseAttributes } from './base'
import { COUNTRIES } from './ModelConstants'

export interface CountryAttributes extends BaseAttributes {
  countryId?: number
  areaId?: number
  countryKey?: string
  countryCode?: string
  countryName?: string
}

export function countryFactory(sequelize: Sequelize) {
  return sequelize.define<CountryAttributes>(COUNTRIES.TABLE, {
    countryId: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
      field: COUNTRIES.FIELDS.COUNTRY_ID
    },
    areaId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      field: COUNTRIES.FIELDS.AREA_ID
    },
    countryKey: {
      type: DataTypes.STRING(10),
      allowNull: false,
      field: COUNTRIES.FIELDS.COUNTRY_KEY
    },
    countryCode: {
      type: DataTypes.STRING(10),
      allowNull: false,
      field: COUNTRIES.FIELDS.COUNTRY_CODE
    },
    countryName: {
      type: DataTypes.STRING(50),
      allowNull: false,
      field: COUNTRIES.FIELDS.COUNTRY_NAME
    },
    createdAt: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      field: COUNTRIES.FIELDS.CREATED_AT
    },
    updatedAt: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      field: COUNTRIES.FIELDS.UPDATED_AT
    },
    deletedFlg: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: COUNTRIES.FIELDS.DELETE_FLG
    }
  })
}
