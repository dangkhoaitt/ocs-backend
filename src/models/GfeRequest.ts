import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { GFE_REQUEST } from './ModelConstants'

export interface GFERequestAttributes extends BaseAttributes, Record<string, unknown> {
  id?: number
  gfeId?: number
  assignee?: number
  gfeStatus?: string
  gfeCount?: number
  requestDueDate?: number
  productReceivedDateBsc?: number
  requestDate?: number
  requestDateFromOem?: number
  requestInformation?: string
  requestTemplate?: string
  respondent?: string
  response?: string
  sendDate?: number
  productAvailableForReturn?: string
}

export function gfeRequestFactory(sequelize: Sequelize) {
  return sequelize.define<GFERequestAttributes>(GFE_REQUEST.TABLE, {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: GFE_REQUEST.FIELDS.ID
    },
    assignee: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: GFE_REQUEST.FIELDS.ASSIGNEE
    },
    gfeId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: GFE_REQUEST.FIELDS.GFE_ID
    },
    gfeStatus: {
      type: DataTypes.STRING(50),
      allowNull: false,
      field: GFE_REQUEST.FIELDS.STATUS
    },
    gfeCount: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: GFE_REQUEST.FIELDS.GFE_COUNT
    },
    requestDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: GFE_REQUEST.FIELDS.REQUEST_DATE
    },
    requestDueDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: GFE_REQUEST.FIELDS.REQUEST_DUE_DATE
    },
    productReceivedDateBsc: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: GFE_REQUEST.FIELDS.PRODUCT_RECEIVED_DATE_BSC
    },
    requestDateFromOem: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: GFE_REQUEST.FIELDS.REQUEST_DATE_FROM_OEM
    },
    sendDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: GFE_REQUEST.FIELDS.SEND_DATE
    },
    responseDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: GFE_REQUEST.FIELDS.RESPONSE_DATE
    },
    requestInformation: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: GFE_REQUEST.FIELDS.REQUEST_INFORMATION
    },
    requestTemplate: {
      type: DataTypes.STRING,
      allowNull: true,
      field: GFE_REQUEST.FIELDS.REQUEST_TEMPLATE
    },
    respondent: {
      type: DataTypes.STRING,
      allowNull: true,
      field: GFE_REQUEST.FIELDS.RESPONDENT
    },
    response: {
      type: DataTypes.STRING,
      allowNull: true,
      field: GFE_REQUEST.FIELDS.RESPONSE
    },
    productAvailableForReturn: {
      type: DataTypes.STRING,
      allowNull: true,
      field: GFE_REQUEST.FIELDS.PRODUCT_AVAILABLE_FOR_RETURN
    },
    createdAt: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: utils.getCurrentTime(),
      field: GFE_REQUEST.FIELDS.CREATED_AT
    },
    updatedAt: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: utils.getCurrentTime(),
      field: GFE_REQUEST.FIELDS.UPDATED_AT
    },
    deletedFlg: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: GFE_REQUEST.FIELDS.DELETE_FLG
    }
  })
}
