import { col, fn, Model, Op, WhereOptions } from 'sequelize'
import { Fn } from 'sequelize/types/lib/utils'

export class BaseAttributes extends Model {
  public createdAt?: number
  public updatedAt?: number
  public deletedFlg?: boolean
}

export const DEFAULT_LIMIT = '10'
export const DEFAULT_PAGE = '1'
export const DEFAULT_SORT = 'DESC'
export const DEFAULT_MAX_ITEM = '2000'
export const DATE_FIELDS_REGEX = /(?:_at|date)/

export function exportFields(modelFields: Record<string, string>) {
  const fields: string[] = []
  for (const key in modelFields) {
    if (!key.includes('ALIAS')) fields.push(modelFields[key])
  }
  return fields
}

export function findOneOptions(where: object): WhereOptions {
  return { ...where, deletedFlg: { [Op.not]: true } }
}

export function concatFields(col1: string, col2?: string): Fn {
  if (col2) return fn('CONCAT', col(col1), ' ', col(col2))
  return fn('CONCAT', col(col1))
}
