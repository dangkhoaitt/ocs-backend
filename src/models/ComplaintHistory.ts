import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { HISTORIES } from './ModelConstants'

export interface ComplainHistoryAttributes extends BaseAttributes {
  complaintId?: string
}

export function ComplaintHistoryFactory(sequelize: Sequelize) {
  return sequelize.define<ComplainHistoryAttributes>(HISTORIES.COMPLAINT, {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: HISTORIES.FIELDS.ID
    },
    complaintId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: HISTORIES.FIELDS.COMPLAINT_ID
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: HISTORIES.FIELDS.USER_ID
    },
    fieldName: {
      type: DataTypes.STRING(100),
      allowNull: true,
      field: HISTORIES.FIELDS.FIELD_NAME
    },
    fieldCode: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: HISTORIES.FIELDS.FIELD_CODE
    },
    oldValue: {
      type: DataTypes.STRING(500),
      allowNull: true,
      field: HISTORIES.FIELDS.OLD_VALUE
    },
    newValue: {
      type: DataTypes.STRING(500),
      allowNull: true,
      field: HISTORIES.FIELDS.NEW_VALUE
    },
    oldValueDisplay: {
      type: DataTypes.STRING(500),
      allowNull: true,
      field: HISTORIES.FIELDS.OLD_VALUE_DISPLAY
    },
    newValueDisplay: {
      type: DataTypes.STRING(500),
      allowNull: true,
      field: HISTORIES.FIELDS.NEW_VALUE_DISPLAY
    },
    watcherId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: HISTORIES.FIELDS.WATCHER_ID
    },
    watcherType: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: HISTORIES.FIELDS.WATCHER_TYPE
    },
    attachment: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: HISTORIES.FIELDS.ATTACHMENT
    },
    mail: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: HISTORIES.FIELDS.MAIL
    },
    action: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: HISTORIES.FIELDS.ACTION
    },
    createdAt: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: utils.getCurrentTime(),
      field: HISTORIES.FIELDS.CREATED_AT
    },
    updatedAt: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: utils.getCurrentTime(),
      field: HISTORIES.FIELDS.UPDATED_AT
    },
    deletedFlg: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: HISTORIES.FIELDS.DELETE_FLG
    }
  })
}
