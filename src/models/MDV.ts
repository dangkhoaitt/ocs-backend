import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { MDV_RECORDS } from './ModelConstants'

export interface MDVAttributes extends BaseAttributes {
  id?: number
  complaintId?: number
  mdvStatus?: number
  reportable?: boolean
  unreportableComment?: string
  timeline?: number
  submissionDate?: Date
  reportingReferenceNumber?: string
  requestDate?: Date
  responseDate?: Date
  licenseNumber?: string
}

export function mdvFactory(sequelize: Sequelize) {
  return sequelize.define<MDVAttributes>(MDV_RECORDS.TABLE, {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: MDV_RECORDS.FIELDS.ID
    },
    mdvStatus: {
      type: DataTypes.STRING(50),
      allowNull: false,
      field: MDV_RECORDS.FIELDS.STATUS
    },
    reportable: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: MDV_RECORDS.FIELDS.REPORTABLE
    },
    unreportableComment: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: MDV_RECORDS.FIELDS.UNREPORTABLE_COMMENT
    },
    timeline: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: MDV_RECORDS.FIELDS.TIMELINE
    },
    submissionDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: MDV_RECORDS.FIELDS.SUBMISSION_DATE
    },
    reportingReferenceNumber: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: MDV_RECORDS.FIELDS.REPORTING_REFERENCE_NUMBER
    },
    requestDate: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: MDV_RECORDS.FIELDS.REQUEST_DATE
    },
    responseDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: MDV_RECORDS.FIELDS.RESPONSE_DATE
    },
    licenseNumber: {
      type: DataTypes.STRING(50),
      allowNull: false,
      field: MDV_RECORDS.FIELDS.LICENSE_NUMBER
    },
    createdAt: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: utils.getCurrentTime(),
      field: MDV_RECORDS.FIELDS.CREATED_AT
    },
    updatedAt: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: utils.getCurrentTime(),
      field: MDV_RECORDS.FIELDS.UPDATED_AT
    },
    deletedFlg: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: MDV_RECORDS.FIELDS.DELETE_FLG
    }
  })
}
