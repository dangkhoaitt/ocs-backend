import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { CRL_RECORDS } from './ModelConstants'

export interface CRLAttributes extends BaseAttributes, Record<string, unknown> {
  id?: number
  complaintId?: number
  crlOwner?: number
  crlRecipient?: number
  crlReviewStatus?: number
  crlAssistant?: number
  letterStatus?: number
  letterRequestDate?: Date
  letterDueDate?: Date
  letterType?: number
  letterComment?: string
  letterCompleteDate?: Date
  coverLetterRequired?: boolean
  receiveDate?: Date
  sendDate?: Date
}

export function crlFactory(sequelize: Sequelize) {
  return sequelize.define<CRLAttributes>(CRL_RECORDS.TABLE, {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: CRL_RECORDS.FIELDS.ID
    },
    crlOwner: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: CRL_RECORDS.FIELDS.CRL_OWNER
    },
    crlRecipient: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: CRL_RECORDS.FIELDS.CRL_RECIPIENT
    },
    crlReviewStatus: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: CRL_RECORDS.FIELDS.CRL_REVIEW_STATUS
    },
    crlAssistant: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: CRL_RECORDS.FIELDS.CRL_ASSISTANT
    },
    letterStatus: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: CRL_RECORDS.FIELDS.STATUS
    },
    letterRequestDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: CRL_RECORDS.FIELDS.LETTER_REQUEST_DATE
    },
    letterDueDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: CRL_RECORDS.FIELDS.LETTER_DUE_DATE
    },
    letterType: {
      type: new DataTypes.STRING(500),
      allowNull: true,
      field: CRL_RECORDS.FIELDS.LETTER_TYPE
    },
    letterComment: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: CRL_RECORDS.FIELDS.LETTER_COMMENT
    },
    letterCompleteDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: CRL_RECORDS.FIELDS.LETTER_COMPLETE_DATE
    },
    coverLetterRequired: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: CRL_RECORDS.FIELDS.COVER_LETTER_REQUIRED
    },
    receiveDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: CRL_RECORDS.FIELDS.RECEIVE_DATE
    },
    sendDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: CRL_RECORDS.FIELDS.SEND_DATE
    },
    createdAt: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: utils.getCurrentTime(),
      field: CRL_RECORDS.FIELDS.CREATED_AT
    },
    updatedAt: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: utils.getCurrentTime(),
      field: CRL_RECORDS.FIELDS.UPDATED_AT
    },
    deletedFlg: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: CRL_RECORDS.FIELDS.DELETE_FLG
    }
  })
}
