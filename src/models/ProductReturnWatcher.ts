import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { PRODUCT_RETURN_WATCHERS } from './ModelConstants'

export interface ProductReturnWatcherAttributes extends BaseAttributes {
  id?: number
  prId?: number
  userId?: number
}

export function productReturnWatcherFactory(sequelize: Sequelize) {
  return sequelize.define<ProductReturnWatcherAttributes>(
    PRODUCT_RETURN_WATCHERS.TABLE,
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        field: PRODUCT_RETURN_WATCHERS.FIELDS.ID
      },
      prId: {
        type: DataTypes.INTEGER,
        field: PRODUCT_RETURN_WATCHERS.FIELDS.PR_ID
      },
      userId: {
        type: DataTypes.INTEGER,
        field: PRODUCT_RETURN_WATCHERS.FIELDS.USER_ID
      },
      createdAt: {
        type: DataTypes.BIGINT,
        allowNull: true,
        defaultValue: utils.getCurrentTime(),
        field: PRODUCT_RETURN_WATCHERS.FIELDS.CREATED_AT
      },
      updatedAt: {
        type: DataTypes.BIGINT,
        allowNull: true,
        defaultValue: utils.getCurrentTime(),
        field: PRODUCT_RETURN_WATCHERS.FIELDS.UPDATED_AT
      },
      deletedFlg: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        field: PRODUCT_RETURN_WATCHERS.FIELDS.DELETE_FLG
      }
    },
    { freezeTableName: true }
  )
}

// export const GET_LIST_QUERY = `SELECT cplwt."${PRODUCT_RETURN_WATCHERS.FIELDS.ID}",
//         cplwt."${PRODUCT_RETURN_WATCHERS.FIELDS.COMPLAINT_ID}" AS "complaintId",
//         cplwt."${PRODUCT_RETURN_WATCHERS.FIELDS.USER_ID}" AS "userId",
//         CONCAT(usr."${USERS.FIELDS.FIRST_NAME}", ' ', usr."${USERS.FIELDS.LAST_NAME}") AS "fullName",
//         usr."${USERS.FIELDS.AVATAR}" AS "avatar",
//         usr."${USERS.FIELDS.EMAIL}" AS "email"
//         FROM "${PRODUCT_RETURN_WATCHERS.TABLE}" cplwt
//         LEFT JOIN "${USERS.TABLE}" usr
//         ON usr."${USERS.FIELDS.ID}" = cplwt."${PRODUCT_RETURN_WATCHERS.FIELDS.USER_ID}"
//         WHERE 1=1 `
