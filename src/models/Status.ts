import { DataTypes, Sequelize } from 'sequelize'
import { BaseAttributes } from './base'
import { STATUS } from './ModelConstants'

export interface StatusAttributes extends BaseAttributes {
  id?: number
  code?: string
  description?: string
  type?: string
  colorHex?: string
}

export function statusFactory(sequelize: Sequelize) {
  return sequelize.define<StatusAttributes>(
    STATUS.TABLE,
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
        field: STATUS.FIELDS.ID
      },
      code: {
        type: DataTypes.STRING(50),
        allowNull: false,
        field: STATUS.FIELDS.CODE
      },
      description: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: STATUS.FIELDS.DESCRIPTION
      },
      type: {
        type: DataTypes.STRING(10),
        allowNull: false,
        field: STATUS.FIELDS.TYPE
      },
      colorHex: {
        type: DataTypes.STRING(10),
        allowNull: false,
        field: STATUS.FIELDS.COLOR_HEX
      },
      postion: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        defaultValue: 1,
        field: STATUS.FIELDS.POSITION
      },
      createdAt: {
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: true,
        field: STATUS.FIELDS.CREATED_AT
      },
      updatedAt: {
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: true,
        field: STATUS.FIELDS.UPDATED_AT
      },
      deletedFlg: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        field: STATUS.FIELDS.DELETE_FLG
      }
    },
    {
      freezeTableName: true, // Model tableName will be the same as the model name
      timestamps: false
    }
  )
}
