import { DataTypes, Sequelize } from 'sequelize'
import { BaseAttributes } from './base'
import { SUPPLIERS } from './ModelConstants'

export interface SupplierAttributes extends BaseAttributes {
  supplierId?: number
  supplierName?: string
  supplierEmailCc?: string
  supplierEmailTo?: string
  companyName?: string
}

export function supplierFactory(sequelize: Sequelize) {
  return sequelize.define<SupplierAttributes>(SUPPLIERS.TABLE, {
    supplierId: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
      field: SUPPLIERS.FIELDS.SUPPLIER_ID
    },
    supplierName: {
      type: DataTypes.STRING(50),
      allowNull: false,
      field: SUPPLIERS.FIELDS.SUPPLIER_NAME
    },
    supplierCode: {
      type: DataTypes.STRING(50),
      allowNull: false,
      field: SUPPLIERS.FIELDS.SUPPLIER_CODE
    },
    supplierEmailCc: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: SUPPLIERS.FIELDS.SUPPLIER_EMAIL_CC
    },
    supplierEmailTo: {
      type: DataTypes.STRING,
      allowNull: true,
      field: SUPPLIERS.FIELDS.SUPPLIER_EMAIL_TO
    },
    companyName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: SUPPLIERS.FIELDS.COMPANY_NAME
    },
    remarks: {
      type: DataTypes.STRING,
      allowNull: true,
      field: SUPPLIERS.FIELDS.REMARKS
    },
    createdAt: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      field: SUPPLIERS.FIELDS.CREATED_AT
    },
    updatedAt: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: true,
      field: SUPPLIERS.FIELDS.UPDATED_AT
    },
    deletedFlg: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: SUPPLIERS.FIELDS.DELETE_FLG
    }
  })
}
