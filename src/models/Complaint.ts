import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { COMPLAINTS, CRL_RECORDS, GFE_RECORDS, MDV_RECORDS, PRODUCT_RETURNS } from './ModelConstants'

export interface ComplaintAttributes extends BaseAttributes, Record<string, unknown> {
  complaintId?: number
  complaintCode: string
  supplierId?: number
  assignee?: number
  productId?: number
  countryId?: number
  jposCipi?: string
  source?: string
  statusCode?: string
  description?: string
  arCode?: string
  eventDate?: number
  eventNotificationDate?: number
  bscAwareDate?: number
  productReturnable?: boolean
  facilityName?: string
  physicianName?: string
  patientOutcome?: string
  eventDescription?: string
  oemNotificationDate?: number
  complaintReviewDate?: number
  oemReferenceNumber?: number
  relatedComplaintId?: number
  businessDivision?: string
  reporterAddress?: string
  reporterName?: string
  reporterPhone?: string
  reporterSaleRep?: string
  eventDescriptionEn?: string
  physicianComment?: string
  procedureDate?: number
  intendedUse?: string
  typeOfProcedure?: string
  anatomyLocation?: string
  diagnosisOrTreatment?: string
  resolution?: string
  timeOfEvent?: string
  whereDidEventOccur?: string
  firstTimeTheUnit?: boolean
  reprocessed?: boolean
  usedBeforeExpiryDate?: boolean
  followInstruction?: boolean
  lastName?: string
  firstName?: string
  identifier?: string
  gender?: string
  dateOfBirth?: BigInt
  age?: number
}

export function complaintFactory(sequelize: Sequelize) {
  return sequelize.define<ComplaintAttributes>(COMPLAINTS.TABLE, {
    complaintId: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
      field: COMPLAINTS.FIELDS.COMPLAINT_ID
    },
    complaintCode: {
      type: new DataTypes.STRING(12),
      unique: true,
      allowNull: false,
      field: COMPLAINTS.FIELDS.COMPLAINT_CODE
    },
    supplierId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true,
      field: COMPLAINTS.FIELDS.SUPPLIER_ID
    },
    assignee: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      field: COMPLAINTS.FIELDS.ASSIGNEE
    },
    productId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      field: COMPLAINTS.FIELDS.PRODUCT_ID
    },
    countryId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      field: COMPLAINTS.FIELDS.COUNTRY_ID
    },
    jposCipi: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: COMPLAINTS.FIELDS.JPOS_CIPI
    },
    source: {
      type: DataTypes.STRING(20),
      allowNull: true,
      field: COMPLAINTS.FIELDS.SOURCE
    },
    statusCode: {
      type: DataTypes.STRING(20),
      allowNull: false,
      field: COMPLAINTS.FIELDS.STATUS
    },
    description: {
      type: new DataTypes.STRING(500),
      allowNull: true,
      field: COMPLAINTS.FIELDS.DESCRIPTION
    },
    arCode: {
      type: new DataTypes.STRING(50),
      allowNull: true,
      field: COMPLAINTS.FIELDS.AR_CODE
    },
    eventDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: COMPLAINTS.FIELDS.EVENT_DATE
    },
    eventNotificationDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: COMPLAINTS.FIELDS.EVENT_NOTIFICATION_DATE
    },
    bscAwareDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: COMPLAINTS.FIELDS.BSC_AWARE_DATE
    },
    productReturnable: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: COMPLAINTS.FIELDS.PRODUCT_RETURNABLE
    },
    facilityName: {
      type: new DataTypes.STRING(256),
      allowNull: true,
      field: COMPLAINTS.FIELDS.FACILITY_NAME
    },
    facilitySapCode: {
      type: new DataTypes.STRING(256),
      allowNull: true,
      field: COMPLAINTS.FIELDS.FACILITY_SAP_CODE
    },
    physicianName: {
      type: new DataTypes.STRING(256),
      allowNull: true,
      field: COMPLAINTS.FIELDS.PHYSICIAN_NAME
    },
    physicianDepartment: {
      type: new DataTypes.STRING(256),
      allowNull: true,
      field: COMPLAINTS.FIELDS.PHYSICIAN_DEPARTMENT
    },
    createdAt: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: utils.getCurrentTime(),
      field: COMPLAINTS.FIELDS.CREATED_AT
    },
    updatedAt: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: utils.getCurrentTime(),
      field: COMPLAINTS.FIELDS.UPDATED_AT
    },
    deletedFlg: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: COMPLAINTS.FIELDS.DELETE_FLG
    },
    patientOutcome: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: COMPLAINTS.FIELDS.PATIENT_OUTCOME
    },
    eventDescription: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: COMPLAINTS.FIELDS.EVENT_DESCRIPTION
    },
    reporterAddress: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: COMPLAINTS.FIELDS.REPORTER_ADDRESS
    },
    reporterName: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: COMPLAINTS.FIELDS.REPORTER_NAME
    },
    reporterPhone: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: COMPLAINTS.FIELDS.REPORTER_PHONE
    },
    reporterSaleRep: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: COMPLAINTS.FIELDS.REPORTER_SALE_REP
    },
    reporterTitle: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: COMPLAINTS.FIELDS.REPORTER_TITLE
    },
    complaintReviewDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: COMPLAINTS.FIELDS.COMPLANT_REVIEW_DATE
    },
    oemNotificationDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: COMPLAINTS.FIELDS.OEM_NOTIFICATION_DATE
    },
    oemReferenceNumber: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: COMPLAINTS.FIELDS.OEM_REFERENCE_NUMBER
    },
    relatedComplaintId: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: COMPLAINTS.FIELDS.RELATED_COMPLAINT_ID
    },
    businessDivision: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.BUSINESS_DIVISION
    },
    eventDescriptionEn: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.EVENT_DESCRIPTION_EN
    },
    physicianComment: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.PHYSICIAN_COMMENT
    },
    procedureDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: COMPLAINTS.FIELDS.PROCEDURE_DATE
    },
    intendedUse: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.INTENDED_USE
    },
    typeOfProcedure: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.TYPE_OF_PROCEDURE
    },
    anatomyLocation: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.ANATOMY_LOCATION
    },
    diagnosisOrTreatment: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.DIAGNOSIS_OR_TREATMENT
    },
    resolution: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.RESOLUTION
    },
    timeOfEvent: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.TIME_OF_EVENT
    },
    whereDidEventOccur: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.WHERE_DID_EVENT_OCCUR
    },
    firstTimeTheUnit: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: COMPLAINTS.FIELDS.FIRST_TIME_THE_UNIT
    },
    reprocessed: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: COMPLAINTS.FIELDS.REPROCESSED
    },
    usedBeforeExpiryDate: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: COMPLAINTS.FIELDS.USED_BEFORE_EXPIRY_DATE
    },
    followInstruction: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: COMPLAINTS.FIELDS.FOLLOW_INSTRUCTION
    },
    lastName: {
      type: DataTypes.STRING(20),
      allowNull: true,
      field: COMPLAINTS.FIELDS.LAST_NAME
    },
    firstName: {
      type: DataTypes.STRING(20),
      allowNull: true,
      field: COMPLAINTS.FIELDS.FIRST_NAME
    },
    identifier: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.IDENTIFIER
    },
    gender: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: COMPLAINTS.FIELDS.GENDER
    },
    dateOfBirth: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: COMPLAINTS.FIELDS.DATE_OF_BIRTH
    },
    age: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: COMPLAINTS.FIELDS.AGE
    },
    patientIsUnder18: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.PATIENT_IS_UNDER_18
    },
    weight: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: COMPLAINTS.FIELDS.WEIGHT
    },
    businessOffice: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.BUSINESS_OFFICE
    },
    managerName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.MANAGER_NAME
    },
    crlRequested: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: COMPLAINTS.FIELDS.CRL_REQUESTED
    },
    crlSendTo: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.CRL_SEND_TO
    },
    scopeUsed: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: COMPLAINTS.FIELDS.SCOPE_USED
    },
    guidewireUsed: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: COMPLAINTS.FIELDS.GUIDEWIRE_USED
    },
    generator: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: COMPLAINTS.FIELDS.GENERATOR
    },
    otherUsed: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: COMPLAINTS.FIELDS.OTHER_USED
    },
    quantityEventNumber: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true,
      field: COMPLAINTS.FIELDS.QUANTITY_OF_EVENT
    },
    quantityProductReturnNumber: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true,
      field: COMPLAINTS.FIELDS.QUANTITY_OF_PRODUCT_RETURN
    },
    oemSpecialRequirement: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.OEM_SPECIAL_REQUIREMENT
    },
    localProductLicenseNumber: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.LOCAL_PRODUCT_LICENSE_NUMBER
    },
    reasonNotProductReturn: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: COMPLAINTS.FIELDS.REASON_PRODUCT_RETURN
    },
    productCMP: {
      type: DataTypes.STRING,
      allowNull: true,
      field: COMPLAINTS.FIELDS.CMP
    }
  })
}

export const GET_LIST_QUERY_STATUS = `SELECT cpl."${COMPLAINTS.FIELDS.COMPLAINT_ID}" AS "complaintId",
        cpl."${COMPLAINTS.FIELDS.STATUS}" AS "complaintStatus",
        prr."${PRODUCT_RETURNS.FIELDS.PR_STATUS}" AS "${PRODUCT_RETURNS.FIELDS.PR_STATUS_ALIAS}",
        prr."${PRODUCT_RETURNS.FIELDS.ID}" AS "${PRODUCT_RETURNS.FIELDS.PR_ID_ALIAS}",
        crl."${CRL_RECORDS.FIELDS.STATUS}" AS "${CRL_RECORDS.FIELDS.STATUS_ALIAS}",
        crl."${CRL_RECORDS.FIELDS.ID}" AS "${CRL_RECORDS.FIELDS.CRL_ID_ALIAS}",
        mdv."${MDV_RECORDS.FIELDS.STATUS}"       AS "${MDV_RECORDS.FIELDS.STATUS_ALIAS}",
        mdv."${MDV_RECORDS.FIELDS.ID}"       AS "${MDV_RECORDS.FIELDS.MVD_ID_ALIAS}",
        gfe."${GFE_RECORDS.FIELDS.STATUS}"       AS "${GFE_RECORDS.FIELDS.STATUS_ALIAS}",
        gfe."${GFE_RECORDS.FIELDS.ID}"       AS "${GFE_RECORDS.FIELDS.GFE_ID_ALIAS}"
        FROM "${COMPLAINTS.TABLE}" cpl
        LEFT JOIN "${PRODUCT_RETURNS.TABLE}" prr
              ON prr."${PRODUCT_RETURNS.FIELDS.COMPLAINT_ID}" = cpl."${COMPLAINTS.FIELDS.COMPLAINT_ID}"
        LEFT JOIN "${CRL_RECORDS.TABLE}" crl
              ON crl."${CRL_RECORDS.FIELDS.COMPLAINT_ID}" = cpl."${COMPLAINTS.FIELDS.COMPLAINT_ID}"
        LEFT JOIN "${MDV_RECORDS.TABLE}" mdv
              ON mdv."${MDV_RECORDS.FIELDS.COMPLAINT_ID}" = cpl."${COMPLAINTS.FIELDS.COMPLAINT_ID}"
        LEFT JOIN "${GFE_RECORDS.TABLE}" gfe
              ON gfe."${GFE_RECORDS.FIELDS.COMPLAINT_ID}" = cpl."${COMPLAINTS.FIELDS.COMPLAINT_ID}"
        WHERE 1=1 `
