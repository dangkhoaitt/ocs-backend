import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { PRODUCTS } from './ModelConstants'

export interface ProductAttributes extends BaseAttributes {
  productId?: number
  productName?: string
  modelNumber?: string
  catalogNumber?: string
  upn?: string
  batch?: string
  lot?: string
  branchName?: string
  productFamily?: string
  division?: number
  serialNumber?: string
}

export function productFactory(sequelize: Sequelize) {
  return sequelize.define<ProductAttributes>(PRODUCTS.TABLE, {
    productId: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: PRODUCTS.FIELDS.PRODUCT_ID
    },
    productName: {
      type: DataTypes.STRING(100),
      allowNull: false,
      field: PRODUCTS.FIELDS.PRODUCT_NAME
    },
    modelNumber: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: PRODUCTS.FIELDS.MODEL_NUMBER
    },
    catalogNumber: {
      type: DataTypes.STRING(20),
      allowNull: true,
      field: PRODUCTS.FIELDS.CATALOG_NUMBER
    },
    upn: {
      type: DataTypes.STRING(2020),
      allowNull: true,
      field: PRODUCTS.FIELDS.UPN
    },
    batch: {
      type: DataTypes.STRING(20),
      allowNull: true,
      field: PRODUCTS.FIELDS.BATCH
    },
    lot: {
      type: DataTypes.STRING(20),
      allowNull: true,
      field: PRODUCTS.FIELDS.LOT
    },
    branchName: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: PRODUCTS.FIELDS.BRANCH_NAME
    },
    productFamily: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: PRODUCTS.FIELDS.PRODUCT_FAMILY
    },
    division: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: PRODUCTS.FIELDS.DIVISION,
      comment: '1: All Division, 2: CRM, 3: NM, 4: IC, 5: EP, 6: LAAC, 7: ENDO, 8: URO&PH'
    },
    serialNumber: {
      type: DataTypes.STRING(256),
      allowNull: false,
      field: PRODUCTS.FIELDS.SERIAL_NUMBER
    },
    createdAt: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: utils.getCurrentTime(),
      field: PRODUCTS.FIELDS.CREATED_AT
    },
    updatedAt: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: utils.getCurrentTime(),
      field: PRODUCTS.FIELDS.UPDATED_AT
    },
    deletedFlg: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: PRODUCTS.FIELDS.DELETE_FLG
    }
  })
}
