import { DataTypes, Sequelize } from 'sequelize'
import { BaseAttributes } from './base'
import { SETTING } from './ModelConstants'

export interface SettingAttributes extends BaseAttributes {
    id?: number
    code?: string
    value?: string
}

export function settingFactory(sequelize: Sequelize) {
    return sequelize.define<SettingAttributes>(
        SETTING.TABLE,
        {
            id: {
                type: DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true,
                field: SETTING.FIELDS.ID
            },
            code: {
                type: DataTypes.STRING,
                allowNull: false,
                field: SETTING.FIELDS.CODE
            },
            value: {
                type: DataTypes.TEXT,
                allowNull: false,
                field: SETTING.FIELDS.VALUE
            },
            createdAt: {
                type: DataTypes.BIGINT.UNSIGNED,
                allowNull: true,
                field: SETTING.FIELDS.CREATED_AT
            },
            updatedAt: {
                type: DataTypes.BIGINT.UNSIGNED,
                allowNull: true,
                field: SETTING.FIELDS.UPDATED_AT
            },
            deletedFlg: {
                type: DataTypes.BOOLEAN,
                allowNull: true,
                field: SETTING.FIELDS.DELETE_FLG
            }
        },
        {
            freezeTableName: true, // Model tableName will be the same as the model name
            timestamps: false
        }
    )
}
