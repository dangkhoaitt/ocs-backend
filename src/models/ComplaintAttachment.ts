import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { COMPLAINT_ATTACHMENTS } from './ModelConstants'

export interface ComplaintAttachmentAttributes extends BaseAttributes {
  id?: number
  complaintId?: string
  fileName?: string
  fileType?: string
  filePath?: string
}

export function ComplaintAttachmentFactory(sequelize: Sequelize) {
  return sequelize.define<ComplaintAttachmentAttributes>(
    COMPLAINT_ATTACHMENTS.TABLE,
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
        field: COMPLAINT_ATTACHMENTS.FIELDS.ID
      },
      complaintId: {
        type: DataTypes.NUMBER,
        allowNull: false,
        field: COMPLAINT_ATTACHMENTS.FIELDS.COMPLAINT_ID
      },
      fileName: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: COMPLAINT_ATTACHMENTS.FIELDS.FILE_NAME
      },
      fileType: {
        type: DataTypes.STRING(20),
        allowNull: false,
        field: COMPLAINT_ATTACHMENTS.FIELDS.FILE_TYPE
      },
      filePath: {
        type: DataTypes.STRING,
        allowNull: false,
        field: COMPLAINT_ATTACHMENTS.FIELDS.FILE_PATH
      },
      createdAt: {
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: true,
        defaultValue: utils.getCurrentTime(),
        field: COMPLAINT_ATTACHMENTS.FIELDS.CREATED_AT
      },
      updatedAt: {
        type: DataTypes.BIGINT.UNSIGNED,
        allowNull: true,
        defaultValue: utils.getCurrentTime(),
        field: COMPLAINT_ATTACHMENTS.FIELDS.UPDATED_AT
      },
      deletedFlg: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        field: COMPLAINT_ATTACHMENTS.FIELDS.DELETE_FLG
      }
    },
    {
      freezeTableName: true
    }
  )
}
