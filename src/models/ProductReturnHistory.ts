import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { HISTORIES } from './ModelConstants'

export interface ProductReturnHistoryAttributes extends BaseAttributes {
  fieldName?: string
  prId?: number
}

export function productReturnHistoryFactory(sequelize: Sequelize) {
  return sequelize.define<ProductReturnHistoryAttributes>(HISTORIES.PRODUCT_RETURN, {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: HISTORIES.FIELDS.ID
    },
    prId: {
      type: DataTypes.INTEGER,
      field: HISTORIES.FIELDS.PR_ID
    },
    userId: {
      type: DataTypes.INTEGER,
      field: HISTORIES.FIELDS.USER_ID
    },
    fieldName: {
      type: DataTypes.STRING(100),
      allowNull: true,
      field: HISTORIES.FIELDS.FIELD_NAME
    },
    oldValue: {
      type: DataTypes.STRING(500),
      allowNull: true,
      field: HISTORIES.FIELDS.OLD_VALUE
    },
    newValue: {
      type: DataTypes.STRING(500),
      allowNull: true,
      field: HISTORIES.FIELDS.NEW_VALUE
    },
    watcherId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: HISTORIES.FIELDS.WATCHER_ID
    },
    watcherType: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: HISTORIES.FIELDS.WATCHER_TYPE
    },
    attachment: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: HISTORIES.FIELDS.ATTACHMENT
    },
    mail: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: HISTORIES.FIELDS.MAIL
    },
    action: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: HISTORIES.FIELDS.ACTION
    },
    createdAt: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: utils.getCurrentTime(),
      field: HISTORIES.FIELDS.CREATED_AT
    },
    updatedAt: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: utils.getCurrentTime(),
      field: HISTORIES.FIELDS.UPDATED_AT
    },
    deletedFlg: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: HISTORIES.FIELDS.DELETE_FLG
    }
  })
}
