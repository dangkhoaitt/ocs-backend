import { DataTypes, Sequelize } from 'sequelize'
import utils from '../services/utils'
import { BaseAttributes } from './base'
import { PRODUCT_RETURNS } from './ModelConstants'

export interface ProductReturnAttributes extends BaseAttributes, Record<string, unknown> {
  id?: number
  complaintId?: number
  prStatus?: number
  productReceivedDateBsc?: number
  packingDate?: number
  courierName?: string
  trackingNumber?: string
  oemSpecialReqDate?: number
  comment?: string
  supplierAddresses?: string
  contact?: string
  shippedDateToOem?: number
  productReceivedDateOem?: number
  infectiousProduct?: boolean
  infectionName?: string
  infectiousInformationRequired?: boolean
  requiredAction?: string
  requestCount?: number
  requestComment?: string
  requestDate?: number
  watchers?: number[]
}

export function productReturnFactory(sequelize: Sequelize) {
  return sequelize.define<ProductReturnAttributes>(PRODUCT_RETURNS.TABLE, {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: PRODUCT_RETURNS.FIELDS.ID
    },
    complaintId: {
      type: DataTypes.INTEGER,
      field: PRODUCT_RETURNS.FIELDS.COMPLAINT_ID
    },
    prStatus: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.PR_STATUS
    },
    productReceivedDateBsc: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.PRODUCT_RECEIVED_DATE_BSC
    },
    packingDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.PACKING_DATE
    },
    courierName: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.COURIER_NAME
    },
    trackingNumber: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.TRACKING_NUMBER
    },
    oemSpecialReqDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.OEM_SPECIAL_REQ_DATE
    },
    comment: {
      type: DataTypes.STRING(500),
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.COMMENT
    },
    supplierAddresses: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.SUPPLIER_ADDRESSES
    },
    contact: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.CONTACT
    },
    shippedDateToOem: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.SHIPPED_DATE_TO_OEM
    },
    productReceivedDateOem: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.PRODUCT_RECEIVED_DATE_OEM
    },
    infectiousProduct: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.INFECTIOUS_PRODUCT
    },
    infectionName: {
      type: DataTypes.STRING(500),
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.INFECTION_NAME
    },
    infectiousInformationRequired: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.INFECTIOUS_INFORMATION_REQUIRED
    },
    requiredAction: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.REQUIRED_ACTION
    },
    requestCount: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.REQUEST_COUNT
    },
    requestComment: {
      type: DataTypes.STRING(500),
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.REQUEST_COMMENT
    },
    requestDate: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.REQUEST_DATE
    },
    createdAt: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: utils.getCurrentTime(),
      field: PRODUCT_RETURNS.FIELDS.CREATED_AT
    },
    updatedAt: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: utils.getCurrentTime(),
      field: PRODUCT_RETURNS.FIELDS.UPDATED_AT
    },
    deletedFlg: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      field: PRODUCT_RETURNS.FIELDS.DELETE_FLG
    }
  })
}
