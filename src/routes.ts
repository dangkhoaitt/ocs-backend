import glob from 'glob'
import path from 'path'

const extList = ['.js', '.ts']
export default (app: Express.Application) => {
  glob(`${__dirname}/routes/**/*Routes*`, {}, (er, files) => {
    if (er) throw er
    files.forEach(async (file) => {
      if (extList.includes(path.extname(file))) {
        const { default: route } = await import(file)
        route(app)
      }
    })
  })
}
