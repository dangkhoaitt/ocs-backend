import { check } from 'express-validator'
import { PRODUCT_RETURNS } from '../models/ModelConstants'
import ComplaintService from '../services/ComplaintService'
import StatusService from '../services/StatusService'

enum ERR_CODE {
  PR_STATUS_REQUIRED = 'PR_STATUS_REQUIRED',
  PR_STATUS_NOT_EXIST = 'PR_STATUS_NOT_EXIST',
  PR_STATUS_TOO_LONG = 'PR_STATUS_TOO_LONG',
  COMPLAINT_ID_REQUIRED = 'COMPLAINT_ID_REQUIRED',
  COMPLAINT_ID_NOT_EXIST = 'COMPLAINT_ID_NOT_EXIST',
  PACKING_DATE_NOT_VALID = 'PACKING_DATE_NOT_VALID',
  PRODUCT_RECEIVED_DATE_BSC_NOT_VALID = 'PRODUCT_RECEIVED_DATE_BSC_NOT_VALID',
  OEM_SPECIAL_REQ_DATE_NOT_VALID = 'OEM_SPECIAL_REQ_DATE_NOT_VALID',
  REQUEST_DATE_NOT_VALID = 'REQUEST_DATE_NOT_VALID',
  PRODUCT_RECEIVED_DATE_NOT_VALID = 'PRODUCT_RECEIVED_DATE_NOT_VALID',
  SHIPPED_DATE_TO_OEM_NOT_VALID = 'SHIPPED_DATE_TO_OEM_NOT_VALID',
  COURIER_NAME_TOO_LONG = 'COURIER_NAME_TOO_LONG',
  TRACKING_NUMBER_TOO_LONG = 'TRACKING_NUMBER_TOO_LONG',
  COMMENT_TOO_LONG = 'COMMENT_TOO_LONG',
  CONTACT_TOO_LONG = 'CONTACT_TOO_LONG',
  INFECTION_NAME_TOO_LONG = 'INFECTION_NAME_TOO_LONG_TOO_LONG',
  REQUEST_COMMENT_TOO_LONG = 'REQUEST_COMMENT_TOO_LONG',
  REQUEST_COUNT_NOT_POSITIVE = 'REQUEST_COUNT_NOT_POSITIVE'
}

const validateStatus = (isCreate?: boolean) => {
  return check(PRODUCT_RETURNS.FIELDS.PR_STATUS_ALIAS)
    .optional(!isCreate)
    .custom(async (value: string) => {
      if (!value && isCreate) return Promise.reject(ERR_CODE.PR_STATUS_REQUIRED)
      if (value.length > 50) return Promise.reject(ERR_CODE.PR_STATUS_TOO_LONG)
      return StatusService.checkStatusExist(`${value}`).then((result) => {
        if (result === 0) return Promise.reject(ERR_CODE.PR_STATUS_NOT_EXIST)
      })
    })
}

const validateComplaintID = () => {
  return check(PRODUCT_RETURNS.FIELDS.COMPLAINT_ID_ALIAS).custom(async (value) => {
    if (!value) return Promise.reject(ERR_CODE.PR_STATUS_REQUIRED)
    return ComplaintService.checkComplaintExist(value).then((result) => {
      if (result === 0) return Promise.reject(ERR_CODE.COMPLAINT_ID_NOT_EXIST)
    })
  })
}

const validateDate = (key: string, errCode: ERR_CODE) => {
  return check(key)
    .optional()
    .custom(async (value) => {
      if (key && (isNaN(value) || Number(value) <= 0)) return Promise.reject(errCode)
      return Promise.resolve()
    })
}

const validateMaxLength = (key: string, max: number, errCode: ERR_CODE) => {
  return check(key)
    .optional()
    .custom(async (value: string) => {
      if (key && value && value.length > max) return Promise.reject(errCode)
      return Promise.resolve()
    })
}

const validatePositive = (key: string, errCode: ERR_CODE) => {
  return check(key)
    .optional()
    .custom(async (value: number) => {
      if (key && value < 0) return Promise.reject(errCode)
      return Promise.resolve()
    })
}

function validatePR(isCreate?: boolean) {
  return [
    validateStatus(isCreate),
    validateDate(PRODUCT_RETURNS.FIELDS.PRODUCT_RECEIVED_DATE_BSC_ALIAS, ERR_CODE.PRODUCT_RECEIVED_DATE_BSC_NOT_VALID),
    validateDate(PRODUCT_RETURNS.FIELDS.PACKING_DATE_ALIAS, ERR_CODE.PACKING_DATE_NOT_VALID),
    validateDate(PRODUCT_RETURNS.FIELDS.OEM_SPECIAL_REQ_DATE_ALIAS, ERR_CODE.OEM_SPECIAL_REQ_DATE_NOT_VALID),
    validateDate(PRODUCT_RETURNS.FIELDS.REQUEST_DATE_ALIAS, ERR_CODE.REQUEST_DATE_NOT_VALID),
    validateDate(PRODUCT_RETURNS.FIELDS.SHIPPED_DATE_TO_OEM_ALIAS, ERR_CODE.SHIPPED_DATE_TO_OEM_NOT_VALID),
    validateMaxLength(PRODUCT_RETURNS.FIELDS.COURIER_NAME_ALIAS, 50, ERR_CODE.COURIER_NAME_TOO_LONG),
    validateMaxLength(PRODUCT_RETURNS.FIELDS.TRACKING_NUMBER_ALIAS, 50, ERR_CODE.TRACKING_NUMBER_TOO_LONG),
    validateMaxLength(PRODUCT_RETURNS.FIELDS.COMMENT, 500, ERR_CODE.COMMENT_TOO_LONG),
    validateMaxLength(PRODUCT_RETURNS.FIELDS.CONTACT, 255, ERR_CODE.CONTACT_TOO_LONG),
    validateMaxLength(PRODUCT_RETURNS.FIELDS.INFECTION_NAME_ALIAS, 500, ERR_CODE.INFECTION_NAME_TOO_LONG),
    validateMaxLength(PRODUCT_RETURNS.FIELDS.REQUEST_COMMENT_ALIAS, 500, ERR_CODE.REQUEST_COMMENT_TOO_LONG),
    validatePositive(PRODUCT_RETURNS.FIELDS.REQUEST_COUNT_ALIAS, ERR_CODE.REQUEST_COUNT_NOT_POSITIVE)
  ]
}

export default {
  create: () => {
    const validations = validatePR(true)
    validations.push(validateComplaintID())
    return validations
  },
  edit: () => {
    return validatePR(false)
  }
}
