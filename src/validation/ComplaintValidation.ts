import { check } from 'express-validator'

export default {
    create: () => {
        return [
            check('statusCode', 'status is required!').not().isEmpty(),
            check('assignee', 'assignee is required!').not().isEmpty(),
            check('productId', 'product is required!').not().isEmpty()
        ]
    },
    edit: () => {
        return [
            check('statusCode', 'status is required!').not().isEmpty(),
            check('assignee', 'assignee is required!').not().isEmpty(),
            check('productId', 'product is required!').not().isEmpty()
        ]
    }
}
