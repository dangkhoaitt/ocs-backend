import { check } from 'express-validator'

export default {
  create: () => {
    return [check('username', 'User name is required!').not().isEmpty()]
  },

  update: () => {
    return [check('name', 'Name is Mandatory Parameter Missing.').not().isEmpty()]
  },

  login: () => {
    return [
      check('username', 'username is required!').not().isEmpty(),
      check('password', 'password is required!').not().isEmpty()
    ]
  }
}
