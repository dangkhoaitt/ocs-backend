import { check } from 'express-validator'
import { GFE_RECORDS } from '../models/ModelConstants'
import ComplaintService from '../services/ComplaintService'
import StatusService from '../services/StatusService'

enum ERR_CODE {
  GFE_STATUS_REQUIRED = 'GFE_STATUS_REQUIRED',
  GFE_STATUS_NOT_EXIST = 'GFE_STATUS_NOT_EXIST',
  GFE_ASSIGNED_REQUIRED = 'GFE_ASSIGNED_REQUIRED',
  GFE_REQUEST_DUE_DATE_REQUIRED = 'GFE_REQUEST_DUE_DATE_REQUIRED',
  GFE_STATUS_TOO_LONG = 'GFE_STATUS_TOO_LONG',
  COMPLAINT_ID_REQUIRED = 'COMPLAINT_ID_REQUIRED',
  COMPLAINT_ID_NOT_EXIST = 'COMPLAINT_ID_NOT_EXIST',
}

const validateStatus = (isCreate?: boolean) => {
  return check(GFE_RECORDS.FIELDS.STATUS_ALIAS)
    .optional(!isCreate)
    .custom(async (value: string) => {
      if (!value && isCreate) return Promise.reject(ERR_CODE.GFE_STATUS_REQUIRED)
      if (value.length > 50) return Promise.reject(ERR_CODE.GFE_STATUS_TOO_LONG)
      return StatusService.checkStatusExist(`${value}`).then((result) => {
        if (result === 0) return Promise.reject(ERR_CODE.GFE_STATUS_NOT_EXIST)
      })
    })
}

const validateComplaintID = () => {
  return check(GFE_RECORDS.FIELDS.COMPLAINT_ID_ALIAS).custom(async (value) => {
    if (!value) return Promise.reject(ERR_CODE.COMPLAINT_ID_REQUIRED)
    return ComplaintService.checkComplaintExist(value).then((result) => {
      if (result === 0) return Promise.reject(ERR_CODE.COMPLAINT_ID_NOT_EXIST)
    })
  })
}

const validateAssignee= () => {
  return check(GFE_RECORDS.FIELDS.ASSIGNEE).custom(async (value) => {
    if (!value) return Promise.reject(ERR_CODE.GFE_ASSIGNED_REQUIRED)
  })
}

const validateDate = (key: string, errCode: ERR_CODE) => {
  return check(key)
    .optional()
    .custom(async (value) => {
      if (key && (isNaN(value) || Number(value) <= 0)) return Promise.reject(errCode)
      return Promise.resolve()
    })
}

const validateMaxLength = (key: string, max: number, errCode: ERR_CODE) => {
  return check(key)
    .optional()
    .custom(async (value: string) => {
      if (key && value && value.length > max) return Promise.reject(errCode)
      return Promise.resolve()
    })
}

const validatePositive = (key: string, errCode: ERR_CODE) => {
  return check(key)
    .optional()
    .custom(async (value: number) => {
      if (key && value < 0) return Promise.reject(errCode)
      return Promise.resolve()
    })
}

function validateGfe(isCreate?: boolean) {
  return [
    validateStatus(isCreate),
    validateDate(GFE_RECORDS.FIELDS.REQUEST_DUE_DATE_ALIAS, ERR_CODE.GFE_REQUEST_DUE_DATE_REQUIRED),
  ]
}

export default {
  create: () => {
    const validations = validateGfe(true)
    validations.push(validateComplaintID())
    validations.push(validateAssignee())
    return validations
  },
  edit: () => {
    return validateGfe(false)
  }
}
