import { Request, Response } from 'express'
import { validationResult } from 'express-validator'
import constant from '../../constant/constant'
import reqResponse, { SuccessResponse } from '../../cors/responseHandler'
import GFEService, { GetGfeInput } from '../../services/GFEService'
import utils from '../../services/utils'

type AuthRequest = Request & { userId: string; countryId: string }

function updateFilter(req: AuthRequest): GetGfeInput {
  const { getAll, ...other } = req.query as GetGfeInput
  if (getAll === 'true') {
    if (other.assignee === req.userId) delete other.assignee
  } else {
    const assignee = other.assignee as string
    if (assignee) {
      other.assignee = assignee.split(',')
    } else {
      other.assignee = [req.userId]
    }
  }
  return other
}

export default {
  getGfeRecords: async (req: AuthRequest, res: Response) => {
    try {
      const input = updateFilter(req)
      const output = await GFEService.getGfeRecords(input)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', ...output }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  },
  exportGfeRecords: async (req: AuthRequest, res: Response) => {
    try {
      const { export_fields, ...other } = updateFilter(req)
      if (!export_fields) {
        res.status(400).send(reqResponse.errorResponse(418))
      } else {
        const buffer = await GFEService.exportGfeRecords(`${export_fields}`.split(','), other)
        if (!buffer) res.status(400).send(reqResponse.errorResponse(418))
        const filename = `gfe_records_export_${utils.convertTimestampToDateStr(Date.now() / 1000, 'YYYY-MM-DD-HH-mm')}`
        res.set({
          'Cache-Control': 'no-cache',
          'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
          'Content-Length': buffer.byteLength,
          'Content-Disposition': `attachment; filename=${filename}.xlsx`
        })
        res.send(buffer)
      }
    } catch (error) {
      res.status(400).send({ ...reqResponse.errorResponse(400), trace: error.message })
    }
  },

  getDetail: async (req: Request, res: Response) => {
    try {
      const detail = await GFEService.getDetail(req.params.id)

      const response: SuccessResponse = { status: 200, succMessage: 'OK', data: detail }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  },

  getByComplaint: async (req: Request, res: Response) => {
    try {
      const detail = await GFEService.getByComplaint(+req.params.complaintId)

      const response: SuccessResponse = { status: 200, succMessage: 'OK', data: detail }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  },

  create: async (req: AuthRequest, res: Response) => {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).send({ ...reqResponse.errorResponse(419), errors: errors.array() })
      }
      const data = await GFEService.createGfe(req.userId, req.body)
      if (!data) {
        return res
          .status(400)
          .send({ status: 400, message: 'You have created gfe too many times', message_key: constant.GFE_ERROR_CREATE })
      }
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      res.status(400).send({ ...reqResponse.errorResponse(419), tract: error.message })
    }
  },

  editGfe: async (req: AuthRequest, res: Response) => {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).send({ ...reqResponse.errorResponse(400), errors: errors.array() })
      }
      const data = await GFEService.editGfe(req.userId, req.params.id, req.body)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      res.status(400).send(reqResponse.errorResponse(400))
    }
  }
}
