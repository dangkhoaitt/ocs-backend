import { Application, Router } from 'express'
import RouteConstant from '../../constant/Routes'
import Middleware from '../../cors/middleware'
import GfeValidation from '../../validation/GfeValidation'
import GfeRecordController from './GfeRecordController'

const router = Router()

export default (app: Application) => {
  router.route('/').get(GfeRecordController.getGfeRecords)
  router.route('/export').get(GfeRecordController.exportGfeRecords)
  router.route('/').post(GfeValidation.create(), GfeRecordController.create)
  router.route('/:id([0-9]+)').put(GfeValidation.edit(), GfeRecordController.editGfe)
  router.route('/:id([0-9]+)').get(GfeRecordController.getDetail)
  router.route('/getGfeByComplaint/:complaintId([0-9]+)').get(GfeRecordController.getByComplaint)

  app.use(RouteConstant.GFE_RECORD, Middleware.checkToken, router)
}
