import { Application, NextFunction, Request, Response, Router } from 'express'
import constant from '../../constant/constant'
import RouteConstant from '../../constant/Routes'
import Middleware from '../../cors/middleware'
import { PRODUCT_RETURNS } from '../../models/ModelConstants'
import ProductReturnValidation from '../../validation/ProductReturnValidation'
import ProductReturnController from './ProductReturnController'

const router = Router()

type AuthRequest = Request & { userId: string; countryId: string; role: string }

const INSERT_FIELDS = Object.values(PRODUCT_RETURNS.FIELDS).filter(
  (f) => !f.includes('_') && f !== PRODUCT_RETURNS.FIELDS.ID
)
const EDIT_FIELDS = Object.values(PRODUCT_RETURNS.FIELDS).filter(
  (f) => !f.includes('_') && f !== PRODUCT_RETURNS.FIELDS.ID && f !== PRODUCT_RETURNS.FIELDS.COMPLAINT_ID_ALIAS
)
const OEM_EDIT_FIELDS = [PRODUCT_RETURNS.FIELDS.PRODUCT_RECEIVED_DATE_OEM_ALIAS]

const FULL_ROLE = [constant.ROLE.ROLE_WIPRO, constant.ROLE.ROLE_QA]

const EDIT_ROLE = FULL_ROLE.concat([constant.ROLE.ROLE_OEM])

const POST_DELETE_METHOD = [constant.HTTP_METHOD.POST, constant.HTTP_METHOD.DELETE]

const checkRole = (req: AuthRequest, res: Response, next: NextFunction) => {
  if (POST_DELETE_METHOD.includes(req.method) && FULL_ROLE.includes(req.role)) next()
  else if (req.method === constant.HTTP_METHOD.PATCH && EDIT_ROLE.includes(req.role)) next()
  else res.status(403).send()
}

/**
 * Middleware to remove unnecessary and common fields from body
 * @param req
 * @param res
 * @param next
 */
const sanitizeBody = (req: AuthRequest, res: Response, next: NextFunction) => {
  const { body } = req
  const keys = Object.keys(body)
  if (keys.length === 0) return res.status(204).send()
  const sanitize: Record<string, unknown> = {}

  if (req.method === constant.HTTP_METHOD.PATCH && req.role === constant.ROLE.ROLE_OEM) {
    keys.forEach((key) => {
      if (OEM_EDIT_FIELDS.includes(key)) sanitize[key] = body[key]
    })
  } else {
    keys.forEach((key) => {
      if (
        (req.method === constant.HTTP_METHOD.POST && INSERT_FIELDS.includes(key)) ||
        (req.method === constant.HTTP_METHOD.PATCH && EDIT_FIELDS.includes(key))
      )
        sanitize[key] = body[key]
    })
  }

  if (Object.keys(sanitize).length === 0) return res.status(204).send()
  req.body = sanitize
  next()
}

export default (app: Application) => {
  router.route('/').get(ProductReturnController.getProductReturns)
  router.route('/export').get(ProductReturnController.exportProductReturns)
  router
    .route('/create')
    .post(checkRole, sanitizeBody, ProductReturnValidation.create(), ProductReturnController.createProductReturn)
  router.route('/basic-complaint/:id([0-9]+)').get(ProductReturnController.getBasicComplaint)
  router.route('/:id([0-9]+)').get(ProductReturnController.getDetailProductReturn)
  router
    .route('/:id([0-9]+)')
    .patch(checkRole, sanitizeBody, ProductReturnValidation.edit(), ProductReturnController.editProductReturn)

  app.use(RouteConstant.PRODUCT_RETURN, Middleware.checkToken, router)
}
