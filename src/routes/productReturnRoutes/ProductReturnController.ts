import { Request, Response } from 'express'
import { validationResult } from 'express-validator'
import reqResponse, { SuccessResponse } from '../../cors/responseHandler'
import ComplaintService from '../../services/ComplaintService'
import ProductReturnService, { GetProductReturnInput } from '../../services/ProductReturnService'
import utils from '../../services/utils'

type AuthRequest = Request & { userId: string; countryId: string }

function updateFilter(req: AuthRequest): GetProductReturnInput {
  const { getAll, ...other } = req.query as GetProductReturnInput
  if (getAll === 'true') {
    if (other.assignee === req.userId) delete other.assignee
    if (other.country_id === req.countryId) delete other.country_id
  } else {
    other.assignee = other.assignee || req.userId
    other.country_id = other.country_id || req.countryId
  }
  return other
}

export default {
  getProductReturns: async (req: AuthRequest, res: Response) => {
    try {
      const input = updateFilter(req)
      const output = await ProductReturnService.getProductReturns(input)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', ...output }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(502).send(reqResponse.errorResponse(502))
    }
  },

  getDetailProductReturn: async (req: Request, res: Response) => {
    try {
      const detail = await ProductReturnService.getDetail(req.params.id)

      const response: SuccessResponse = { status: 200, succMessage: 'OK', data: detail }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(502).send(reqResponse.errorResponse(502))
    }
  },

  exportProductReturns: async (req: AuthRequest, res: Response) => {
    try {
      const { export_fields, ...other } = updateFilter(req)
      if (!export_fields) {
        res.status(400).send(reqResponse.errorResponse(418))
      } else {
        const buffer = await ProductReturnService.exportProductReturns(`${export_fields}`.split(','), other)
        if (!buffer) res.status(400).send(reqResponse.errorResponse(418))
        const filename = `product_returns_export_${utils.convertTimestampToDateStr(
          Date.now() / 1000,
          'YYYY-MM-DD-HH-mm'
        )}`
        res.set({
          'Cache-Control': 'no-cache',
          'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
          'Content-Length': buffer.byteLength,
          'Content-Disposition': `attachment; filename=${filename}.xlsx`
        })
        res.send(buffer)
      }
    } catch (error) {
      res.status(502).send({ ...reqResponse.errorResponse(418), trace: error.message })
    }
  },
  createProductReturn: async (req: AuthRequest, res: Response) => {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).send({ ...reqResponse.errorResponse(419), errors: errors.array() })
      }

      const data = await ProductReturnService.createProductReturn(req.userId, req.body)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      res.status(502).send({ ...reqResponse.errorResponse(419), trace: error.message })
    }
  },
  getBasicComplaint: async (req: AuthRequest, res: Response) => {
    try {
      const complaint = await ComplaintService.getBasicComplaint(req.params.id)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data: complaint }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(502).send(reqResponse.errorResponse(502))
    }
  },
  editProductReturn: async (req: AuthRequest, res: Response) => {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).send({ ...reqResponse.errorResponse(419), errors: errors.array() })
      }
      const data = await ProductReturnService.editProductReturn(req.userId, req.params.id, req.body)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      res.status(502).send(reqResponse.errorResponse(502))
    }
  }
}
