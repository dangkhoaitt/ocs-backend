import { Application, Router } from 'express'
import RouteConstant from '../../constant/Routes'
import Middleware from '../../cors/middleware'
import Validation from '../../validation/UserValidation'
import UserProfileController from './UserProfileController'

const router = Router()

export default (app: Application) => {
  router.route('/create').post(Validation.create(), UserProfileController.createUser)

  router.route('/update').post(Validation.update(), UserProfileController.updateUser)

  router.route('/test').get(UserProfileController.testUser);

  router.route('/getProfile').get(UserProfileController.getProfile);

  router.route('/logout').post(UserProfileController.signOut);

  router.route('/filter').patch(UserProfileController.updateFilter)

  app.use(RouteConstant.USER, Middleware.checkToken, router)
}
