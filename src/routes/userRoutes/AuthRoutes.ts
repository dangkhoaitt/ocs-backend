import { Application, Router } from 'express'
import RouteConstant from '../../constant/Routes'
import Validation from '../../validation/UserValidation'
import UserProfileController from './UserProfileController'

const router = Router()

export default (app: Application) => {

    router.route('/login').post(Validation.login(), UserProfileController.signin);
    router.route('/login-sso').get(UserProfileController.signinSSo);
    router.route('/assert-sso').post(UserProfileController.assertSSo);

    app.use(RouteConstant.USER, router)
}
