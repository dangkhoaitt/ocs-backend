import bcrypt from 'bcryptjs';
import config from 'config';
import { Request, Response } from 'express';
import { validationResult } from 'express-validator';
import jwt from 'jsonwebtoken';
import mode from '../../config';
import constant from '../../constant/constant';
import reqResponse, { SuccessResponse } from '../../cors/responseHandler';
import UserProfileServices from '../../services/UserProfileServices';


const generateToken = async (userId: number, countryId?: number, role?: string) => {
    const token = jwt.sign({ id: userId, countryId, role }, config.get(`${mode}.secret`), {
        expiresIn: constant.TOKEN_EXPIRE // 24 hours
    });
    return token;
}

export default {
    createUser: async (req: Request, res: Response) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(402).send(reqResponse.errorResponse(402))
        }
        const data = req.body
        const params = req.params
        const query = req.query
        try {
            const result = await UserProfileServices.createUser(data);
            const response: SuccessResponse = {
                status: 201,
                succMessage: 'User Created',
                data: 'User has been created successfully'
            }
            res.status(201).send(reqResponse.sucessResponse(response))
        } catch (error) {
            console.error(error)
            res.status(501).send(reqResponse.errorResponse(501))
        }
    },

    updateUser: (req: Request, res: Response) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(402).send(reqResponse.errorResponse(402))
        }
        const data = req.body
        const params = req.params
        const query = req.query
        UserProfileServices.updateUser(data, params, query)
            .then(() => {
                const response: SuccessResponse = {
                    status: 201,
                    succMessage: 'User Updated',
                    data: 'User has been updated successfully'
                }
                res.status(201).send(reqResponse.sucessResponse(response))
            })
            .catch((error: Error) => {
                console.error(error)
                res.status(502).send(reqResponse.errorResponse(502))
            })
    },

    signin: async (req: Request, res: Response) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(405).send(reqResponse.errorResponse(405));
        }
        try {
            const userInfo = await UserProfileServices.getUserByUserName(req.body.username);
            if (!userInfo) {
                return res.status(401).send({
                    status: 401,
                    message: "Invalid username!",
                    message_key: constant.ERROR_WRONG_USERORPASSWORD
                });
            }
            const passwordIsValid = bcrypt.compareSync(
                req.body.password,
                userInfo.password
            );

            if (!passwordIsValid) {
                return res.status(401).send({
                    status: 401,
                    message: "Invalid Password!",
                    message_key: constant.ERROR_WRONG_USERORPASSWORD
                });
            }
            const token = await generateToken(userInfo.id, userInfo.countryId, userInfo.role);
            if (token) {
                const response: SuccessResponse = {
                    status: 201,
                    succMessage: 'User Updated',
                    data: {
                        id: userInfo.id,
                        username: userInfo.userName,
                        fullName: userInfo.firstName + " " + userInfo.lastName,
                        role: userInfo.role,
                        accessToken: token
                    },
                    message_key: constant.LOGIN_SUCCESS
                }
                res.status(201).send(reqResponse.sucessResponse(response))
            }
        } catch (error) {
            console.error(error)
            res.status(502).send(reqResponse.errorResponse(502))
        }
    },

    signOut: async (req: Request, res: Response) => {
        return res.status(200).send({
            status: 200,
            message: "ok"
        });
    },

    testUser: (req: any, res: Response) => {
        console.log(req.userId);
        return res.status(200).send({
            status: 200,
            message: "ok"
        });
    },

    signinSSo: async (req: Request, res: Response) => {

        const redirect = "?redirect_url=" + (req.query.redirect_url ? req.query.redirect_url : "");
        console.log("\n\n\n/*** redirect ***\\ \n", redirect)

        const idp = await UserProfileServices.createSamlIP();
        const sp = await UserProfileServices.createSamlSP(redirect);
        sp.create_login_request_url(idp, {}, (err, login_url, request_id) => {
            if (err != null)
                return res.status(501).send(reqResponse.errorResponse(501));
            if (login_url && login_url.length > 0) {
                res.redirect(login_url);
            }
        });
    },

    assertSSo: async (req: Request, res: Response) => {
        const sp = await UserProfileServices.createSamlSP("");
        const idp = await UserProfileServices.createSamlIP();
        const directUrl = (req.query.redirect_url ? req.query.redirect_url : "");
        console.log(directUrl);

        const options = { request_body: req.body };
        sp.post_assert(idp, options, async (err, saml_response) => {
            if (err != null) {
                return res.status(401).send(reqResponse.errorResponse(401));
            }
            const result = saml_response.user;
            console.log("\n\n\n/*** result ***\\ \n", JSON.stringify(result));

            if (result) {
                let userInfo = await UserProfileServices.getUserByUserName(result.name_id);
                if (!userInfo) {
                    const dataUser = {
                        username: result.name_id,
                        firstName: result.attributes?.firstname[0],
                        lastName: result.attributes?.lastname[0],
                        phone: result.attributes?.telephoneNumber[0],
                        title: result.attributes?.title[0],
                        password: "",
                        role: constant.ROLE.ROLE_WIPRO,
                        mobile: result.attributes?.mobile[0],
                    };
                    userInfo = await UserProfileServices.createUser(dataUser)
                }

                // create jwtToken for user
                const jwtToken = await generateToken(userInfo.id, userInfo.countryId, userInfo.role);

                // redirect to client with token
                res.redirect(directUrl + '?token=' + jwtToken);
            } else {
                return res.status(401).send(reqResponse.errorResponse(401));
            }
        });
    },

    getProfile: async (req: any, res: Response) => {
        const userInfo = await UserProfileServices.getUserById(req.userId);
        if (!userInfo) {
            return res.status(401).send(reqResponse.errorResponse(401));
        }
        const response: SuccessResponse = {
            status: 200,
            succMessage: 'successfully!!',
            data: {
                id: userInfo.id,
                username: userInfo.userName,
                fullName: userInfo.fullName,
                role: userInfo.role,
                avatar: userInfo.avatar,
                type: userInfo.type,
                email: userInfo.email,
                countryId: userInfo.countryId,
            }
        }
        res.status(200).send(reqResponse.sucessResponse(response));
    },
    updateFilter: async (req: Request & { userId: number }, res: Response) => {
      const { filter } = req.body
      const userInfo = await UserProfileServices.updateFilter(req.userId, filter)
      const response: SuccessResponse = { status: 200, succMessage: 'successfully!!', data: userInfo }
      res.status(200).send(reqResponse.sucessResponse(response))
    }
}
