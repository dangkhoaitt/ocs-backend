import { Request, Response } from 'express'
import constant from '../../constant/constant'
import reqResponse, { SuccessResponse } from '../../cors/responseHandler'
import ComplaintService, { GetListInput } from '../../services/ComplaintService'
import utils from '../../services/utils'

type AuthRequest = Request & { userId: string; countryId: string }

function updateFilter(req: AuthRequest): GetListInput {
  const { getAll, ...other } = req.query as GetListInput
  if (getAll === 'true') {
    if (other.assignee === req.userId) delete other.assignee
    if (other.country_id === req.countryId) delete other.country_id
  } else {
    other.assignee = other.assignee || (req.userId as string)
    other.country_id = other.country_id || (req.countryId as string)
  }
  return other
}

export default {
  getComplaints: async (req: AuthRequest, res: Response) => {
    try {
      const input = updateFilter(req)
      const { total, page, data, limit } = await ComplaintService.getList(input)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data, total, page, limit }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      res.status(502).send({ ...reqResponse.errorResponse(418), trace: error.message })
    }
  },
  exportComplaints: async (req: AuthRequest, res: Response) => {
    try {
      const input = updateFilter(req)
      const { export_fields, ...other } = input
      if (!export_fields) {
        res.status(400).send(reqResponse.errorResponse(418))
      } else {
        const buffer = await ComplaintService.getExportList(`${export_fields}`.split(','), other)
        if (!buffer) return res.status(400).send(reqResponse.errorResponse(418))
        const filename = `complaints_export_${utils.convertTimestampToDateStr(Date.now() / 1000, 'YYYY-MM-DD-HH-mm')}`
        res.set({
          'Cache-Control': 'no-cache',
          'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
          'Content-Length': buffer.byteLength,
          'Content-Disposition': `attachment; filename=${filename}.xlsx`
        })
        res.status(200).send(buffer)
      }
    } catch (error) {
      res.status(502).send({ ...reqResponse.errorResponse(418), trace: error.message })
    }
  },
  getComplaintDetail: async (req: Request, res: Response) => {
    try {
      const complaintId = parseInt(req.params.complaintId, 10)

      const dataComplaint = await ComplaintService.getDetail(complaintId)

      const response: SuccessResponse = { status: 200, succMessage: 'OK', data: { detail: dataComplaint } }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(502).send(reqResponse.errorResponse(502))
    }
  },
  createComplaint: async (req: AuthRequest, res: Response) => {
    try {
      const input = req.body

      const data = await ComplaintService.updateOrCreate(req.userId, input)

      const response: SuccessResponse = { status: 200, succMessage: 'OK', data }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  },
  editComplaint: async (req: AuthRequest, res: Response) => {
    try {
      const input = req.body
      const complaintId = parseInt(req.params.complaintId, 10)

      const data = await ComplaintService.updateOrCreate(req.userId, input, complaintId)

      const response: SuccessResponse = { status: 200, succMessage: 'OK', data: { complaintId: data } }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  },
  cloneComplaint: async (req: Request, res: Response) => {
    try {
      const complaintId = req.body.complaintId as number

      const data = await ComplaintService.cloneComplaint(complaintId)
      if (!data) {
        return res.status(400).send({ status: 400, message: 'Not found!', message_key: constant.RES_ERROR })
      }
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data: { complaintId: data } }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  },
  getInfoByComplaintId: async (req: Request, res: Response) => {
    try {
      const complaintId = parseInt(req.params.complaintId, 10)

      const data = await ComplaintService.getInfoByComplaintId(complaintId)

      const response: SuccessResponse = { status: 200, succMessage: 'OK', data }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  }
}
