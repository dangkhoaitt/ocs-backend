import { Application, NextFunction, Request, Response, Router } from 'express'
import constant from '../../constant/constant'
import RouteConstant from '../../constant/Routes'
import Middleware from '../../cors/middleware'
import Validation from '../../validation/ComplaintValidation'
import ComplaintController from './ComplaintController'
const router = Router()

export default (app: Application) => {
  router.route('/').get(ComplaintController.getComplaints)
  router.route('/:complaintId([0-9]+)').get((req: Request, res: Response, next: NextFunction) => Middleware.checkRole(req, res, next, constant.ROLE_COMPLAINT.VIEW), ComplaintController.getComplaintDetail)
  router.route('/export').get(ComplaintController.exportComplaints)
  router.route('/').post(Validation.create(), (req: Request, res: Response, next: NextFunction) => Middleware.checkRole(req, res, next, constant.ROLE_COMPLAINT.ADD), ComplaintController.createComplaint)
  router.route('/:complaintId([0-9]+)').put(Validation.edit(), (req: Request, res: Response, next: NextFunction) => Middleware.checkRole(req, res, next, constant.ROLE_COMPLAINT.EDIT), ComplaintController.editComplaint)
  router.route('/clone-complaint').post((req: Request, res: Response, next: NextFunction) => Middleware.checkRole(req, res, next, constant.ROLE_COMPLAINT.CLONE), ComplaintController.cloneComplaint)
  router.route('/get-action-link/:complaintId([0-9]+)').get(ComplaintController.getInfoByComplaintId)

  app.use(RouteConstant.COMPLAINT, Middleware.checkToken, router)
}
