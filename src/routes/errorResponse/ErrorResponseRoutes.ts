import { Application, Router } from 'express'
import RouteConstant from '../../constant/Routes'
import ErrorResponseController from './ErrorResponseController'

const router = Router()

export default (app: Application) => {
  router.route('/getAllErrorData').get(ErrorResponseController.getAllErrorData)

  router.route('/getErrorDataByCode/:code').get(ErrorResponseController.getErrorDataByCode)

  app.use(RouteConstant.GET_ERROR, router)
}
