import { Request, Response } from 'express'
import getErrorStatus from '../../constant/ErrorData'
import reqResponse, { SuccessResponse } from '../../cors/responseHandler'

export default {
  getAllErrorData: async (req: Request, res: Response) => {
    const response: SuccessResponse = {
      status: 201,
      succMessage: 'Fetch All Error Code',
      data: getErrorStatus.ERROR_STATUS_ARRAY
    }
    res.status(201).send(reqResponse.sucessResponse(response))
  },

  getErrorDataByCode: (req: Request, res: Response) => {
    const { code } = req.params
    const findErrorCode = getErrorStatus.ERROR_STATUS_ARRAY.find((c) => c.status === Number(code))
    const response: SuccessResponse = { status: 201, succMessage: 'Fetch Error Code', data: findErrorCode }
    res.status(201).send(reqResponse.sucessResponse(response))
  }
}
