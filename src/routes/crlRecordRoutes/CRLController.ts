import { Request, Response } from 'express'
import reqResponse from '../../cors/responseHandler'
import CRLService, { GetCRLFilter } from '../../services/CRLService'
import utils from '../../services/utils'

type AuthRequest = Request & { userId: string; countryId: string }

function updateFilter(req: AuthRequest): GetCRLFilter {
  const { getAll, ...other } = req.query as GetCRLFilter
  if (getAll === 'true') {
    if (other.assignee === req.userId) delete other.assignee
    if (other.country_id === req.countryId) delete other.country_id
  } else {
    other.assignee = other.assignee || req.userId
    other.country_id = other.country_id || req.countryId
  }
  return other
}

export default {
  exportCRL: async (req: AuthRequest, res: Response) => {
    try {
      const { export_fields, ...other } = updateFilter(req)
      if (!export_fields) {
        res.status(400).send(reqResponse.errorResponse(418))
      } else {
        const buffer = await CRLService.exportCRL(`${export_fields}`.split(','), other)
        if (!buffer) res.status(400).send(reqResponse.errorResponse(418))
        const filename = `crl_records_export_${utils.convertTimestampToDateStr(
          Date.now() / 1000,
          'YYYY-MM-DD-HH-mm'
        )}`
        res.set({
          'Cache-Control': 'no-cache',
          'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
          'Content-Length': buffer.byteLength,
          'Content-Disposition': `attachment; filename=${filename}.xlsx`
        })
        res.send(buffer)
      }
    } catch (error) {
      console.log('error :>> ', error);
      res.status(502).send({ ...reqResponse.errorResponse(418), trace: error.message })
    }
  }
}
