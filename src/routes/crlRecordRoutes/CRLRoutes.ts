import { Application, Router } from 'express'
import RouteConstant from '../../constant/Routes'
import Middleware from '../../cors/middleware'
import CRLController from './CRLController'

const router = Router()

export default (app: Application) => {
  router.route('/export').get(CRLController.exportCRL)

  app.use(RouteConstant.CRL_RECORD, Middleware.checkToken, router)
}
