import { Request, Response } from 'express'
import constant from '../../constant/constant'
import reqResponse, { SuccessResponse } from '../../cors/responseHandler'
import CommonService from '../../services/CommonService'
import ComplaintService from '../../services/ComplaintService'
import ProductService from '../../services/ProductService'
import SupplierService from '../../services/SupplierService'
import UserProfileServices from '../../services/UserProfileServices'
import utils from '../../services/utils'

type AuthRequest = Request & { userId: number; countryId: number }

const TYPE_ENUM = {
  COMPLAINT: 'complaint',
  GFE: 'gfe',
  CRL: 'crl',
  MDV: 'mdv',
  PRO_RETURN: 'pro_ret',
  COUNTRY: 'country',
  OEM: 'oem',
  AREA: 'area',
  ATTACHMENT: 'attachment',
  PRODUCT: 'product',
  USER: 'user'
}

export default {
  getStatus: async (_: Request, res: Response) => {
    try {
      const data = await CommonService.getStatusList()
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(502).send(reqResponse.errorResponse(502))
    }
  },
  autoComplete: async (req: AuthRequest, res: Response) => {
    try {
      const { type, keyword = '', is_default } = req.query as { type: string; keyword: string; is_default: string }
      let data: unknown = []
      switch (type) {
        case TYPE_ENUM.OEM:
          data = await SupplierService.getSupplierSuggestionList(keyword, is_default === 'true')
          break
        case TYPE_ENUM.USER:
          data = await UserProfileServices.getUserSuggestionList(keyword, is_default === 'true')
          break
        case TYPE_ENUM.PRODUCT:
          data = await ProductService.getProductSuggestionList(keyword, is_default === 'true')
          break
        case TYPE_ENUM.COMPLAINT:
          data = await ComplaintService.getSuggestionList(keyword, req.countryId, is_default === 'true')
          break
        default:
          break
      }

      const response: SuccessResponse = { status: 200, succMessage: 'OK', data }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      res.status(502).send({ ...reqResponse.errorResponse(420), trace: error.message })
    }
  },
  countries: async (_: Request, res: Response) => {
    try {
      const data: unknown = await CommonService.getCountryList()
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      res.status(502).send(reqResponse.errorResponse(502))
    }
  },

  getAttachment: async (req: Request, res: Response) => {
    try {
      const id = parseInt(req.params.Id, 10)
      const type = req.query.type.toString()
      const data = await CommonService.getAttachment(type, id)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  },

  getWatcher: async (req: Request, res: Response) => {
    try {
      const id = parseInt(req.params.Id, 10)
      const type = req.query.type.toString()
      const data = await CommonService.getWatcher(type, id)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  },

  uploadFile: async (req: Request, res: Response) => {
    try {
      const uploadFile = utils.upload()
      uploadFile(req, res, async (err: any) => {
        if (err) {
          console.log(err)
          return res.status(403).send({ status: 403, message: err.message, message_key: err.code })
        }
        const data = await CommonService.upLoadFile(req)
        const response: SuccessResponse = { status: 200, succMessage: 'OK', data, message_key: constant.RES_SUCCCESS }
        res.status(200).send(reqResponse.sucessResponse(response))
      })
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  },

  addAttachmentFile: async (req: AuthRequest, res: Response) => {
    try {
      const input = req.body
      const data = await CommonService.addAttachmentFile(req.userId, input)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data, message_key: constant.RES_SUCCCESS }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  },

  addWatcher: async (req: AuthRequest, res: Response) => {
    try {
      const data = await CommonService.addWatcher(req.userId, req.body)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data, message_key: constant.RES_SUCCCESS }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  },

  deleteWatcher: async (req: AuthRequest, res: Response) => {
    try {
      const id = utils.convertStrToInt(req.params.Id)
      const type = req.query.type as string
      const data = await CommonService.deleteWatcher(req.userId, id, type)
      if (!data) {
        return res.status(400).send({ status: 400, message: 'Delete error', message_key: constant.RES_ERROR })
      }
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data, message_key: constant.RES_SUCCCESS }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  },

  deleteAttachmentFile: async (req: AuthRequest, res: Response) => {
    try {
      const id = utils.convertStrToInt(req.params.Id)
      const type = req.query.type as string
      const data = await CommonService.deleteAttachmentFile(req.userId, id, type)
      if (!data) {
        return res.status(400).send({ status: 400, message: 'Delete file error', message_key: constant.RES_ERROR })
      }
      utils.deleteFileFromS3(data)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data, message_key: constant.RES_SUCCCESS }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  },

  getSummary: async (req: Request & { userId: number; countryId: number }, res: Response) => {
    try {
      const data = await CommonService.getSummary(req.userId, req.countryId)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data, message_key: constant.RES_SUCCCESS }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      console.error(error)
      res.status(400).send(reqResponse.errorResponse(400))
    }
  },

  getActivities: async (req: AuthRequest, res: Response) => {
    try {
      const data = await CommonService.getActivities(req.params.id, req.query)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data, message_key: constant.RES_SUCCCESS }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      res.status(502).send(reqResponse.errorResponse(502))
    }
  },
  getManualMailTemplate: async (req: AuthRequest, res: Response) => {
    try {
      const data = await CommonService.getManualMailTemplate(req.countryId, req.query as any)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data, message_key: constant.RES_SUCCCESS }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      res.status(502).send(reqResponse.errorResponse(502))
    }
  },
  sendMailAuto: async (req: AuthRequest, res: Response) => {
    try {
      const data = await CommonService.sendMailAuto(req.userId, req.query as any)
      const response: SuccessResponse = { status: 200, succMessage: 'OK', data, message_key: constant.RES_SUCCCESS }
      res.status(200).send(reqResponse.sucessResponse(response))
    } catch (error) {
      res.status(502).send(reqResponse.errorResponse(502))
    }
  }
}
