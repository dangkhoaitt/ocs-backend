import { Application, Router } from 'express'
import RouteConstant from '../../constant/Routes'
import Middleware from '../../cors/middleware'
import CommonController from './CommonController'
const router = Router()

export default (app: Application) => {
  router.route('/get-status').get(CommonController.getStatus)
  router.route('/autocomplete').get(CommonController.autoComplete)
  router.route('/countries').get(CommonController.countries)
  router.route('/get-attachment/:Id([0-9]+)').get(CommonController.getAttachment)
  router.route('/get-watcher/:Id([0-9]+)').get(CommonController.getWatcher)
  router.route('/upload-files').post(CommonController.uploadFile)
  router.route('/add-attachment').post(CommonController.addAttachmentFile)
  router.route('/add-watcher').post(CommonController.addWatcher)
  router.route('/delete-watcher/:Id([0-9]+)').delete(CommonController.deleteWatcher)
  router.route('/delete-attachment/:Id([0-9]+)').delete(CommonController.deleteAttachmentFile)
  router.route('/summary').get(CommonController.getSummary)
  router.route('/activity/:id([0-9]+)').get(CommonController.getActivities)
  router.route('/send-mail-manual').get(CommonController.getManualMailTemplate)
  router.route('/send-mail-auto').post(CommonController.sendMailAuto)

  app.use(RouteConstant.COMMON, Middleware.checkToken, router)
}
