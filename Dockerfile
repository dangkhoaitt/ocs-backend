FROM node:10.23.0-alpine
WORKDIR /app
COPY package.json /app

ARG NODE_ENV
ENV NODE_ENV=$NODE_ENV
RUN echo "NODE_ENV = ${NODE_ENV}"

RUN npm install
COPY . /app
RUN npm run build
CMD node src/server.js
EXPOSE 8000