define({ "api": [
  {
    "type": "post",
    "url": "/api/complaints",
    "title": "Create complaint",
    "name": "CreateComplaint",
    "group": "Complaint",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "reporter",
            "description": "<p>Exp: admin *</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"version\": \"1.0\",\n   \"statusCode\": 200,\n   \"messageCode\": \"SUCCESS\",\n   \"message\": \"Request successful.\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/services/UserProfileServices.js",
    "groupTitle": "Complaint"
  },
  {
    "type": "get",
    "url": "/api/complaints",
    "title": "Get list complaint",
    "name": "GetList",
    "group": "Complaint",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "reporter",
            "description": "<p>Exp: admin</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assigned",
            "description": "<p>Exp: alex</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "created",
            "description": "<p>Exp: 2020-12-07</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Exp: {TYPE_ENUM}</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>StatusID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "country",
            "description": "<p>country code</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>number</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sortField",
            "description": "<p>field to sort</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sortOrder",
            "description": "<p>ASC or DESC</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"version\": \"1.0\",\n   \"statusCode\": 200,\n   \"messageCode\": \"SUCCESS\",\n   \"message\": \"Request successful.\",\n   \"data\":[\n             {\n               \"complaint_id\":1,\n               \"status_name\":\"NEW\",\n               \"status_id\":1,\n               \"reporter\":\"Hai Tran\",\n               \"assigned\":\"Alex\",\n               \"created_at\":\"2020-12-07 16:10\",\n               \"country\":\"China\",\n               \"product_name\":\"Machine\",\n               \"mdv_id\": 1,\n               \"product_return_id\":1,\n               \"crl_id\":1,\n               \"gpe_id\":1\n             }\n         ],\n   \"limit\": 10,\n   \"page\": 1,\n   \"total\": 6\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/services/UserProfileServices.js",
    "groupTitle": "Complaint"
  },
  {
    "type": "get",
    "url": "/api/product-return",
    "title": "Get list product return",
    "name": "GetList",
    "group": "ProductReturn",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "reporter",
            "description": "<p>Exp: admin</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "assigned",
            "description": "<p>Exp: alex</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "created",
            "description": "<p>Exp: 2020-12-07</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Exp: {TYPE_ENUM}</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>StatusID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "country",
            "description": "<p>country code</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "description": "<p>number</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sortField",
            "description": "<p>field to sort</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sortOrder",
            "description": "<p>ASC or DESC</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"version\": \"1.0\",\n   \"statusCode\": 200,\n   \"messageCode\": \"SUCCESS\",\n   \"message\": \"Request successful.\",\n   \"data\":[\n             {\n               \"complaint_id\":1,\n               \"status_name\":\"NEW\",\n               \"status_id\":1,\n               \"reporter\":\"Hai Tran\",\n               \"assigned\":\"Alex\",\n               \"created_at\":\"2020-12-07 16:10\",\n               \"country\":\"China\",\n               \"product_name\":\"Machine\",\n               \"mdv_id\": 1,\n               \"product_return_id\":1,\n               \"crl_id\":1,\n               \"gpe_id\":1\n             }\n         ],\n   \"limit\": 10,\n   \"page\": 1,\n   \"total\": 6\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/services/UserProfileServices.js",
    "groupTitle": "ProductReturn"
  }
] });
