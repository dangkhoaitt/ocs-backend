define({
  "title": "Custom apiDoc browser title",
  "url": "",
  "name": "OCS",
  "version": "1.0.0",
  "description": "OCS System",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2020-12-08T03:36:42.520Z",
    "url": "https://apidocjs.com",
    "version": "0.25.0"
  }
});
