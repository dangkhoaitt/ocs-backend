# Nodejs Project
Nodejs Project Structure For Express REST API

## Testing Locally
Make sure you have installed [Nodejs](#https://nodejs.org/en/), [npm](#https://www.npmjs.com/get-npm) and [git](#https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
<br />
<small>Note: npm is installed with Node.js</small>

```bash
yarn install

yarn dev (For Development environment) 

OR

yarn staging (For Staging environment)

OR

yarn prod (For Production environment)
```
